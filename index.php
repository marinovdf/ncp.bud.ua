<?php
date_default_timezone_set('Europe/Kiev');

$debug = 1;

if (in_array($_SERVER['REMOTE_ADDR'], array('212.109.49.250'))) {
    error_reporting(E_ALL);
    ini_set('display_errors', true);
    ini_set('error_reporting', E_ALL);
} else {
    //todo убрать после запуска
    //die('Access denied');
    //header('HTTP/1.0 403 Forbidden');
    //die('Forbidden');
}

if (isset($_SERVER['HTTP_REFERER'])) {
    $referer = parse_url($_SERVER['HTTP_REFERER']);
} else {
    $referer['host'] = '';
}

if (in_array($referer['host'], array('bud.ua', 'www.bud.ua'))) {
    setcookie('age', 1, time() + 60 * 60 * 24 * 30);
}

// change the following paths if necessary
$yii    = dirname(__FILE__) . '/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

if ($debug) {
    if ($_SERVER['REMOTE_ADDR'] == '212.109.49.250' || $_SERVER['REMOTE_ADDR'] == '195.214.214.102') {
        // remove the following lines when in production mode
        defined('YII_DEBUG') or define('YII_DEBUG', true);
        // specify how many levels of call stack should be shown in each log message
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
    }
}

require_once($yii);
Yii::createWebApplication($config)->run();