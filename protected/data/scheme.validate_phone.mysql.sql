--
-- Структура таблицы `buddy_bar_validatephone`
--

CREATE TABLE IF NOT EXISTS `buddy_bar_validatephone` (`PhoneNumber` VARCHAR (12) NOT NULL COMMENT 'Phone Number 380ххххххххх', `ValidateCode` VARCHAR (8) NOT NULL COMMENT 'Sended validation code', `ValidateStatus` VARCHAR (255) NOT NULL COMMENT 'Phone validation status (send/validate)', `ResponseText` VARCHAR (255) DEFAULT NULL COMMENT 'Response text from provider. Null if not exists', `IP` VARCHAR (255) DEFAULT NULL COMMENT 'User IP', `ResponseStatus` VARCHAR (50) DEFAULT NULL COMMENT 'Response status from provider', `MessageID` VARCHAR (50) DEFAULT NULL COMMENT 'Unique message ID from provider', `TransactionDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Transaction Date and Time') ENGINE = MyISAM DEFAULT CHARSET = utf8;
