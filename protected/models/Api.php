<?php

/**
 * Class API
 * Модель для работы с пользователями
 */
class Api extends Users
{
    /**
     * GUID активности Passport2Rio
     */
    const ACTIVITY_GUID_PASSPORT2RIO = '257b97446F7D-A7A3-E311-8F3F-000C29E38BCE';

    /**
     * GUID активности Stadiums
     */
    const ACTIVITY_GUID_STADIUMS = '257b97446F7D-A7A3-E311-8F3F-000C29E38BCE';

    /**
     * Адрес сервера JUMP для генерации пароля
     */
    const JUMP_API_URL = 'http://bud2014.nmi.com.ua/index.php';

    /**
     * Номер телефона
     *
     * @var
     */
    public $phone;
    /**
     * Пароль
     *
     * @var
     */
    public $password;
    /**
     * Вид активности
     *
     * @var
     */
    public $activity;
    /**
     * Токен
     *
     * @var
     */
    public $token;

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('activity', 'required'),
            array('phone', 'match', 'pattern' => '/^380+(67|68|96|97|98|50|66|95|99|63|93)+\d{7}$/', 'allowEmpty' => false, 'message' => 'invalid phone format'),
            array('password', 'match', 'pattern' => '/^[A-z0-9\s]+$/u', 'allowEmpty' => false, 'message' => 'invalid password format'),
            array('token', 'length', 'is' => 26, 'allowEmpty' => true, 'message' => 'invalid token format'),
            array('activity', 'in', 'range' => $this->getActivityList(), 'allowEmpty' => false)
        );
    }

    /**
     * Return user data
     *
     * @param array $params
     *
     * @return array|mixed
     */
    public function getUser($params = array())
    {
        $sql = 'EXECUTE dbo.p_rapp_GetConsumer
                    @ConsumerGUID =:ConsumerGUID,
                    @FacebookUserID =:FacebookUserID,
                    @Password =:Password,
                    @MobilePhone =:MobilePhone,
                    @EmailAddress =:EmailAddress';

        $connection = Yii::app()->db->createCommand($sql);
        $connection->bindValues(array(
            ':ConsumerGUID'   => !empty($params['ConsumerGUID']) ? $params['ConsumerGUID'] : '',
            ':FacebookUserID' => !empty($params['FacebookUserID']) ? $params['FacebookUserID'] : '',
            ':Password'       => !empty($params['Password']) ? $params['Password'] : '',
            ':MobilePhone'    => !empty($params['MobilePhone']) ? $params['MobilePhone'] : '',
            ':EmailAddress'   => !empty($params['EmailAddress']) ? $params['EmailAddress'] : '',
        ));

        $result  = $connection->queryRow();
        $charset = new Charset();

        if ($result) {
            foreach ($result as $k => $v)
                $result[$k] = $charset->toUTF8($v);
        } else
            $result = array();

        return $result;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getUsers($params = array())
    {
        $sql = 'EXECUTE dbo.p_rapp_GetConsumer
                    @ConsumerGUID =:ConsumerGUID,
                    @FacebookUserID =:FacebookUserID,
                    @Password =:Password,
                    @MobilePhone =:MobilePhone,
                    @EmailAddress =:EmailAddress';

        $connection = Yii::app()->db->createCommand($sql);
        $connection->bindValues(array(
            ':ConsumerGUID'   => !empty($params['ConsumerGUID']) ? $params['ConsumerGUID'] : '',
            ':FacebookUserID' => !empty($params['FacebookUserID']) ? $params['FacebookUserID'] : '',
            ':Password'       => !empty($params['Password']) ? $params['Password'] : '',
            ':MobilePhone'    => !empty($params['MobilePhone']) ? $params['MobilePhone'] : '',
            ':EmailAddress'   => !empty($params['EmailAddress']) ? $params['EmailAddress'] : '',
        ));

        $result  = $connection->queryAll();
        $charset = new Charset();

        if ($result) {
            foreach ($result as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    $result[$k][$k1] = $charset->toUTF8($v1);
                }
            }
        } else
            $result = array();

        return $result;
    }

    /**
     * Фиксирует участника активности
     *
     * @param array $params
     *
     * @return array
     */
    public static function setCampaignActivity($params = array())
    {
        $sql = 'EXECUTE dbo.p_rapp_CAResponseSET
                    @ConsumerGUID =:ConsumerGUID,
                    @ActivityGUID =:ActivityGUID';

        $connection = Yii::app()->db->createCommand($sql);
        $connection->bindValues(array(
            ':ConsumerGUID' => !empty($params['ConsumerGUID']) ? $params['ConsumerGUID'] : '',
            ':ActivityGUID' => !empty(Yii::app()->params['activity']['guid']) ? Yii::app()->params['activity']['guid'] : '',
        ));

        try {
            $result = $connection->queryAll();
        } catch (Exception $e) {
            return $e;
        }

        $charset = new Charset();

        if ($result) {
            foreach ($result as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    $result[$k][$k1] = $charset->toUTF8($v1);
                }
            }
        } else
            $result = array();

        return $result;
    }

    /**
     * Проверяет существует ли учетка пользователя в CRM
     *
     * @internal param null $phone Номер телефона
     * @internal param null $password Пароль
     * @return array|mixed
     */
    public function isUserExists()
    {
        $user = null;

        if (!empty($this->phone)) {
            $params = array(
                'MobilePhone' => $this->phone,
                //'Password'    => $this->password
            );

            $user = $this->getUser($params);

        } else {
            $this->addError('phone', 'empty phone');
        }

        return $user;
    }

    /**
     * Получает пароли с сервера JUMP
     *
     * @return bool|null
     * @throws Exception
     */
    public function getPassword()
    {
        try{
            $data = $this->jumpRequest();
        } catch (Exception $e) {
            throw new Exception ($e->getMessage());
        }

        if($data->status == 0) {
            $password = $data->password;
        } else {
            throw new Exception ('jump error');
        }

        return $password;
    }

    /**
     * Возвращает количество крышек пользователя
     *
     * @return string
     * @throws Exception
     */
    public function getUserCountries()
    {
        try{
            $data = $this->jumpRequest();
        } catch (Exception $e) {
            throw new Exception ($e->getMessage());
        }

        if($data->status == 0) {
            $counrties = $data->countries;
        } else {
            throw new Exception ('jump error');
        }

        return $counrties;
    }

    /**
     * Запрос к серверу JUMP на получение данных пользователя
     *
     * @return bool|mixed
     * @throws Exception
     */
    public function jumpRequest()
    {
        if ($this->validate(array('phone'))) {
            try {
                $output = Yii::app()->curl->get(self::JUMP_API_URL, array('msisdn' => $this->phone, 'requestID' => microtime(true)));
            } catch (Exception $e) {
                throw new Exception ($e->getMessage());
            }
        } else {
            throw new Exception ('invalid phone');
        }

        if ($output) {
            $outputParsed = json_decode($output);

            // логирование
            $log             = new JumpPassword();
            $log->attributes = array(
                'RequestID'      => $outputParsed->requestID,
                'Msisdn'         => $outputParsed->msisdn,
                'Status'         => $outputParsed->status,
                'Result'         => $outputParsed->result,
                'Password'       => $outputParsed->password,
                'SmsParticipant' => $outputParsed->sms_participant,
                'Countries'      => $outputParsed->countries,
            );

            if ($log->validate()) {
                $log->save();
            } else {

            }

        } else {
            return false;
        }

        return $outputParsed;
    }

    /**
     * Проверяет валидность токена в JUMP
     *
     * @return null
     */
    public function checkValidToken()
    {
        $output = null;

        if ($this->validate('phone')) {
            $params = array(
                'phone' => $this->phone,
                'token' => $this->token,
            );

            $output = Yii::app()->curl->post(Yii::app()->createAbsoluteUrl('/api/checkValidToken'), $params);
        }

        return $output;
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array(
            'phone'    => 'Мобільний телефон',
            'password' => 'Пароль',
            'token'    => 'Токен',
        );
    }

    /**
     * Возвращает список активностей
     *
     * @return array
     */
    private function getActivityList()
    {
        return array('passport', 'stadiums', 'stadium');
    }
}