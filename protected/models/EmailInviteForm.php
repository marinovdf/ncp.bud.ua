<?php
class EmailInviteForm extends CFormModel
{
    public $email;
    public $friendName;
    public $body;

    private $_fromName;
    private $_fromEmail;
    public function __construct()
    {
        $this->_fromEmail = Yii::app()->user->email;

        $profile = new Profile();
        $this->_fromName = $profile->getFullName();
    }

    public function beforeValidate()
    {
        if($this->body) $this->body = strip_tags($this->body);

        if($this->email == $this->_fromEmail){
            $this->addError('email', 'Неможливо відправити повідомлення на власний e-mail');
        }

        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        return parent::afterValidate();
    }
    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // email, body are required
            array('email, body, friendName', 'required', 'message'=>'Не заповнене поле {attribute}'),

            array('friendName', 'length', 'max'=>50, 'tooLong'=>'Ім`я може містити не більше 50 символів'),
            array('body', 'length', 'max'=>1000, 'tooLong'=>'Повідомлення може містити не більше 1000 символів'),

            // email has to be a valid email address
            array('email', 'email', 'checkMX'=>true, 'message'=>'Email введено невірно'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'body'=>'Повідомлення',
            'email'=>'E-mail',
            'friendName'=>'Ім`я',
        );
    }

    /**
     *
     */
    public function resetForm()
    {
        foreach($this->attributes as $k=>$v)
            $this->$k = null;
    }


    /**
     * @param null $requestId
     *
     * @return Exception
     */
    public function sendInviteMail($requestId = null)
    {
        $mail = new Mail();

        $link = CHtml::link('BUD-тест',Yii::app()->createAbsoluteUrl('site/index', array('r_id'=>$requestId)));

        $content = "$this->body<br><br>
        Ти ще не в курсі?
        Щоб виграти грандіозний подарунок від Короля Пива, достатньо просто бути тобою!
        Пройди $link, визнач тип свого темпераменту та виграй подарунок, котрий ідеально пасуватиме саме тобі. Не забудь й про мене – прийми моє запрошення в тест і ми разом виграємо ящик освіжаючого BUD.";

        try{
            $mail->sendInviteMail($this->email, 'no-reply@bud.ua', $this->_fromName, 'BUD', $content, false);
        } catch (Exception $e){
            return $e;
        }

    }
}