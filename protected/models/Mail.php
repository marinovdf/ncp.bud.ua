<?php

class Mail
{

    function __construct()
    {

    }

    private function getSmtpConfig()
    {
        return array(
            "host"     => "localhost",                  //smtp сервер
            "debug"    => 1,                            //отображение информации дебаггера (0 - нет вообще)
            "auth"     => false,                        //сервер требует авторизации
            "port"     => 25,                           //порт (по-умолчанию - 25)
            "secure"   => "ssl",
            "username" => "",                           //имя пользователя на сервере
            "password" => "",                           //пароль
            "addreply" => "support@rappukraine.com",    //ваш е-mail
            "replyto"  => "support@rappukraine.com"     //e-mail ответа
        );
    }

    public function sendFeedbackMail($to, $from, $fromName='', $subject, $content, $attach=false)
    {
        $content .="<br/><br/>IP: ".$_SERVER['REMOTE_ADDR']."<br/>TIME: ".date('Y-m-d H:i:s');

        $smtp = $this->getSmtpConfig();

        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;

        $mail->IsSMTP(true);
        try {
            $mail->CharSet = 'utf-8';
            $mail->IsSendmail(true);
            $mail->Host       = $smtp['host'];
            $mail->SMTPDebug  = $smtp['debug'];
            $mail->Port       = $smtp['port'];
            $mail->FromName   = $fromName;
            $mail->AddReplyTo($smtp['addreply'], $smtp['username']);
            $mail->AddAddress($to);            //кому письмо
            $mail->SetFrom($from, $fromName);  //от кого (желательно указывать свой реальный e-mail на используемом SMTP сервере
            $mail->AddReplyTo($smtp['addreply'], $smtp['username']);
            $mail->Subject = htmlspecialchars($subject);
            $mail->MsgHTML($content);
            if($attach)  $mail->AddAttachment($attach);

            if($mail->Send())
                return true;
            else
                return false;

        } catch (phpmailerException $e) {
            echo $e->errorMessage();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return false;
    }

    /**
     * @param        $to
     * @param        $from
     * @param string $fromName
     * @param        $subject
     * @param        $content
     * @param bool   $attach
     *
     * @return bool|Exception|phpmailerException
     */
    public function sendInviteMail($to, $from, $fromName='', $subject, $content, $attach=false)
    {
        $smtp = $this->getSmtpConfig();

        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;

        $mail->IsSMTP(true);
        try {
            $mail->CharSet = 'utf-8';
            $mail->IsSendmail(true);
            $mail->Host       = $smtp['host'];
            $mail->SMTPDebug  = $smtp['debug'];
            $mail->Port       = $smtp['port'];
            $mail->FromName   = $fromName;
            $mail->AddReplyTo($smtp['addreply'], $smtp['username']);
            $mail->AddAddress($to);            //кому письмо
            $mail->SetFrom($from, $fromName);  //от кого (желательно указывать свой реальный e-mail на используемом SMTP сервере
            $mail->AddReplyTo($smtp['addreply'], $smtp['username']);
            $mail->Subject = htmlspecialchars($subject);
            $mail->MsgHTML($content);
            if($attach)  $mail->AddAttachment($attach);

            if($mail->Send())
                return true;
            else
                return false;

        } catch (phpmailerException $e) {
            return $e;
        } catch (Exception $e) {
            return $e;
        }

    }
}