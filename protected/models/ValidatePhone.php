<?php

/**
 * This is the model class for table "{{validatephone}}".
 *
 * The followings are the available columns in table '{{validatephone}}':
 *
 * @property string $PhoneNumber
 * @property string $ValidateCode
 * @property string $ValidateStatus
 * @property string $TransactionDateTime
 * @property string $ResponseText
 * @property string $ResponseStatus
 * @property string $MessageID
 */
class ValidatePhone extends CActiveRecord
{
    /**
     * Логин смс-провайдера
     */
    const PROVIDER_LOGIN    = 'rapp';
    /**
     * Пароль смсм-провайдера
     */
    const PROVIDER_PASSWORD = '6KiikMk907NBYUn';
    /**
     * Текст "от"
     */
    const TEXT_FROM         = 'BUD.UA';

    /**
     *
     * @var bool
     */
    public $userCode = false;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return ValidatePhone the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{validatephone}}';
    }


    /**
     * Подключение к базе
     *
     * @return mixed
     */
    public function getDbConnection()
    {
        return Yii::app()->mysqlDb;
    }

    /**
     * Действие перед валидацией данных
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->userCode != $this->ValidateCode)
            $this->addError('userCode', 'Код введено не вірно');

       /* if($this->checkCountCode()>3)
            $this->addError('userCode', 'Використана максимальна кількість спроб реєстрацій на добу');*/

        return parent::beforeValidate();
    }

    /**
     * Праввила валидации
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array( // array('userCode', 'compare', 'compareAttribute'=>'ValidateCode', 'operator'=>'=', 'message'=>'error code'),
            //array('PhoneNumber, ValidateCode, ValidateStatus', 'required'),
            //array('PhoneNumber', 'length', 'max'=>12),
            //array('ValidateCode', 'length', 'max'=>8),
            //array('ValidateStatus, ResponseText, ResponseStatus, MessageID', 'length', 'max'=>255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            //array('PhoneNumber, ValidateCode, ValidateStatus', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * Описания атрибутов
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'PhoneNumber'         => 'Phone Number',
            'ValidateCode'        => 'Validate Code',
            'ValidateStatus'      => 'Validate Status',
            'TransactionDateTime' => 'Transaction Date Time',
            'ResponseText'        => 'Response Text',
            'ResponseStatus'      => 'Response Status',
            'MessageID'           => 'Message ID',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('PhoneNumber', $this->PhoneNumber, true);
        $criteria->compare('ValidateCode', $this->ValidateCode, true);
        $criteria->compare('ResponseText', $this->ResponseText, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Формирует смс
     * @param $phone
     * @param $text
     *
     * @return bool|string
     */
    public function sendSms($phone, $text)
    {
        $from  = self::TEXT_FROM;
        $to    = $phone;
        $start = "";
        $xml   = "<message><service id='single' validity='+2 hour' source='$from' start='$start'/><to>$to</to><body content-type='plain/text' encoding='plain'>$text</body></message>";
        $answ  = $this->post_request($xml, 'http://bulk.startmobile.com.ua/clients.php');

        return $answ;
    }

    /**
     * Отпрвляет смс
     * @param $data
     * @param $url
     *
     * @return bool|string
     */
    public function post_request($data, $url)
    {
        $login   = self::PROVIDER_LOGIN;
        $pwd     = self::PROVIDER_PASSWORD;

        Yii::trace(CVarDumper::dumpAsString($login),'vardump');
        Yii::trace(CVarDumper::dumpAsString($pwd),'vardump');

        $credent = sprintf('Authorization: Basic %s', base64_encode($login . ":" . $pwd));
        $params  = array('http' => array('method' => 'POST', 'content' => $data, 'header' => $credent));
        $ctx     = @stream_context_create($params);
        $fp      = @fopen($url, 'rb', false, $ctx);

        if ($fp) {
            $response = stream_get_contents($fp);
            $this->parseResponse($response);
        } else
            $response = false;

        return $response;
    }

    /**
     * Парсит ответ от провайдера
     *
     * @param $xmlResponse
     *
     * @return bool
     */
    public function parseResponse($xmlResponse)
    {
        if (!$xmlResponse)
            return false;

        $parsed = simplexml_load_string($xmlResponse);

        $this->ResponseText   = $parsed->state->attributes()->error;
        $this->ResponseStatus = $parsed->state;
        $this->MessageID      = $parsed->id;

        $this->saveCode();
    }

    /**
     *
     * Сохраняет код в базе
     *
     */
    public function saveCode()
    {
        $command = $this->dbConnection->createCommand()->
            insert($this->tableName(), array(
                'PhoneNumber'    => $this->PhoneNumber,
                'ValidateCode'   => $this->ValidateCode,
                'ValidateStatus' => 'send',
                'ResponseText'   => $this->ResponseText,
                'ResponseStatus' => $this->ResponseStatus,
                'MessageID'      => $this->MessageID,
                'IP'             => Yii::app()->request->getUserHostAddress()
            ));

    }

    /**
     * Сохраняем статус валидации
     *
     */
    public function saveSuccess()
    {
        $command = $this->dbConnection->createCommand()->
            insert($this->tableName(), array(
                'PhoneNumber'    => $this->PhoneNumber,
                'ValidateCode'   => $this->ValidateCode,
                'ValidateStatus' => 'validate',
                'IP'             => Yii::app()->request->getUserHostAddress()
            ));
    }

    /**
     * Проверяем количество попыток регистраций
     *
     * @return mixed
     */
    public function checkCountCode()
    {
        $command = $this->dbConnection->createCommand()
            ->select('count(*) as count')
            ->from($this->tableName())
            ->where('PhoneNumber =:PhoneNumber AND date(TransactionDateTime)=date(now())', array(':PhoneNumber' => $this->PhoneNumber))
            ->queryRow();

        return $command['count'];
    }


    /**
     * Генерирует код валидации
     * @return string
     */
    function genCode()
    {
        $number     = '';
        $ar_numbers = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"); //Набор символов для генерации
        shuffle($ar_numbers); //Перебор символов

        for ($a = 1; $a < 7; $a++) { //Цикл перебора, обозначающий длину регистра кода
            $ar_rand = array_rand(array_flip($ar_numbers), 2); //
            $number .= $ar_rand[0]; //
        }

        return $number;
    }
}