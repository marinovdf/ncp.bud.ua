<?php

/**
 * This is the model class for table "{{reminder_password}}".
 *
 * The followings are the available columns in table '{{reminder_password}}':
 * @property integer $id
 * @property string $Phone
 * @property string $operatorCode
 * @property string $Text
 * @property string $ProviderResponse
 * @property string $TransactionDateTime
 */
class ReminderPassword extends CActiveRecord
{
    public $verifyCode;
    public $operatorCode;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ReminderPassword the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->mysqlDb;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{reminder_password}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Phone', 'required'),
			array('operatorCode', 'required'),
			array('Phone', 'length', 'max'=>12),
			array('Text', 'length', 'max'=>50),
			array('ProviderResponse', 'length', 'max'=>255),
            array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'message'=>'Код введено невірно'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Phone, Text, ProviderResponse, operatorCode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Phone' => 'Телефон',
			'Text' => 'Text',
			'ProviderResponse' => 'Provider Response',
			'TransactionDateTime' => 'Transaction Date Time',
            'verifyCode'=>'Код перевірки',
            'operatorCode'=>'Код оператора',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Phone',$this->Phone,true);
		$criteria->compare('Text',$this->Text,true);
		$criteria->compare('ProviderResponse',$this->ProviderResponse,true);
		$criteria->compare('TransactionDateTime',$this->TransactionDateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Возвращает список кодов операторов
     * @return array
     */
    public function getOperatorCodeList()
    {
        return array(
            '38039' => '039',
            '38050' => '050',
            '38063' => '063',
            '38066' => '066',
            '38067' => '067',
            '38068' => '068',
            '38091' => '091',
            '38092' => '092',
            '38093' => '093',
            '38094' => '094',
            '38095' => '095',
            '38096' => '096',
            '38097' => '097',
            '38098' => '098',
            '38099' => '099',
        );
    }

    /**
     * Проводит дедублиикацию пользователя по номеру телефона
     *
     * Если совпадают два из трех полей (дата рождения, имейл, телефон),
     * то пользователь существует/не существует и его можно создать/апдейтнуть,
     *
     * иначе
     *
     * пользователь с таким номером телефона существует, но его нельза апдейтнуть
     *
     * @return void
     */
    public function isPhoneExists()
    {
        $user = new Users();

        $users = $user->getUsers(array(
            'MobilePhone'=>$this->operatorCode.$this->Phone
        ));

        if(!empty($users))
        {
            Yii::trace(CVarDumper::dumpAsString($users),'vardump');

            return $users;
        } else {
            return false;
        }

        return;
    }
}