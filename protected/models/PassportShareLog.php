<?php

/**
 * This is the model class for table "{{passport_share_log}}".
 *
 * The followings are the available columns in table '{{passport_share_log}}':
 * @property integer $id
 * @property string $UserGUID
 * @property string $Phone
 * @property string $PostID
 * @property string $TransactionDateTime
 */
class PassportShareLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PassportShareLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->mysqlDb;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{passport_share_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UserGUID', 'required'),
			array('UserGUID', 'length', 'max'=>36),
			array('Phone', 'length', 'max'=>12),
			array('PostID', 'length', 'max'=>50),
			array('TransactionDateTime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, UserGUID, Phone, PostID, TransactionDateTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'UserGUID' => 'User Guid',
			'Phone' => 'Phone',
			'PostID' => 'Post',
			'TransactionDateTime' => 'Transaction Date Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('UserGUID',$this->UserGUID,true);
		$criteria->compare('Phone',$this->Phone,true);
		$criteria->compare('PostID',$this->PostID,true);
		$criteria->compare('TransactionDateTime',$this->TransactionDateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}