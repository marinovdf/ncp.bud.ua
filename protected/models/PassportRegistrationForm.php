<?php

/**
 * Class PassportRegistrationForm
 * Форма регистрации для активности Passport2Rio
 */
class PassportRegistrationForm extends RegistrationForm
{
    public $cityModel;

    //Default constants
    protected $ACTIVITY_NAME         = 'NCP2014-Passport2Rio';
    protected $TRANSACTION_CODE      = '';
    protected $TRANSACTION_TYPE_CODE = 'RS';
    protected $ACTIVITY_TYPE_CODE    = 'SW';
    protected $MEDIA_CHANNEL_CODE    = 'WB';
    protected $INBOUND_RDS           = 'Standard survey';
    protected $INBOUND_RC            = 'Standard survey';
    protected $SURVEY_ID             = 1;
    protected $COUNTRY_CODE          = 'UKR';
    protected $SOURCE_TYPE_CODE      = 'WEB';
    protected $CUR_BRAND_CODE        = 1795;
    protected $ADDRESS_STATUS        = 'VALID';
    protected $AGE_STATUS            = 'VALID';
    protected $IMPORT_FILE_ID        = null;

    public function beforeValidate()
    {
        $this->MobilePhone = trim($this->MobilePhone);

        if ($this->Birthday == '0000.00.00')
            $this->Birthday = null;

        if ($this->FirstName) {
            $this->FirstName = $this->setFirstLetterToUpper($this->FirstName);
        }

        if ($this->LastName) {
            $this->LastName = $this->setFirstLetterToUpper($this->LastName);
        }

        if ($this->Patronymic) {
            $this->Patronymic = $this->setFirstLetterToUpper($this->Patronymic);
        }

        $jump = new Api();
        $jump->phone = $this->operatorCode.$this->MobilePhone;

        if($jump->validate(array('phone'))){
            //Получаем пароль у джампа
            $this->Password = $jump->getPassword();

            //Получаем количесво крышек от джампа
            $countries = 0;

            try{
                $countries = $jump->getUserCountries();
            } catch (Exception $e) {
                $this->addError('Phone', 'Помилка бази даних');
            }

            if($countries < 8){
                $this->addError('Phone', 'Доступ заборонений');
            }
        }

        return parent::beforeValidate();
    }
}