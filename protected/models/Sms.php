<?php
class Sms extends CModel
{
    /**
     * Логин смс-провайдера
     */
    const PROVIDER_LOGIN    = 'rapp';
    /**
     * Пароль смсм-провайдера
     */
    const PROVIDER_PASSWORD = '6KiikMk907NBYUn';
    /**
     * Текст "от"
     */
    const TEXT_FROM         = 'BUD.UA';

    /**
     *
     * @var bool
     */
    public $userCode = false;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return ValidatePhone the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Формирует смс
     * @param $phone
     * @param $text
     *
     * @return bool|string
     */
    public function sendSms($phone, $text)
    {
        $from  = self::TEXT_FROM;
        $to    = $phone;
        $start = "";
        $xml   = "<message><service id='single' validity='+2 hour' source='$from' start='$start'/><to>$to</to><body content-type='plain/text' encoding='plain'>$text</body></message>";
        $answ  = $this->post_request($xml, 'http://bulk.startmobile.com.ua/clients.php');

        return $answ;
    }

    /**
     * Отпрвляет смс
     * @param $data
     * @param $url
     *
     * @return bool|string
     */
    public function post_request($data, $url)
    {
        $login   = self::PROVIDER_LOGIN;
        $pwd     = self::PROVIDER_PASSWORD;


        $credent = sprintf('Authorization: Basic %s', base64_encode($login . ":" . $pwd));
        $params  = array('http' => array('method' => 'POST', 'content' => $data, 'header' => $credent));
        $ctx     = @stream_context_create($params);
        $fp      = @fopen($url, 'rb', false, $ctx);

        if ($fp) {
            $response = stream_get_contents($fp);
            $this->parseResponse($response);
        } else
            $response = false;

        return $response;
    }

    /**
     * Парсит ответ от провайдера
     *
     * @param $xmlResponse
     *
     * @return bool
     */
    public function parseResponse($xmlResponse)
    {
        if (!$xmlResponse)
            return false;
        else
            return simplexml_load_string($xmlResponse);
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        // TODO: Implement attributeNames() method.
    }
}