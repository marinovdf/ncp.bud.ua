<?php

/**
 * Class Anketa
 * Модель для работы с Анкетой
 *
 * @property mixed image
 */
class Anketa extends PassportAnketa
{
    public $result;
    public $stepId;
    public $stepIndex;
    public $image;
    private $_points;
    private $_questionnaireStepId;

    public function init()
    {
        $this->_points = $this->getPointsList();

        parent::init();
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('UserGUID, QuestionID, Points, Answer', 'required'),
            array('Points', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 100),
            array('QuestionID', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 8),
            //array('QuestionID', 'validateQuestionID'),
            array('UserGUID', 'match', 'pattern' => '/^[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}$/'),
            array('Phone', 'match', 'pattern' => '/^380+(67|68|96|97|98|50|66|95|99|63|93)+\d{7}$/'),
            array('QuestionStatus', 'length', 'max' => 50),
            array('InputText', 'length', 'max' => 500),
            array('Answer', 'length', 'max' => 5000),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, UserGUID, Phone, QuestionID, QuestionStatus, Answer, InputText, Points', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Проверят наличие в таблице записи с ответом
     *
     * @return CActiveRecord
     */
    public function isExistsQuestion()
    {
        $existsQuestion = $this->find('UserGUID=:UserGUID AND QuestionID=:QuestionID AND QuestionStatus IS NULL', array(
            ':UserGUID'   => $this->UserGUID,
            ':QuestionID' => $this->QuestionID
        ));

        return $existsQuestion;
    }

    /**
     * Валидатор QuestionID
     *
     * @param $attribute
     * @param $params
     */
    public function validateQuestionID($attribute, $params)
    {
        if ($this->isExistsQuestion()) {
            $this->addError($attribute, 'question is exists');
        }
    }

    /**
     * Сохраняет либо обновляет поля в базе
     *
     * @param bool $runValidation
     * @param null $attributes
     *
     * @return bool|int
     */
    public function save($runValidation = true, $attributes = null)
    {
        // Если такая запись уже есть с базе - обновляем
        if ($row = $this->isExistsQuestion()) {
            $this->id = $row['id'];

            return parent::updateByPk($this->id, $this->attributes);
        } else {
            return parent::save($runValidation, $attributes);
        }
    }

    /**
     * Возвращает массив баллов
     *
     * @return array
     */
    protected function getPointsList()
    {
        return array(
            //Уявімо, що завтра ти вирушаєш до Ріо-де-Жанейро. Що приваблює тебе найбільше в майбутній подорожі? (Вибери один із можливих варіантів)
            1 => array(
                1 => 0, // Шанс побачити запальну Бразилію
                2 => 0, // Думка про те, що я обраний серед багатьох, додає наснаги
                3 => 8, // Тільки заради фотографії на фоні легендарного Стадіону Чемпіонату Світу варто їхати до Ріо-де-Жанейро
                4 => 2, // Несподівані знайомства та неочікувані зустрічі зроблять мою подорож незабутньою
            ),

            2 => array(
                1 => 0
            ), //Обери соціальні мережі, якими ти користуєшся
            3 => array(
                1 => 5, // Facebook
                2 => 2, // ВКонтакте
                3 => 3, // Twitter
                4 => 3, // Instagram
                5 => 1, // Google+
                6 => 0, // Не маю сторінки у соціальних мережах
            ),
            //Король Пива переконаний: твого запалу та енергії вистачає на багацько цікавих справ. Як ти любиш проводити свій вільний час? (Обери, будь ласка, три позиції)
            4 => array(
                1 => 4, // За келихом пива у дружній компанії
                2 => 4, // Свій вільний час я присвячую спорту (будь ласка, уточни)
                3 => 2, // Якщо я маю вільну хвилинку, я терміново підключаюсь до Інтернету
                4 => 0, // Я ніколи не втомлююсь займатись шопінгом
                5 => 0, // Перегляд телепрограм забирає у мене весь вільний час
                6 => 0, // Мені ніколи не набридає слухати улюблену музику
                7 => 1, // Нічні клуби – це моя стихія
                8 => 5, // Я обожнюю дивитись футбольні матчі
                9 => 4, // У вільний від навчання й роботи час я згадую про своє хоббі (уточни, будь ласка)
            ),
            //Прояви себе. Завантаж фотографію, яка характеризує тебе найкраще.
            5 => array(
                1 => 5
            ),

            6 => array(
                1 => 6
            ),
            //Ти вже став фаном офіційної сторінки BUD на Facebook?
            7 => array(
                1 => 3, // Так
                2 => 0, // Так
            ),
            8 => array(
                1 => 0,
                2 => 0,
                3 => 0,
            )
        );
    }

    /**
     * Возвращает данные пользователя по игре
     * todo дописать метод
     *
     * @return array
     */
    public function getSessionData()
    {
        $quiz = new Quiz();

        $data = $this->findAll('UserGUID=:UserGUID', array(':UserGUID' => $this->UserGUID));

        $questionnaireAnswers = array();
        $questionnaireStepId  = array();

        foreach ($data as $item) {

            if (in_array($item['QuestionID'], array(3, 4, 8))) {
                $answersInDb   = unserialize($item['Answer']);
                $answersParsed = array();

                foreach ($answersInDb as $ans) {
                    $answersParsed[] = $ans;
                }

                $result = array(
                    'answers' => $answersParsed,
                    'passed'  => true
                );
            } elseif ($item['QuestionID'] == 5) {

                $ans = unserialize($item['Answer']);
                $result = array(
                    'cropRect' => $ans['cropRect'],
                    'imageURL' => $ans['imageURL'],
                    'passed'   => true
                );

                //$result = $ans;
            } else {
                $result = array(
                    'answers' => array(
                        array(
                            'id'        => $item['Answer'],
                            'inputText' => $item['InputText']
                        )
                    ),
                    'passed'  => true
                );
            }

            $questionnaireAnswers[] = array(
                'stepId' => $item['QuestionID'],
                'result' => $result
            );

            $questionnaireStepId[] = $item['QuestionID'];
        }

        $output = array(
            'status' => 0,
            'data'   => array(
                'userId'                 => $this->UserGUID,
                'questionnaireStepId'    => $this->getQuestionnaireStepId(),
                'questionnaireAnswers'   => $questionnaireAnswers,
                'questionnaireDataSaved' => $this->getQuestionnaireDataSavedStatus(),
                'questionnaireDataSent'  => $this->getQuestionnaireDataSavedStatus(),
                'quizStepId'             => $quiz->getQuizStepId(),
                'quizStepIndex'          => $quiz->quizStepIndex(),
                'quizScore'              => $quiz->getQuizScore(),
                'quizAnswersCount'       => $quiz->getQuizAnswersCorrectCount()
            ),
        );

        return $output;
    }

    /**
     * Изменяет текущий этап анкеты
     * todo дописать метод
     *
     * @return array
     */
    public function setQuestionnaireStep()
    {
        try {
            Yii::app()->user->setState("questionnaireStepId", $this->stepId);
            $output = array(
                'status'  => 0,
            );
        } catch (Exception $e) {
            $output = array(
                'status'  => 1,
                'message' => $e->getMessage()
            );
        }

        return $output;
    }

    /**
     * Установка результата для текущего этапа анкеты
     * todo дописать метод
     *
     * @return array
     */
    public function setQuestionnaireStepResult()
    {
        // Если пришел флаг для записи в базу
        if ($this->result->passed) {

            // если пришла картинка
            if (!empty($this->result->croppedImage)) {
                $this->QuestionID = $this->stepId;
                //$this->Answer     = serialize(array('cropRect' => (array)$this->result->cropRect, 'imageUrl' => $this->result->imageURL));
                $this->Answer     = serialize((array)$this->result);
                $this->Points     = $this->_points[5][1];
            } else {
                // если больше одного ответа
                if (in_array($this->stepId, array(3, 4, 8))) {
                    $this->QuestionID = $this->stepId;
                    $this->Answer     = $this->getMultiAnswers($this->result->answers);
                    $this->Points     = $this->getMultiPoints($this->result->answers);
                    $this->InputText  = (string)$this->result->answers[0]->inputText;
                } else {
                    $this->QuestionID = $this->stepId;
                    $this->Answer     = $this->result->answers[0]->id;
                    $this->Points     = $this->_points[$this->QuestionID][$this->Answer];
                    $this->InputText  = (string)$this->result->answers[0]->inputText;
                }
            }

            if ($this->validate()) {

                try {
                    $this->save();
                    $output = array(
                        'status' => 0,
                    );
                } catch (Exception $e) {
                    $output = array(
                        'status'  => 1,
                        'message' => $e->getMessage(),
                    );
                }

            } else {
                $output = array(
                    'status'  => 1,
                    'message' => $this->getErrors(),
                );
            }
        } else {
            $output = array(
                'status' => 0,
            );
        }

        return $output;
    }

    /**
     * Сохранение анкеты после заполнения
     * todo дописать метод
     *
     * @return array
     */
    public function saveQuestionnaireData()
    {
        $output = array(
            'status' => 0,
        );

        return $output;
    }

    /**
     * Отправка анкеты после заполнения
     *
     * @return array
     */
    public function sendQuestionnaireData()
    {
        $output = array(
            'status' => 0,
        );

        try {
            $this->updateAll(array('QuestionStatus' => 1), 'UserGUID =:UserGUID', array(':UserGUID' => $this->UserGUID));
        } catch (Exception $e) {
            $output = array(
                'status'  => 1,
                'message' => $e->getMessage()
            );
        }

        return $output;
    }

    /**
     * Сохранение изображений для анкеты
     * todo дописать метод
     *
     * @return array
     */
    public function sendQuestionnaireImage()
    {
        $output = array(
            'status' => 0,
            'data'   => array(
                'imageURL' => Yii::app()->getBaseUrl(true) . '/media/images/' . $this->UserGUID . '.png'
            )
        );

        return $output;
    }

    /**
     * Смена текущего этапа викторины
     * todo дописать метод
     *
     * @return array
     */
    public function setQuizStep()
    {
        $output = array(
            'status' => 0,
        );

        return $output;
    }

    /**
     * Установка результата для текущего этапа викторины
     * todo дописать метод
     *
     * @return array
     */
    public function setQuizStepResult()
    {
        $quiz = new Quiz();

        if ($this->stepId == 15) {
            $quiz->UserGUID   = $this->UserGUID;
            $quiz->StepID     = $this->stepId;
            $quiz->StepIndex  = $this->stepIndex;
            $quiz->QuestionID = $this->result->questionId;
            $quiz->Answer     = $this->getMultiAnswers($this->result->answers);
            $quiz->Points     = $quiz->getMultiPoints($this->result->answers);
        } else {
            $quiz->UserGUID   = $this->UserGUID;
            $quiz->StepID     = $this->stepId;
            $quiz->StepIndex  = $this->stepIndex;
            $quiz->QuestionID = $this->result->questionId;
            $quiz->Answer     = $this->result->answers[0]->id;
            $quiz->Points     = $quiz->getCorrectAnswer() ? $quiz->getCorrectPoints() : 0;
        }

        if ($quiz->validate()) {

            try {
                $quiz->save();

                if($this->stepId == 15){
                    $output = array(
                        'status' => 0,
                        'data'   => array(
                            'score'   => $quiz->Points,
                            'correct' => !empty($quiz->Points) ? true : false
                        )
                    );
                } else {
                    $output = array(
                        'status' => 0,
                        'data'   => array(
                            'score'   => $quiz->getCorrectAnswer() ? $quiz->getCorrectPoints() : 0,
                            'correct' => $quiz->getCorrectAnswer()
                        )
                    );
                }

            } catch (Exception $e) {
                $output = array(
                    'status'  => 1,
                    'message' => $e->getMessage(),
                );
            }

        } else {
            $output = array(
                'status'  => 1,
                'message' => $quiz->getErrors(),
            );
        }

        return $output;
    }

    /**
     * Возвращает id этапа
     *
     * @return mixed
     */
    public function getQuestionnaireStepId()
    {
        $data = $this->dbConnection->createCommand()
            ->select('MAX(QuestionID)')
            ->from($this->tableName())
            ->where('UserGUID=:UserGUID', array(':UserGUID' => $this->UserGUID))
            ->queryScalar();

        if($data == 0) {
            $step = null;
        } elseif($data == 8) {
            $step = 8;
        } else {
            $step = $data + 1;
        }

        return $step;
    }

    /**
     * Сериализует несколько ответов для базы
     *
     * @param $data
     *
     * @return string
     */
    protected function getMultiAnswers($data)
    {
        $answers = array();

        if (!empty($data)) {
            foreach ($data as $answer) {
                $answers[] = (array)$answer;
            }
        }

        return serialize($answers);
    }

    /**
     * Считает баллы за несколько ответов
     *
     * @param $data
     *
     * @return int
     */
    protected function getMultiPoints($data)
    {
        $points = 0;

        if (!empty($data)) {
            foreach ($data as $answer) {
                $points = $points + $this->_points[$this->QuestionID][$answer->id];
            }
        }

        return $points;
    }

    /**
     * Сохраняет картинку
     *
     * @return bool
     * @throws Exception
     */
    public function saveImage()
    {
        if ($this->image['error'] == 0) {

            if (mime_content_type($this->image['tmp_name']) == 'image/png') {
                try {
                    move_uploaded_file($this->image['tmp_name'], Yii::app()->basePath . "/../media/images/$this->UserGUID.png");
                } catch (Exception $e) {
                    throw new Exception('Could not save image');
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * Тестовый метод очистки данных
     *
     * @return array
     */
    public function clearData()
    {
        try {
            $quiz = new Quiz();
            $this->deleteAll('UserGUID =:UserGUID', array(':UserGUID' => $this->UserGUID));
            $quiz->deleteAll('UserGUID =:UserGUID', array(':UserGUID' => $this->UserGUID));
            $output = array(
                'status'  => '0',
                'message' => 'cleared'
            );
        } catch (Exception $e) {
            $output = array(
                'status' => '1',
            );
        }

        return $output;
    }

    /**
     * Возвращает статус прохождения анкеты
     *
     * @return bool
     * @throws Exception
     */
    public function getQuestionnaireDataSavedStatus()
    {
        $data = $this->dbConnection->createCommand()
            ->select('count(QuestionStatus)')
            ->from($this->tableName())
            ->where('UserGUID=:UserGUID AND QuestionStatus = 1', array(':UserGUID' => $this->UserGUID))
            ->queryScalar();

        if ($data >= 8) {
            //throw new Exception ('Invalid number of questions');
            return true;
        } elseif ($data < 8) {
            return false;
        } else {
            return true;
        }
    }
}