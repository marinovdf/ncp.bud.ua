<?php

class ReminderForm extends CFormModel
{
    public $verifyCode;
    public $operatorCode;
    public $Phone;
    public $mobilePhone;
    public $password;
    public $ResponseText;
    public $ResponseStatus;
    public $MessageID;

    /**
     * Логин смс-провайдера
     */
    const PROVIDER_LOGIN = 'rapp';
    /**
     * Пароль смсм-провайдера
     */
    const PROVIDER_PASSWORD = '6KiikMk907NBYUn';
    /**
     * Текст "от"
     */
    const TEXT_FROM = 'BUD.UA';

    protected function beforeValidate()
    {
        if($this->getReminderCount() >= 3){
            $this->addError('', 'Ви вже використали максимальну кількість спроб на добу!');

        }

        return parent::beforeValidate();
    }


    protected function afterValidate()
    {
        if (!$this->getErrors()) {
            $this->password = $this->getPassword();

            if ($this->password) {
                $this->sendPassword();
            }
        }
        parent::afterValidate();
    }


    public function sendPassword()
    {
        if ($this->sendSms($this->operatorCode . $this->Phone, $this->password)) {
            Yii::app()->user->setFlash('ShowPopup', array('title' => 'Повідомлення', 'content' => 'Пароль надіслано'));
        } else {
            $this->addError('', 'Виникла помилка');
        }
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Phone, operatorCode', 'required'),
            array('Phone', 'length', 'is' => 7, 'message' => '{attribute} повинен містити 7 цифр'),

            // Только цифри
            array('Phone, operatorCode', 'numerical', 'message' => '{attribute} повинен містити лише цифри'),
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'message' => 'Код введено невірно'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'Phone'        => 'Телефон',
            'verifyCode'   => 'Код перевірки',
            'operatorCode' => 'Код оператора',
        );
    }


    /**
     * Возвращает список кодов операторов
     * @return array
     */
    public function getOperatorCodeList()
    {
        return array(
            '38039' => '039',
            '38050' => '050',
            '38063' => '063',
            '38066' => '066',
            '38067' => '067',
            '38068' => '068',
            '38091' => '091',
            '38092' => '092',
            '38093' => '093',
            '38094' => '094',
            '38095' => '095',
            '38096' => '096',
            '38097' => '097',
            '38098' => '098',
            '38099' => '099',
        );
    }

    /**
     * Проводит дедублиикацию пользователя по номеру телефона
     *
     * Если совпадают два из трех полей (дата рождения, имейл, телефон),
     * то пользователь существует/не существует и его можно создать/апдейтнуть,
     *
     * иначе
     *
     * пользователь с таким номером телефона существует, но его нельза апдейтнуть
     *
     * @return mixed
     */
    private function isPhoneExists()
    {
        $user = new Users();

        $users = $user->getUsers(array(
            'MobilePhone' => $this->operatorCode . $this->Phone
        ));

        if (!empty($users)) {
            return $users;
        } else {
            $this->addError('', 'Користувач з таким мобільним телефоном не зареєстрований');
        }

        return false;
    }

    /**
     * @return bool
     */
    private function getPassword()
    {
        if ($user = $this->isPhoneExists()) {
            return $user[0]['Password'];
        }

        return false;
    }

    /**
     * Формирует смс
     * @param $phone
     * @param $text
     *
     * @return bool|string
     */
    public function sendSms($phone, $text)
    {
        $from  = self::TEXT_FROM;
        $to    = $phone;
        $start = "";
        $xml   = "<message><service id='single' validity='+2 hour' source='$from' start='$start'/><to>$to</to><body content-type='plain/text' encoding='plain'>$text</body></message>";
        $answ  = $this->post_request($xml, 'http://bulk.startmobile.com.ua/clients.php');

        return $answ;
    }

    /**
     * Отпрвляет смс
     * @param $data
     * @param $url
     *
     * @return bool|string
     */
    public function post_request($data, $url)
    {
        $login = self::PROVIDER_LOGIN;
        $pwd   = self::PROVIDER_PASSWORD;

        $credent = sprintf('Authorization: Basic %s', base64_encode($login . ":" . $pwd));
        $params  = array('http' => array('method' => 'POST', 'content' => $data, 'header' => $credent));
        $ctx     = @stream_context_create($params);
        $fp      = @fopen($url, 'rb', false, $ctx);

        if ($fp) {
            $response = stream_get_contents($fp);
            $this->parseResponse($response);
        } else
            $response = false;

        return $response;
    }

    /**
     * Парсит ответ от провайдера
     *
     * @param $xmlResponse
     *
     * @return bool
     */
    public function parseResponse($xmlResponse)
    {
        if (!$xmlResponse)
            return false;

        $parsed = simplexml_load_string($xmlResponse);

        $this->ResponseText   = $parsed->state->attributes()->error;
        $this->ResponseStatus = $parsed->state;
        $this->MessageID      = $parsed->id;

        $this->saveCode();
    }

    /**
     *
     * Сохраняет код в базе
     *
     */
    public function saveCode()
    {
        $command = Yii::app()->mysqlDb->createCommand()->
            insert('profiling_reminder_password', array(
                'Phone'            => $this->operatorCode . $this->Phone,
                'ProviderResponse' => $this->ResponseStatus,
                'Text'             => $this->password,
                'IP'               => Yii::app()->request->getUserHostAddress()
            ));
    }

    public function getReminderCount()
    {
        $command = Yii::app()->mysqlDb->createCommand()->
            select(array('count(*)'))->
            from('profiling_reminder_password')->
            where('IP LIKE :IP AND TransactionDateTime >  DATE_SUB( NOW( ) , INTERVAL 24 HOUR )', array(':IP' => Yii::app()->request->getUserHostAddress()))->
            queryScalar();

        return $command;
    }
}