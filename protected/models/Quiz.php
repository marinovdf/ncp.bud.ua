<?php

/**
 * Class PassportQuiz
 * Модель для работы с Квизом
 */
class Quiz extends PassportQuiz
{

    /**
     * Массив баллов за правильные ответы
     * @var
     */
    private $_correctPointsList;
    private $_correctAnswerList;

    public function init()
    {
        $this->_correctPointsList = $this->getCorrectPointList();
        $this->_correctAnswerList = $this->getPointsList();
        parent::init();
    }

    /**
     * Возвращает массив возможных баллов
     *
     * @return array
     */
    protected function getPointsList()
    {
        return array(
            9  => array(
                1  => 4,
                2  => 5,
                3  => 5,
                4  => 4,
                5  => 5,
                6  => 4,
                7  => 4,
                8  => 3,
                9  => 5,
                10 => 2,
                11 => 5,
                12 => 1,
                13 => 2,
                14 => 5,
                15 => 1,
                16 => 2,
                17 => 5,
                18 => 4,
                19 => 4,
                20 => 3,
                21 => 4,
                22 => 5,
                23 => 4,
                24 => 1,
            ),
            10 => array(
                1  => 3,
                2  => 3,
                3  => 1,
                4  => 5,
                5  => 4,
                6  => 3,
                7  => 5,
                8  => 6,
                9  => 1,
                10 => 6,
                11 => 4,
                12 => 5,
                13 => 2,
                14 => 3,
                15 => 5,
                16 => 3,
                17 => 4,
                18 => 3,
                19 => 2,
                20 => 5,
                21 => 3,
                22 => 4,
                23 => 4,
                24 => 4
            ),
            11 => array(
                1  => 4,
                2  => 3,
                3  => 3,
                4  => 3,
                5  => 3,
                6  => 4,
                7  => 2,
                8  => 4,
                9  => 1,
                10 => 2,
                11 => 3,
                12 => 2,
                13 => 1,
                14 => 1,
                15 => 4,
                16 => 4,
                17 => 4,
                18 => 4,
                19 => 2,
                20 => 4,
                21 => 3,
                22 => 3,
                23 => 4,
                24 => 3
            ),
            12 => array(
                1 => 5
            ),
            13 => array(
                1 => 3
            ),
            14 => array(
                1 => 1
            ),
            15 => array(
                1 => 1
            ),
            16 => array(
                1 => 2
            ),
            17 => array(
                1 => 1
            )
        );
    }

    /**
     * Возвращает массива баллов за правильный ответ
     *
     * @return array
     */
    public function getCorrectPointList()
    {
        return array(
            9  => 6,
            10 => 6,
            11 => 8,
            12 => 4,
            13 => 4,
            14 => 4,
            15 => 4,
            16 => 4,
            17 => 4
        );
    }

    /**
     * Возвращает баллы за правильный ответ
     *
     * @return int
     */
    public function getCorrectPoints()
    {
        $points = !empty($this->_correctPointsList[$this->StepID]) ? $this->_correctPointsList[$this->StepID] : 0;

        return $points;
    }

    /**
     * Возвращает флаг правильности ответа
     *
     * @return bool
     */
    public function getCorrectAnswer()
    {
        $correct = $this->_correctAnswerList[$this->StepID][$this->QuestionID] == $this->Answer ? true : false;

        return $correct;
    }

    /**
     * Возвращает количество баллов пользователя
     *
     * @return mixed
     */
    public function getQuizScore()
    {
        $score = $this->dbConnection->createCommand()
            ->select('SUM(Points)')
            ->from($this->tableName())
            ->where('UserGUID =:UserGUID', array(':UserGUID' => Yii::app()->user->data->ConsumerGUID))
            ->queryScalar();

        return (int)$score;
    }

    /**
     * Возвращает максимальное значение StepIndex
     *
     * @return int
     */
    public function quizStepIndex()
    {
        $score = $this->dbConnection->createCommand()
            ->select('MAX(StepIndex)')
            ->from($this->tableName())
            ->where('UserGUID =:UserGUID', array(':UserGUID' => Yii::app()->user->data->ConsumerGUID))
            ->queryScalar();

        return (int)$score;
    }

    /**
     * Возврацает количество отвеченных вопросов в квизе
     *
     * @return int
     */
    public function getQuizAnswersCorrectCount()
    {
        $score = $this->dbConnection->createCommand()
            ->select('COUNT(Answer)')
            ->from($this->tableName())
            ->where('UserGUID =:UserGUID AND Points>0', array(':UserGUID' => Yii::app()->user->data->ConsumerGUID))
            ->queryScalar();

        return (int)$score;
    }

    /**
     * @return bool
     */
    public function isExistsQuizAnswers()
    {
        $exists = $this->exists('UserGUID =:UserGUID', array(':UserGUID' => Yii::app()->user->data->ConsumerGUID));

        return $exists;
    }

    public function getQuizStepId()
    {
        $stepIndex = $this->quizStepIndex();

        switch ($stepIndex) {
            case 0 :
                $stepId = $this->isExistsQuizAnswers() ? 10 : 9;
                break;
            case 1 :
                $stepId = 11;
                break;
            case 2 :
                $stepId = 12;
                break;
            case 3 :
                $stepId = 16;
                break;
            case 4 :
                $stepId = 18;
                break;
            default :
                $stepId = 9;
                break;
        }

        return (int)$stepId;
    }

    /**
     * Проверяет правильность ответа на вопрос о составе пива и возвращает количество баллов
     *
     * @param $answers
     *
     * @return int
     */
    public function getMultiPoints($answers)
    {
        $need = array(1, 2, 3, 4, 7);

        foreach ($answers as $k) {
            if (!in_array($k->id, $need))
                return 0;
        }

        return $this->_correctPointsList[15];
    }
}