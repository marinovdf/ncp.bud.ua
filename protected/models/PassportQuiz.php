<?php

/**
 * This is the model class for table "{{passport_quiz}}".
 *
 * The followings are the available columns in table '{{passport_quiz}}':
 * @property integer $id
 * @property string $UserGUID
 * @property string $Phone
 * @property integer $StepIndex
 * @property integer $StepID
 * @property integer $QuestionID
 * @property integer $Points
 * @property string $TransactionDateTime
 * @property string $Answer
 */
class PassportQuiz extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PassportQuiz the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->mysqlDb;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{passport_quiz}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UserGUID, QuestionID, Answer', 'required'),
			array('StepIndex, StepID, QuestionID, Points', 'numerical', 'integerOnly'=>true),
			array('UserGUID', 'length', 'max'=>36),
			array('Phone', 'length', 'max'=>12),
			array('Answer', 'length', 'max'=>255),
			array('TransactionDateTime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, UserGUID, Phone, StepIndex, StepID, QuestionID, Points, TransactionDateTime, Answer', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'UserGUID' => 'User Guid',
			'Phone' => 'Phone',
			'StepIndex' => 'Step Index',
			'StepID' => 'Step',
			'QuestionID' => 'Question',
			'Points' => 'Points',
			'TransactionDateTime' => 'Transaction Date Time',
			'Answer' => 'Answer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('UserGUID',$this->UserGUID,true);
		$criteria->compare('Phone',$this->Phone,true);
		$criteria->compare('StepIndex',$this->StepIndex);
		$criteria->compare('StepID',$this->StepID);
		$criteria->compare('QuestionID',$this->QuestionID);
		$criteria->compare('Points',$this->Points);
		$criteria->compare('TransactionDateTime',$this->TransactionDateTime,true);
		$criteria->compare('Answer',$this->Answer,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}