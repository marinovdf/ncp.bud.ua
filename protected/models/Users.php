<?php

/**
 * Class Users
 * Модель для работы с пользователями
 */
class Users extends CModel
{
    /**
     * Return user data
     *
     * @param array $params
     *
     * @return array|mixed
     */
    public function getUser($params = array())
    {
        $sql = 'EXECUTE dbo.p_rapp_GetConsumer
                    @ConsumerGUID =:ConsumerGUID,
                    @FacebookUserID =:FacebookUserID,
                    @Password =:Password,
                    @MobilePhone =:MobilePhone,
                    @EmailAddress =:EmailAddress';

        $connection = Yii::app()->db->createCommand($sql);
        $connection->bindValues(array(
            ':ConsumerGUID'   => !empty($params['ConsumerGUID']) ? $params['ConsumerGUID'] : '',
            ':FacebookUserID' => !empty($params['FacebookUserID']) ? $params['FacebookUserID'] : '',
            ':Password'       => !empty($params['Password']) ? $params['Password'] : '',
            ':MobilePhone'    => !empty($params['MobilePhone']) ? $params['MobilePhone'] : '',
            ':EmailAddress'   => !empty($params['EmailAddress']) ? $params['EmailAddress'] : '',
        ));

        $result  = $connection->queryRow();
        $charset = new Charset();

        if ($result) {
            foreach ($result as $k => $v)
                $result[$k] = $charset->toUTF8($v);
        } else
            $result = array();

        return $result;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function getUsers($params = array())
    {
        $sql = 'EXECUTE dbo.p_rapp_GetConsumer
                    @ConsumerGUID =:ConsumerGUID,
                    @FacebookUserID =:FacebookUserID,
                    @Password =:Password,
                    @MobilePhone =:MobilePhone,
                    @EmailAddress =:EmailAddress';

        $connection = Yii::app()->db->createCommand($sql);
        $connection->bindValues(array(
            ':ConsumerGUID'   => !empty($params['ConsumerGUID']) ? $params['ConsumerGUID'] : '',
            ':FacebookUserID' => !empty($params['FacebookUserID']) ? $params['FacebookUserID'] : '',
            ':Password'       => !empty($params['Password']) ? $params['Password'] : '',
            ':MobilePhone'    => !empty($params['MobilePhone']) ? $params['MobilePhone'] : '',
            ':EmailAddress'   => !empty($params['EmailAddress']) ? $params['EmailAddress'] : '',
        ));

        $result  = $connection->queryAll();
        $charset = new Charset();

        if ($result) {
            foreach ($result as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    $result[$k][$k1] = $charset->toUTF8($v1);
                }
            }
        } else
            $result = array();

        return $result;
    }

    /**
     * Фиксирует участника активности
     *
     * @param array $params
     *
     * @return array
     */
    public static function setCampaignActivity($params = array())
    {
        $sql = 'EXECUTE dbo.p_rapp_CAResponseSET
                    @ConsumerGUID =:ConsumerGUID,
                    @ActivityGUID =:ActivityGUID';

        $connection = Yii::app()->db->createCommand($sql);
        $connection->bindValues(array(
            ':ConsumerGUID' => !empty($params['ConsumerGUID']) ? $params['ConsumerGUID'] : '',
            ':ActivityGUID' => !empty(Yii::app()->params['activity']['guid']) ? Yii::app()->params['activity']['guid'] : '',
        ));

        try {
            $result = $connection->queryAll();
        } catch (Exception $e) {
            return $e;
        }

        $charset = new Charset();

        if ($result) {
            foreach ($result as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    $result[$k][$k1] = $charset->toUTF8($v1);
                }
            }
        } else
            $result = array();

        return $result;
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {
        return array();
    }
}