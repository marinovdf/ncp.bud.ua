<?php
class Profile extends CModel{

    private $_user;

    public $fullName;
    public $birthDate;
    public $age;
    public $email;
    public $cityName;
    public $rayonName;
    public $oblastName;
    public $phone;

    function __construct()
    {
        $this->_user      = (object)$this->getUser();
        $this->fullName   = $this->getFullName();
        $this->birthDate  = $this->getBirthDate();
        $this->age        = $this->getAge();
        $this->email      = $this->getEmail();
        $this->phone      = $this->getPhone();
        $this->cityName   = $this->getCityName();
        $this->rayonName  = $this->getRayonName();
        $this->oblastName = $this->getOblastName();
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        $this->_user = !empty(Yii::app()->user->data) ? Yii::app()->user->data : null;

        return $this->_user;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        $birthDate = $this->birthDate;
        //explode the date to get month, day and year
        $birthDate = explode(".", $birthDate);
        //get age from date or birthdate
        $age       = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md") ? ((date("Y") - $birthDate[0]) - 1) : (date("Y") - $birthDate[0]));
        $this->age = $age;

        return $this->age;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        $this->birthDate = date('Y.m.d', strtotime($this->_user->BirthDate));

        return $this->birthDate;
    }


    /**
     * @return mixed
     */
    public function getCityName()
    {
        $this->cityName = $this->_user->CityName;
        return $this->cityName;
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        $this->email = $this->_user->EmailAddress;

        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        $this->fullName = $this->_user->FullName;

        return $this->fullName;
    }

    /**
     * @return mixed
     */
    public function getOblastName()
    {
        $this->oblastName = $this->_user->OblastName;
        return $this->oblastName;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        $countryCode = substr($this->_user->MainMobilePhone, 0, 3);
        $operatorCode = substr($this->_user->MainMobilePhone, 3, 2);
        $phoneNumber =  substr($this->_user->MainMobilePhone, 5, 7);

        $this->phone = '+'.$countryCode.' ('. $operatorCode .') '. $phoneNumber;
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getRayonName()
    {
        $this->rayonName = $this->_user->RayonName;
        return $this->rayonName;
    }

    /**
     * Returns the list of attribute names of the model.
     *
     * @return array list of attribute names.
     */
    public function attributeNames()
    {

    }
}