<?php

class ValidateBirthdateForm extends CFormModel
{
    public $day;
    public $month;
    public $year;
    public $birthday;

    public function __construct()
    {

    }

    public function beforeValidate()
    {

        if(!empty($this->year) && !empty($this->month) && !empty($this->day)){
            $this->birthday = $this->year.'-'.$this->month.'-'.$this->day;
        }

        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        if(!$this->hasErrors()){
            $need=new DateTime(date("Y-m-d", strtotime("-18 year", time())));
            $birth=new DateTime($this->birthday);

            if($birth>$need){
                $this->addError('','Для доступу на сайт вам повинно бути не менше 18 років');
            }
        }

        return parent::afterValidate();
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // email, body are required
            array('day, month, year', 'required', 'message' => 'Не вказаний {attribute}'),
            array('day', 'date', 'format' => 'dd', 'message' => 'Некорректне значення поля {attribute}'),
            array('month', 'date', 'format' => 'MM', 'message' => 'Некорректне значення поля {attribute}'),
            array('year', 'date', 'format' => 'yyyy', 'message' => 'Некорректне значення поля {attribute}'),

            //array('birthday','compare','compareValue'=>date("d.m.Y", strtotime("-18 year", time())),'operator'=>'<'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'day'   => 'День',
            'month' => 'Місяць',
            'year'  => 'Рік',
        );
    }

    public function resetForm()
    {
        foreach ($this->attributes as $k => $v)
            $this->$k = null;
    }
}