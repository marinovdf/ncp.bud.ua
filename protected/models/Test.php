<?php

/**
 * This is the model class for table "{{test}}".
 *
 * The followings are the available columns in table '{{test}}':
 * @property integer $id
 * @property string $UserGUID
 * @property integer $QuestionID
 * @property integer $AnswerID
 * @property string $TransactionDateTime
 */
class Test extends CActiveRecord
{
    public function init()
    {
        $this->UserGUID = !empty(Yii::app()->user->data->ConsumerGUID) ? Yii::app()->user->data->ConsumerGUID : null;
        parent::init();
    }

    protected function afterValidate()
    {
        if($this->isQuestionExists()){
            $this->addError('QuestionID', 'Duplicate question');
        }

        parent::afterValidate();
    }

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Test the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->mysqlDb;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{test}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UserGUID, QuestionID, AnswerID', 'required'),
			array('AnswerID', 'numerical', 'min'=>1, 'max'=>4, 'integerOnly'=>true),
			array('QuestionID', 'numerical', 'min'=>1, 'max'=>8, 'integerOnly'=>true),
			array('UserGUID', 'length', 'is'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, UserGUID, QuestionID, AnswerID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'UserGUID' => 'User Guid',
			'QuestionID' => 'Question ID',
			'AnswerID' => 'Answer ID',
			'TransactionDateTime' => 'Transaction Date Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('UserGUID',$this->UserGUID,true);
		$criteria->compare('QuestionID',$this->QuestionID);
		$criteria->compare('AnswerID',$this->AnswerID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Проверяет наличие записи с ответом в БД
     * @return bool
     */
    private function isQuestionExists()
    {
        $exists = $this->exists("UserGUID=:UserGUID AND QuestionID=:QuestionID", array(
                ":UserGUID"  => $this->UserGUID,
                ":QuestionID" => $this->QuestionID)
        );

        return $exists;
    }

    /**
     * Возвращает текущий номер вопроса пользователя
     *
     * @return int
     */
    public function getCurrentUserQuestion()
    {
        $query = $this->dbConnection->createCommand()
            ->select('MAX(QuestionID) as lastQuestionID')
            ->from($this->tableName())
            ->where('UserGUID=:UserGUID', array(':UserGUID'=>$this->UserGUID))
            ->queryRow();

        if($query['lastQuestionID'] == null){
            $curQuestion = 1;
        } elseif ($query['lastQuestionID'] >= 8) {
            $curQuestion = 9;
        } else {
            $curQuestion = (int)$query['lastQuestionID'] + 1;
        }

        return $curQuestion;
    }

    /**
     * Возвращает ключи к тесту
     *
     * @return array
     */
    private function keys()
    {
        return array(
            1 => array(
                1 => array("NV"),
                2 => array("DK"),
                3 => array("AM"),
                4 => array("GN")
            ),
            2 => array(
                1 => array("NV", "AM"),
                2 => array("DK", "AM"),
                3 => array("DK", "GN"),
                4 => array("DK", "AM", "GN")
            ),
            3 => array(
                1 => array("NV", "DK", "AM", "GN"),
                2 => array("NV", "AM"),
                3 => array("DK", "GN"),
                4 => array("NV", "AM")
            ),

            4 => array(
                1 => array("DK", "AM", "GN"),
                2 => array("DK", "GN"),
                3 => array("NV", "DK", "AM", "GN"),
                4 => array("NV", "AM")
            ),
            5 => array(
                1 => array("NV", "DK", "AM", "GN"),
                2 => array("AM"),
                3 => array("NV", "DK", "GN"),
                4 => array("DK", "GN")
            ),
            6 => array(
                1 => array("NV", "AM"),
                2 => array("DK", "AM", "GN"),
                3 => array("DK", "AM", "GN"),
                4 => array("DK", "GN")
            ),
            7 => array(
                1 => array("NV", "AM"),
                2 => array("DK", "GN"),
                3 => array("DK", "GN"),
                4 => array("NV", "DK", "AM"),
            ),
            8 => array(
                1 => array("NV", "DK", "AM"),
                2 => array("DK", "GN"),
                3 => array("NV", "DK", "AM"),
                4 => array("NV", "DK")
            )
        );
    }


    /**
     * Возвращаем количество ключей
     *
     * @return array
     */
    private function getAllKeysCount()
    {
        $result = $this->dbConnection->createCommand()
            ->select('QuestionID, AnswerID')
            ->from($this->tableName())
            ->where('UserGUID=:UserGUID', array(':UserGUID' => $this->UserGUID))
            ->queryAll();

        $keys = $this->keys();
        
        $finalKeys = array();

        foreach($result as $k=>$v){
            $key = $keys[$v['QuestionID']][$v['AnswerID']];

            $finalKeys = array_merge($key, $finalKeys);
        }
        
        $counts = array_count_values($finalKeys);
        
        return $counts;
    }

    /**
     * Определяет и возвращает ключ характера
     *
     * @return mixed
     */
    public function getCharacter()
    {
        //Получаем количество каждого из ключей
        $counts = $this->getAllKeysCount();

        //Пересортировуем массив в порядок NV, AM, DK, GN
        $resortCounts = array();

        if(!empty($counts['NV']))
            $resortCounts['NV'] = $counts['NV'];

        if(!empty($counts['AM']))
            $resortCounts['AM'] = $counts['AM'];

        if(!empty($counts['DK']))
            $resortCounts['DK'] = $counts['DK'];

        if(!empty($counts['GN']))
            $resortCounts['GN'] = $counts['GN'];

        // Определяем первое из максимальных значений
        $character = array_search(max($resortCounts), $resortCounts);

        return $character;
    }

    /**
     * Возвращает тип характера по его ключу
     *
     * @param null $characterKey
     *
     * @return bool
     */
    public function getCharacterType($characterKey = null)
    {
        $types = array(
            'NV'=> 'Невиправний тусовщик',
            'AM'=> 'Ловець емоцій',
            'DK'=> 'Душа компанії',
            'GN'=> 'Завзятий ентузіаст'
        );

        if(empty($characterKey))
            return false;
        else
            return $types[$characterKey];
    }

    /**
     * Возвращает характерный подарок
     *
     * @param null $characterKey
     *
     * @return bool
     */
    public function getGiftType($characterKey = null)
    {
        $types = array(
            'NV'=> 'У ритмі серця',
            'AM'=> 'Наважся на більше',
            'DK'=> 'На пульсі розваг',
            'GN'=> 'Заряд емоцій'
        );

        if(empty($characterKey))
            return false;
        else
            return $types[$characterKey];
    }

    /**
     * @throws Exception
     */
    public function saveCharacterType()
    {
        $characterKey = $this->getCharacter();

        if($characterKey){
            $sql = "insert into profiling_characters  (UserGUID, CharacterType) values (:UserGUID, :CharacterType)";
            
            $parameters = array(
                ":UserGUID"=>$this->UserGUID,
                ':CharacterType'=>$characterKey);
            try{
                $this->dbConnection->createCommand($sql)->execute($parameters);
            } catch (Exception $e){
                throw new Exception($e->getMessage());
            }
        }
    }
}