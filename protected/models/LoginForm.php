<?
class LoginForm extends CFormModel
{
    public $phone;
    public $password;

    private $_identity;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // email, body are required
            array('phone, password', 'required', 'message' => 'Не заповнене поле {attribute}'),
            array('phone', 'match', 'pattern' => '/\+380\(\d{2}\)\d{3}-\d{2}-\d{2}/', 'message' => '{attribute} повинен мати формат 380(xx)xxx-xx-xx)'),
            array('password', 'length', 'max' => 50, 'tooLong' => 'Пароль може містити не більше 50 символів'),

            // password needs to be authenticated
            array('Password', 'authenticate')
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'phone'    => 'Телефон',
            'password' => 'Пароль',
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->clearPhone(), $this->password);
            if (!$this->_identity->authenticate()) {
                $this->addError('', $this->_identity->errorMessage);
            }
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     *
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity(intval($this->clearPhone()), $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = 3600 * 24 * 30; // 30 days
            Yii::app()->user->login($this->_identity, $duration);

            // Трекинг авторизации
            Yii::app()->user->setFlash('Auth', '1');

            // Фиксируем пользователя как участника активности
            $consumerGUID = !empty(Yii::app()->user->data->ConsumerGUID) ? Yii::app()->user->data->ConsumerGUID : null;

            if($consumerGUID){
                $params = array('ConsumerGUID'=>$consumerGUID);

                Users::setCampaignActivity($params);
            }

            return true;
        } else
            return false;
    }

    /**
     * Очищает номер телефона от лишних символов
     * @return mixed
     */
    private function clearPhone(){
        $clear = preg_replace('/\D/', '', $this->phone);

        return $clear;
    }
}