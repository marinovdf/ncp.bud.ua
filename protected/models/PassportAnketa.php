<?php

/**
 * This is the model class for table "{{passport_anketa}}".
 *
 * The followings are the available columns in table '{{passport_anketa}}':
 * @property integer $id
 * @property string $UserGUID
 * @property string $Phone
 * @property integer $QuestionID
 * @property string $QuestionStatus
 * @property string $Answer
 * @property string $InputText
 * @property integer $Points
 * @property integer $ModerationStatus
 * @property string $TransactionDateTime
 */
class PassportAnketa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PassportAnketa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->mysqlDb;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{passport_anketa}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UserGUID, Phone, QuestionID, Points, Answer', 'required'),
			array('QuestionID, Points', 'numerical', 'integerOnly'=>true),
			array('UserGUID', 'length', 'max'=>36),
			array('Phone', 'length', 'max'=>12),
			array('QuestionStatus', 'length', 'max'=>50),
			array('ModerationStatus', 'length', 'max'=>1),
			array('Answer', 'length', 'max'=>500),
			array('InputText', 'length', 'max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, UserGUID, Phone, QuestionID, QuestionStatus, Answer, Points, InputText, ModerationStatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'UserGUID' => 'User Guid',
			'Phone' => 'Phone',
			'QuestionID' => 'Question',
			'QuestionStatus' => 'Question Status',
			'Answer' => 'Answer',
			'Points' => 'Points',
			'InputText' => 'Input Text',
			'ModerationStatus' => 'Moderation Status',
			//'TransactionDateTime' => 'Transaction Date Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('UserGUID',$this->UserGUID,true);
		$criteria->compare('Phone',$this->Phone,true);
		$criteria->compare('QuestionID',$this->QuestionID);
		$criteria->compare('QuestionStatus',$this->QuestionStatus,true);
		$criteria->compare('Answer',$this->Answer,true);
		$criteria->compare('Points',$this->Points);
		$criteria->compare('InputText',$this->InputText);
		$criteria->compare('ModerationStatus',$this->ModerationStatus);
		//$criteria->compare('TransactionDateTime',$this->TransactionDateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}