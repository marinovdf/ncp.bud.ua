<?php

/**
 * @property integer $id
 * @property string  $LastName
 * @property string  $FirstName
 * @property string  $Patronymic
 * @property string  $Birthday
 * @property string  $Gender
 * @property string  $Email
 * @property string  $MobilePhone
 * @property string  $Oblast
 * @property string  $Rayon
 * @property string  $CityName
 * @property string  $CampaignName
 * @property string  $MediaChannelName
 * @property string  $ActivityType
 * @property string  $TransactionType
 * @property string  $InboundResponseDetailedSource
 * @property string  $InboundResponseCreative
 * @property string  $SurveyID
 * @property integer $BrandID
 * @property string  $TransactionDate
 * @property string  $Country
 * @property string  $DisclaimerCheckBox
 */
class RegistrationForm extends CFormModel
{

    public $cityModel;

    //Default constants
    protected $ACTIVITY_NAME         = 'BUD Profiling 2014';
    protected $TRANSACTION_CODE      = '';
    protected $TRANSACTION_TYPE_CODE = 'RS';
    protected $ACTIVITY_TYPE_CODE    = 'SW';
    protected $MEDIA_CHANNEL_CODE    = 'WB';
    protected $INBOUND_RDS           = 'Standard survey';
    protected $INBOUND_RC            = 'Standard survey';
    protected $SURVEY_ID             = 1;
    protected $COUNTRY_CODE          = 'UKR';
    protected $SOURCE_TYPE_CODE      = 'WEB';
    protected $CUR_BRAND_CODE        = 1795;
    protected $ADDRESS_STATUS        = 'VALID';
    protected $AGE_STATUS            = 'VALID';
    protected $IMPORT_FILE_ID        = null;

    public $LastName;
    public $FirstName;
    public $Patronymic;
    public $Birthday;
    public $Oblast;
    public $Rayon;
    public $CityName;
    public $Gender;
    public $MobilePhone;
    public $FaceBookUserID;
    public $Email;
    public $Password;
    public $DisclaimerCheckBox;

    public $PhoneValidStatusCode;

    public $operatorCode;
    public $phoneVerified = false;
    private $_verifiedCode = false;

    public $ValidationCode;
    public $ValidationCodeSended;

    public $code;

    function __construct()
    {
        $this->cityModel = new City();

        if (!empty(Yii::app()->user->facebookID))
            $this->FaceBookUserID = Yii::app()->user->facebookID;

    }

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Users the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        $this->MobilePhone = trim($this->MobilePhone);

        if ($this->Birthday == '0000.00.00')
            $this->Birthday = null;

        if ($this->FirstName) {
            $this->FirstName = $this->setFirstLetterToUpper($this->FirstName);
        }

        if ($this->LastName) {
            $this->LastName = $this->setFirstLetterToUpper($this->LastName);
        }

        if ($this->Patronymic) {
            $this->Patronymic = $this->setFirstLetterToUpper($this->Patronymic);
        }

        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        // Дедуплицируем имейл
        if (!$this->hasErrors('Email')) {

            if ($email = $this->checkEmail(array('Email' => $this->Email)) !== array()){
                $this->isEmailExists();
            }
        }

        if (!$this->hasErrors()) {
            if ($this->phoneVerified == false) {
                $this->addError('phoneVerified', 'Номер не перевірений');
            }
        }

        // Дедуплицируем телефон
        if (!$this->hasErrors('MobilePhone')) {
            if ($this->checkMobile(array('MobilePhone' => $this->operatorCode . $this->MobilePhone)) !== array())
                $this->addError('MobilePhone', 'Користувач з таким мобільним телефоном вже зареєстрований');
        }

        // Проверяем

        return parent::afterValidate();
    }

    public function beforeSave()
    {
        $this->MobilePhone = $this->operatorCode . $this->MobilePhone;

        /*
         * перепроверяем чекбокс дисклаймера
         * если чекбокс активирован - переопределяем значение на YES
         * */
        if (!empty($this->DisclaimerCheckBox)) $this->DisclaimerCheckBox = 'YES';

        return parent::beforeSave();
    }

    public function afterSave()
    {
        if ($this->DisclaimerCheckBox === 'YES')
            $this->DisclaimerCheckBox = 1;

        $this->MobilePhone = substr($this->MobilePhone, 5);

        return parent::afterSave();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{users}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'LastName, FirstName, Email, Birthday, MobilePhone, Gender, Oblast, Rayon, CityName, operatorCode',
                'required',
                'message' => 'Необхідно заповнити поле {attribute}'
            ),
            array('LastName, FirstName, Patronymic, Oblast, Rayon, CityName', 'length', 'max' => 50),

            // Только кириллица и апостроф
            array('LastName, FirstName, Patronymic', 'match', 'pattern' => '/^[А-Яа-яіїєґІЇЄҐ\'\s]+$/u', 'message' => 'Поле {attribute} повинно містити лише кирилицю та апостроф'),

            array('Gender', 'length', 'max' => 1),
            array('Email', 'length', 'max' => 255),
            array('Email', 'email', 'checkMX' => true, 'message' => 'Email введено невірно'),

            array('MobilePhone', 'length', 'is' => 7, 'message' => '{attribute} повинен містити 7 цифр'),
            array('FaceBookUserID', 'length', 'max' => 20, 'message' => '{attribute} повинен містити 20 цифр'),

            // Только цифри
            array('MobilePhone, FaceBookUserID', 'numerical', 'message' => '{attribute} повинен містити лише цифри'),

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('LastName, FirstName, Patronymic, Birthday, Gender, Email, MobilePhone, Oblast, Rayon, CityName', 'safe', 'on' => 'search'),

            // Password validation
            array('Password', 'length', 'min' => 5, 'max' => 50, 'tooShort' => 'Мінімальна довжина пароля 5 символів', 'tooLong' => 'Максимальна довжина пароля 50 символів'),
            array('Password', 'match', 'pattern' => '/^[A-z0-9\s]+$/u', 'message' => 'Некорректний формат пароля. Некоректний формат пароля. Пароль може містити латинські літери и цифри.'),

            // валидация дисклаймера
            array('DisclaimerCheckBox', 'compare',
                'compareValue' => true,
                'message'      => 'Для реєстрації необхідно піддтвердити, що вам виповнилось 18 років, i що всі наведені вами персональні дані є достовірними'),

            // валидация возраста
            array('Birthday', 'date', 'format' => 'yyyy.mm.dd', 'allowEmpty' => false, 'message' => 'Дата має невірний формат'),
            array('Birthday', 'checkValidAge')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'LastName'       => 'Прізвище',
            'FirstName'      => 'Ім`я',
            'Patronymic'     => 'По-батькові',
            'Birthday'       => 'Дата народження',
            'Gender'         => 'Стать',
            'Email'          => 'Email',
            'MobilePhone'    => 'Мобільний телефон',
            'Oblast'         => 'Область',
            'Rayon'          => 'Район',
            'CityName'       => 'Населений пункт',
            'operatorCode'   => 'Код оператора',
            'Password'       => 'Пароль',
            'FaceBookUserID' => 'Facebook ID',
        );
    }

    /**
     * Возвращает список кодов операторов
     *
     * @return array
     */
    public function getOperatorCodeList()
    {
        return array(
            '38039' => '039',
            '38050' => '050',
            '38063' => '063',
            '38066' => '066',
            '38067' => '067',
            '38068' => '068',
            '38091' => '091',
            '38092' => '092',
            '38093' => '093',
            '38094' => '094',
            '38095' => '095',
            '38096' => '096',
            '38097' => '097',
            '38098' => '098',
            '38099' => '099',
        );
    }

    /**
     * @return array
     */
    public function getGenderList()
    {
        return array(
            'M' => 'Чоловіча',
            'F' => 'Жіноча'
        );
    }

    public function getOblastList()
    {
        $list = $this->cityModel->getOblast();

        return $list;
    }

    public function getRayonList()
    {
        $list = array();

        if ($this->Oblast)
            $list = $this->cityModel->getRayon(array('OblastName' => $this->Oblast));

        return $list;
    }

    public function getCityList()
    {
        $list = array();

        if ($this->Rayon && $this->Oblast)
            $list = $this->cityModel->getCity(array('RayonName' => $this->Rayon, 'OblastName' => $this->Oblast));

        return $list;
    }

    function genCode()
    {
        //Функция генерации кода
        $number     = '';
        $ar_numbers = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"); //Набор символов для генерации
        shuffle($ar_numbers); //Перебор символов

        for ($a = 1; $a < 7; $a++) {
            //Цикл перебора, обозначающий длину регистра кода
            $ar_rand = array_rand(array_flip($ar_numbers), 2); //
            $number .= $ar_rand[0]; //
        }

        return $number;
    }

    public function save()
    {
        $sql = "EXECUTE dbo.p_rapp_TransactionImport
                    @TransactionCode=:TransactionCode,
                    @TransactionDate=:TransactionDate,
                    @TransactionTypeCode=:TransactionTypeCode,
                    @ActivityTypeCode=:ActivityTypeCode,
                    @MediaChannelCode=:MediaChannelCode,
                    @Cur1BrandCode=:Cur1BrandCode,
                    @Cur2BrandCode=:Cur2BrandCode,
                    @Cur3BrandCode=:Cur3BrandCode,
                    @Cur1BrandARCode=:Cur1BrandARCode,
                    @Cur2BrandARCode=:Cur2BrandARCode,
                    @Cur3BrandARCode=:Cur3BrandARCode,
                    @ActivityName=:ActivityName,
                    @LastName=:LastName,
                    @FirstName=:FirstName,
                    @Patronymic=:Patronymic,
                    @Birthdate=:Birthdate,
                    @GenderCode=:GenderCode,
                    @CityName=:CityName,
                    @RayonName=:RayonName,
                    @OblastName=:OblastName,
                    @PostalCode=:PostalCode,
                    @PostalCode1=:PostalCode1,
                    @PostalCode2=:PostalCode2,
                    @StreetTypeCode=:StreetTypeCode,
                    @Street=:Street,
                    @Building=:Building,
                    @Apartment=:Apartment,
                    @MobilePhone=:MobilePhone,
                    @HomePhone=:HomePhone,
                    @EmailAddress=:EmailAddress,
                    @InboundRDS=:InboundRDS,
                    @InboundRC=:InboundRC,
                    @SurveyID=:SurveyID,
                    @CountryCode=:CountryCode,
                    @ImportFileID=:ImportFileID,
                    @AddressStatus=:AddressStatus,
                    @AgeStatus=:AgeStatus,
                    @DisclaimerCode=:DisclaimerCode,
                    @FaceBookUserID=:FaceBookUserID,
                    @PrizeCode=:PrizeCode,
                    @EmailValidStatusCode=:EmailValidStatusCode,
                    @PhoneValidStatusCode=:PhoneValidStatusCode,
                    @SourceTypeCode=:SourceTypeCode,
                    @Password=:Password";

        $command = Yii::app()->db->createCommand($sql);
        $command->bindValues(array(
            ':TransactionCode'      => md5(time()),
            ':TransactionDate'      => date('Y.m.d H:i:s'),
            ':TransactionTypeCode'  => $this->TRANSACTION_TYPE_CODE,
            ':ActivityTypeCode'     => $this->ACTIVITY_TYPE_CODE,
            ':MediaChannelCode'     => $this->MEDIA_CHANNEL_CODE,
            ':Cur1BrandCode'        => $this->CUR_BRAND_CODE,
            ':ActivityName'         => $this->ACTIVITY_NAME,
            ':InboundRDS'           => $this->INBOUND_RDS,
            ':InboundRC'            => $this->INBOUND_RC,
            ':SurveyID'             => $this->SURVEY_ID,
            ':CountryCode'          => $this->COUNTRY_CODE,
            ':SourceTypeCode'       => $this->SOURCE_TYPE_CODE,
            ':ImportFileID'         => $this->IMPORT_FILE_ID,
            ':AddressStatus'        => $this->ADDRESS_STATUS,
            ':AgeStatus'            => $this->AGE_STATUS,
            ':Cur2BrandCode'        => '',
            ':Cur3BrandCode'        => '',
            ':Cur1BrandARCode'      => '',
            ':Cur2BrandARCode'      => '',
            ':Cur3BrandARCode'      => '',
            ':LastName'             => iconv('utf8', 'cp1251', $this->LastName),
            ':FirstName'            => iconv('utf8', 'cp1251', $this->FirstName),
            ':Patronymic'           => iconv('utf8', 'cp1251', $this->Patronymic),
            ':Birthdate'            => $this->Birthday,
            ':GenderCode'           => $this->Gender,
            ':CityName'             => iconv('utf8', 'cp1251', $this->CityName),
            ':RayonName'            => iconv('utf8', 'cp1251', $this->Rayon),
            ':OblastName'           => iconv('utf8', 'cp1251', $this->Oblast),
            ':MobilePhone'          => $this->operatorCode . $this->MobilePhone,
            ':EmailAddress'         => $this->Email,
            ':Password'             => $this->Password,
            ':PostalCode'           => '',
            ':PostalCode1'          => '',
            ':PostalCode2'          => '',
            ':StreetTypeCode'       => '',
            ':Street'               => '',
            ':Building'             => '',
            ':Apartment'            => '',
            ':HomePhone'            => '',
            ':DisclaimerCode'       => !empty($this->DisclaimerCheckBox) ? 'YES' : 'NO',
            ':FaceBookUserID'       => $this->FaceBookUserID,
            ':PrizeCode'            => '',
            ':EmailValidStatusCode' => 'VALID',
            ':PhoneValidStatusCode' => $this->PhoneValidStatusCode,
        ));

        try {
            $res = $command->queryAll();


            array_walk_recursive($res, function (&$value, $key) {
                if (is_string($value)) {
                    $value = iconv('cp1251', 'utf8', $value);
                }
            });

            Yii::trace(CVarDumper::dumpAsString($res), 'vardump');

            // если транзакция прошла успешно - возвращаем true
            if ($res[0]['ReturnCode'] == 1)
                return true;
            else
                return false;


        } catch (Exception $e) {
            Yii::trace(CVarDumper::dumpAsString($e->getTraceAsString()), 'vardump');

            return false;
        }
    }

    /**
     * Return user data
     *
     * @param array $params
     *
     * @return array|mixed
     */
    public function getConsumer($params = array())
    {
        $sql = 'EXECUTE dbo.p_rapp_GetConsumer
                    @ConsumerGUID =:ConsumerGUID,
                    @FacebookUserID =:FacebookUserID,
                    @Password =:Password,
                    @MobilePhone =:MobilePhone,
                    @EmailAddress =:EmailAddress';

        $connection = Yii::app()->db->createCommand($sql);
        $connection->bindValues(array(
            ':ConsumerGUID'   => !empty($params['ConsumerGUID']) ? $params['ConsumerGUID'] : '',
            ':FacebookUserID' => !empty($params['FacebookUserID']) ? $params['FacebookUserID'] : '',
            ':Password'       => !empty($params['Password']) ? $params['Password'] : '',
            ':MobilePhone'    => !empty($params['MobilePhone']) ? $params['MobilePhone'] : '',
            ':EmailAddress'   => !empty($params['EmailAddress']) ? $params['EmailAddress'] : '',
        ));

        $result  = $connection->queryRow();
        $charset = new Charset();

        if ($result) {
            foreach ($result as $k => $v)
                $result[$k] = $charset->toUTF8($v);
        } else
            $result = array();

        return $result;
    }

    /**
     * Проводит дедублиикацию пользователя по номеру телефона
     *
     * Если совпадают два из трех полей (дата рождения, имейл, телефон),
     * то пользователь существует/не существует и его можно создать/апдейтнуть,
     *
     * иначе
     *
     * пользователь с таким номером телефона существует, но его нельза апдейтнуть
     *
     * @return void
     */
    public function isPhoneExists()
    {
        $user = new Users();

        $users = $user->getUsers(array(
            'MobilePhone' => $this->operatorCode . $this->MobilePhone
        ));

        if (!empty($users)) {
            $regUserBirth = strtotime($this->Birthday);

            foreach ($users as $key) {
                // Отсекаем юзера по дате рождения
                if (strtotime($key['BirthDate']) != $regUserBirth) {
                    // Отсекаем юзера по имейлу
                    if ($this->Email != $key['EmailAddress']) {
                        $this->addError('MobilePhone', 'Телефон вже зареєстрований і належить іншому користувачу');
                    }
                }
            }
        }

        return;
    }

    /**
     * Проводит дедублиикацию пользователя по имейлу
     *
     * Если совпадают два из трех полей (дата рождения, имейл, телефон),
     * то пользователь существует/не существует и его можно создать/апдейтнуть,
     *
     * иначе
     *
     * пользователь с таким имейлом существует, но его нельза апдейтнуть
     *
     * @return void
     */
    public function isEmailExists()
    {
        $user = new Users();

        $users = $user->getUsers(array(
            'EmailAddress' => $this->Email
        ));

        if (!empty($users)) {
            $regUserBirth = strtotime($this->Birthday);

            foreach ($users as $key) {
                // Отсекаем юзера по дате рождения
                if (strtotime($key['BirthDate']) != $regUserBirth) {
                    // Отсекаем юзера по мобильному телефону
                    if ($this->operatorCode . $this->MobilePhone != $key['MainMobilePhone']) {
                        $this->addError('EmailAddress', 'Користувач з таким e-mail вже зареєстрований');
                    }
                }
            }
        }

        return;
    }

    /**
     * Проводит дедупликацию пользователя по номеру телефона
     * ValidStatus = 1 - телефон пользователя провалидирован
     *
     * @param array $params
     *
     * @return array
     */
    public function checkMobile($params = array())
    {
        $sql = "EXECUTE dbo.p_rapp_CheckMobile
                    @MobilePhone =:MobilePhone,
                    @ConsumerGUID =:ConsumerGUID,
                    @ValidStatus =:ValidStatus,
                    @DirectBy =:DirectBy";

        $connection = Yii::app()->db->createCommand($sql);
        $connection->bindValues(array(
            ':ConsumerGUID' => !empty($params['ConsumerGUID']) ? $params['ConsumerGUID'] : '',
            ':MobilePhone'  => !empty($params['MobilePhone']) ? $params['MobilePhone'] : '',
            ':ValidStatus'  => 1,
            ':DirectBy'     => !empty($params['DirectBy']) ? $params['DirectBy'] : 0,
        ));

        $result = $connection->queryAll();

        return $result;
    }

    public function checkEmail($params = array())
    {
        $sql = "EXECUTE dbo.p_rapp_CheckEmail
                    @Email =:Email,
                    @ConsumerGUID =:ConsumerGUID,
                    @DirectBy =:DirectBy";

        $connection = Yii::app()->db->createCommand($sql);
        $connection->bindValues(array(
            ':Email'        => !empty($params['Email']) ? $params['Email'] : '',
            ':ConsumerGUID' => !empty($params['ConsumerGUID']) ? $params['ConsumerGUID'] : '',
            ':DirectBy'     => !empty($params['DirectBy']) ? $params['DirectBy'] : 0,
        ));

        $result = $connection->queryAll();

        Yii::trace(CVarDumper::dumpAsString($result), 'vardump');

        return $result;
    }

    /**
     * Конвертириует строку в нижний регистр,
     * при этом первую букву делает заглавной
     *
     * @param string $str
     *
     * @return null|string
     */
    protected function setFirstLetterToUpper($str = '')
    {
        if (!empty($str)) {
            mb_internal_encoding("UTF-8");
            $str = mb_strtoupper(mb_substr($str, 0, 1)) . mb_strtolower(mb_substr($str, 1));
        } else {
            return null;
        }

        return $str;
    }

    /**
     * Валидация возраста
     *
     * @param $attribute
     * @param $params
     */
    public function checkValidAge($attribute, $params)
    {
        $min = date("Y.m.d", strtotime("-18 year", time()));
        $max = '1930.01.01';
        $birth = $this->Birthday;

        //проверка минимального возраста
        if($birth > $min){
            $this->addError($attribute, 'Для доступу на сайт вам повинно бути не менше 18 років.');
        }

        //проверка максимального возраста
        if($birth < $max){
            $this->addError($attribute, 'Невірно вказана дата');
        }
    }
}