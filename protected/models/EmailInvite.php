<?php

class EmailInvite extends Invites
{
    public function init()
    {
        if (!empty(Yii::app()->user->data)) {
            $this->UserGUID = !empty(Yii::app()->user->data->ConsumerGUID) ? Yii::app()->user->data->ConsumerGUID : null;
        }

        parent::init();
    }

    /**
     * Проверяет существует ли запись с указанным EmailRequestID
     *
     * @param null $requestID
     *
     * @return bool
     */
    public function isExistsRequestID($requestID = null)
    {
        $exists = false;

        if ($requestID) {
            $exists = $this->exists("EmailRequestID=:EmailRequestID", array(
                    ":EmailRequestID" => $requestID)
            );
        }

        return $exists;
    }


    /**
     * @return Exception
     */
    public function updateInviteStatus()
    {
        try{
            $this->dbConnection->createCommand()->update($this->tableName(), array(
                'InviteStatus'   => $this->InviteStatus,
                'FriendUserGUID' => $this->FriendUserGUID
            ), 'EmailRequestID=:EmailRequestID', array(
                ':EmailRequestID' => $this->EmailRequestID
            ));
        } catch (Exception $e){
            return $e;
        }
    }
}