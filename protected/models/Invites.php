<?php

/**
 * This is the model class for table "{{invites}}".
 *
 * The followings are the available columns in table '{{invites}}':
 * @property integer $id
 * @property string $UserGUID
 * @property string $FriendUserGUID
 * @property string $FriendFacebookID
 * @property string $FriendEmail
 * @property string $InviteStatus
 * @property string $EmailRequestID
 * @property string $FacebookRequestID
 * @property string $CreatedAt
 * @property string $UpdatedAt
 */
class Invites extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invites the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->mysqlDb;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{invites}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UserGUID, InviteStatus', 'required'),
			array('UserGUID, FriendUserGUID', 'length', 'max'=>36),
			array('FriendFacebookID, EmailRequestID, FacebookRequestID', 'length', 'max'=>25),
			array('FriendEmail', 'length', 'max'=>255),
			array('InviteStatus', 'length', 'max'=>10),
			array('CreatedAt, UpdatedAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, UserGUID, FriendUserGUID, FriendFacebookID, FriendEmail, InviteStatus, EmailRequestID, FacebookRequestID, CreatedAt, UpdatedAt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'UserGUID' => 'User Guid',
			'FriendUserGUID' => 'Friend User Guid',
			'FriendFacebookID' => 'Friend Facebook',
			'FriendEmail' => 'Friend Email',
			'InviteStatus' => 'Invite Status',
			'EmailRequestID' => 'Email Request',
			'FacebookRequestID' => 'Facebook Request',
			'CreatedAt' => 'Created At',
			'UpdatedAt' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('UserGUID',$this->UserGUID,true);
		$criteria->compare('FriendUserGUID',$this->FriendUserGUID,true);
		$criteria->compare('FriendFacebookID',$this->FriendFacebookID,true);
		$criteria->compare('FriendEmail',$this->FriendEmail,true);
		$criteria->compare('InviteStatus',$this->InviteStatus,true);
		$criteria->compare('EmailRequestID',$this->EmailRequestID,true);
		$criteria->compare('FacebookRequestID',$this->FacebookRequestID,true);
		$criteria->compare('CreatedAt',$this->CreatedAt,true);
		$criteria->compare('UpdatedAt',$this->UpdatedAt,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}