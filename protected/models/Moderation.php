<?php

/**
 * Class Moderation
 * Модель для работы с Интерфейсом модератора
 *
 * @property mixed image
 * @property mixed pointsFor2
 */
class Moderation extends Anketa
{
    public $answers;
    public $pointsFor2;
    public $pointsFor5;
    public $pointsFor6;
    public $pointsFor8;
    public $moderationStatus;
    public $lastDate;
    public $FullName;
    public $Phone;
    public $Email;
    public $UserGUID;
    public $quizCount;
    public $points;


    public function getUsersList()
    {
        /*$this->dbConnection->createCommand()
            ->select('UserGUID')
            ->from($this->tableName())
            ->group('UserGUID');*/


        $dataProvider=new CActiveDataProvider('PassportAnketa', array(
            'criteria'=>array(
                'order'=>'TransactionDateTime DESC',
                'group'=>'UserGUID'
            ),

            'pagination'=>array(
                'pageSize'=>20,
            ),
        ));

        return $dataProvider;
    }

    public function getUserAnswers()
    {

    }

    public function getAnswers()
    {
        $data = $this->dbConnection->createCommand()
            ->select('*')
            ->from($this->tableName())
            //->where('UserGUID =:UserGUID AND QuestionID IN ("2","5","6","8")',array('UserGUID'=>$this->UserGUID))
            ->where('UserGUID =:UserGUID',array('UserGUID'=>$this->UserGUID))
            ->order('QuestionID')
            ->queryAll();

        foreach($data as $k){
            switch($k['QuestionID']){
                case 2 :
                    $this->setPointsFor2($k['Points']);
                    break;
                case 5 :
                    $this->setPointsFor5($k['Points']);
                    break;
                case 6 :
                    $this->setPointsFor6($k['Points']);
                    break;
                case 8 :
                    $this->setPointsFor8($k['Points']);
                    break;

                default:break;
            }
        }

        return $data;
    }

    public function updatePointsFor2()
    {
        $data = $this->dbConnection->createCommand()
            ->update($this->tableName(), array(
                'Points'=>$this->pointsFor2), 'UserGUID =:UserGUID AND QuestionID=2',array(':UserGUID'=>$this->UserGUID));

        return true;
    }

    public function updatePointsFor5()
    {
        $data = $this->dbConnection->createCommand()
            ->update($this->tableName(), array(
                'Points'=>$this->pointsFor5), 'UserGUID =:UserGUID AND QuestionID=5',array(':UserGUID'=>$this->UserGUID));

        return true;
    }
    public function updatePointsFor6()
    {
        $data = $this->dbConnection->createCommand()
            ->update($this->tableName(), array(
                'Points'=>$this->pointsFor6), 'UserGUID =:UserGUID AND QuestionID=6',array(':UserGUID'=>$this->UserGUID));

        return true;
    }

    public function updatePointsFor8()
    {
        $data = $this->dbConnection->createCommand()
            ->update($this->tableName(), array(
                'Points'=>$this->pointsFor8), 'UserGUID =:UserGUID AND QuestionID=8',array(':UserGUID'=>$this->UserGUID));

        return true;
    }

    public function updateModerationStatus()
    {
        $data = $this->dbConnection->createCommand()
            ->update($this->tableName(), array(
                'ModerationStatus'=>$this->ModerationStatus), 'UserGUID =:UserGUID',array(':UserGUID'=>$this->UserGUID));

        return true;
    }


    /**
     * @return mixed
     */
    public function getPointsFor2()
    {
        return $this->pointsFor2;
    }

    /**
     * @param mixed $pointsFor2
     */
    public function setPointsFor2($pointsFor2)
    {
        $this->pointsFor2 = $pointsFor2;
    }

    /**
     * @return mixed
     */
    public function getPointsFor5()
    {
        return $this->pointsFor5;
    }

    /**
     * @param mixed $pointsFor5
     */
    public function setPointsFor5($pointsFor5)
    {
        $this->pointsFor5 = $pointsFor5;
    }

    /**
     * @return mixed
     */
    public function getPointsFor6()
    {
        return $this->pointsFor6;
    }

    /**
     * @param mixed $pointsFor6
     */
    public function setPointsFor6($pointsFor6)
    {
        $this->pointsFor6 = $pointsFor6;
    }

    /**
     * @return mixed
     */
    public function getPointsFor8()
    {
        return $this->pointsFor8;
    }

    /**
     * @param mixed $pointsFor8
     */
    public function setPointsFor8($pointsFor8)
    {
        $this->pointsFor8 = $pointsFor8;
    }

    public function getQuizPoints(){
        $result = $this->dbConnection->createCommand()
            ->select('SUM(Points)')
            ->from('ncp_passport_quiz')
            ->where('UserGUID =:UserGUID',array('UserGUID'=>$this->UserGUID))
            ->queryScalar();
        
        return $result;
    }

    public static function getUserData($UserGUID)
    {

        $sql = "SELECT * FROM ncp_users WHERE ConsumerGUID LIKE '".$UserGUID."'";
        $data =  Yii::app()->mysqlDb->createCommand($sql)->queryRow();

        /*if($_COOKIE['dev_mode'] == 1 ){

            $sql = "SELECT * FROM ncp_users WHERE ConsumerGUID LIKE '".$UserGUID."'";
            $data =  Yii::app()->mysqlDb->createCommand($sql)->queryRow();

        } else {
            $model = new Users();

            $data = Yii::app()->cache->get('UserData_'.$UserGUID);
            if(empty($data)){
                $data = $model->getUser(array('ConsumerGUID'=>$UserGUID));
                Yii::app()->cache->set('UserData_'.$UserGUID, $data, 60 * 60);
            }
        }*/
        return $data;
    }

    public static function getUserName($UserGUID)
    {
        $data = self::getUserData($UserGUID);

        $result = !empty($data['FullName']) ? $data['FullName'] : '';

        return $result;
    }

    public static function getUserPhone($UserGUID)
    {
        $data = self::getUserData($UserGUID);

        $result = !empty($data['MainMobilePhone']) ? $data['MainMobilePhone'] : '';

        return $result;
    }

    public static function getUserEmail($UserGUID)
    {
        $data = self::getUserData($UserGUID);

        $result = !empty($data['EmailAddress']) ? $data['EmailAddress'] : '';

        return $result;
    }

    public static function getUserPoints($UserGUID)
    {
        $sql = "SELECT
                UserGUID AS guid,
                (
                    SELECT
                        SUM(Points)
                    FROM
                        ncp_passport_anketa
                    WHERE
                        UserGUID = `guid`
                ) AS `questPoints`,
                (
                    SELECT
                        SUM(Points)
                    FROM
                        ncp_passport_quiz
                    WHERE
                        UserGUID = `guid`
                ) AS `quizPoints`,
                (
                    SELECT
                        2
                    FROM
                        ncp_passport_share_log
                    WHERE
                        UserGUID = `guid`
                    AND PostID IS NOT NULL
                    LIMIT 1
                ) AS sharePoints
                FROM
                    ncp_passport_anketa
                WHERE UserGUID LIKE '$UserGUID'

                GROUP BY
                UserGUID
                ";
        $data =  Yii::app()->mysqlDb->createCommand($sql)->queryRow();

        return $data;
    }

    public static function getPointsToList($UserGUID){
        $points = self::getUserPoints($UserGUID);

        $total = (int)$points['questPoints']+ (int)$points['quizPoints'] + (int)$points['sharePoints'];
        $total .=!empty($points['sharePoints']) ? ' (Расшарил)' : '';

        return $total;

    }

    public static function getQuizCount($UserGUID)
    {
        $sql = "SELECT 'Да' from ncp_passport_quiz WHERE UserGUID LIKE '$UserGUID' AND Points>0 HAVING COUNT(Points)>=2 LIMIT 1";
        $data =  Yii::app()->mysqlDb->createCommand($sql)->queryScalar();

        return $data;
    }
}