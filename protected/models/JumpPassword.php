<?php

/**
 * This is the model class for table "{{jump_password_log}}".
 *
 * The followings are the available columns in table '{{jump_password_log}}':
 * @property integer $ID
 * @property string $RequestID
 * @property string $Msisdn
 * @property integer $Status
 * @property integer $Result
 * @property string $Password
 * @property integer $SmsParticipant
 * @property integer $Countries
 * @property string $TransactionDateTime
 */
class JumpPassword extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return JumpPassword the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->mysqlDb;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{jump_password_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RequestID', 'required'),
			array('Status, Result, SmsParticipant, Countries', 'numerical', 'integerOnly'=>true),
			array('RequestID, Msisdn, Password', 'length', 'max'=>50),
			array('TransactionDateTime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, RequestID, Msisdn, Status, Result, Password, SmsParticipant, Countries, TransactionDateTime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'RequestID' => 'Request',
			'Msisdn' => 'Msisdn',
			'Status' => 'Status',
			'Result' => 'Result',
			'Password' => 'Password',
			'SmsParticipant' => 'Sms Participant',
			'Countries' => 'Countries',
			'TransactionDateTime' => 'Transaction Date Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('RequestID',$this->RequestID,true);
		$criteria->compare('Msisdn',$this->Msisdn,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Result',$this->Result);
		$criteria->compare('Password',$this->Password,true);
		$criteria->compare('SmsParticipant',$this->SmsParticipant);
		$criteria->compare('Countries',$this->Countries);
		$criteria->compare('TransactionDateTime',$this->TransactionDateTime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}