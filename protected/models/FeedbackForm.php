<?php

/**
 * FeedbackForm class.
 * FeedbackForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class FeedbackForm extends CFormModel
{
	public $email;
	public $body;
	public $verifyCode;

    public function __construct()
    {
        if(!Yii::app()->user->isGuest)
            $this->email = Yii::app()->user->email;
    }

    public function beforeValidate()
    {
        if($this->body) $this->body = strip_tags($this->body);

        return parent::beforeValidate();
    }

    public function afterValidate()
    {
        return parent::afterValidate();
    }
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// email, body are required
			array('email, body', 'required', 'message'=>'Не заповнене поле {attribute}'),

            array('body', 'length', 'max'=>1000, 'tooLong'=>'Повідомлення може містити не більше 1000 символів'),

			// email has to be a valid email address
            array('email', 'email', 'checkMX'=>true, 'message'=>'Email введено невірно'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'message'=>'Код введено невірно'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
            'body'=>'Повідомлення',
            'email'=>'E-mail',
			'verifyCode'=>'Код перевірки',
		);
	}

    public function resetForm()
    {
        foreach($this->attributes as $k=>$v)
            $this->$k = null;
    }
}