<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
return array(
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'       => 'BUD Passport',
    'language'   => 'uk',
    'theme'      =>'bootstrap',

    // preloading 'log' component
    'preload'    => array('log'),

    // autoloading model and component classes
    'import'     => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.Zend.*',

        //facebook
        'application.extensions.eoauth.*',
        'application.extensions.eoauth.lib.*',
        'application.extensions.lightopenid.*',
        'application.extensions.eauth.services.*',
    ),

    'modules'    => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class'     => 'system.gii.GiiModule',
            'password'  => 'Es8Un2Mup',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('212.109.49.250','195.211.60.156', '::1'),
            'generatorPaths'=>array(
                'bootstrap.gii',
            ),
        ),
    ),

    // application components
    'components' => array(
        'cache'  => array(
            'class'  => 'system.caching.CFileCache',
        ),
        'bootstrap'=>array(
            'class'=>'bootstrap.components.Bootstrap',
        ),
        'request'=>array(
            'enableCsrfValidation'=>false,
        ),
        'curl' => array(
            'class' => 'ext.curl.Curl',
            'options' => array()
        ),
        'user'         => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl'       => array('site/registration'),
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager'   => array(
            'urlFormat'      => 'path',
            'showScriptName' => false, // убирааем index.php
            'rules'          => array(

                '/'=>'passport/app',
                'registration'=>'site/registration',
                '<controller:\w+>/<id:\d+>'              => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
            ),
        ),

        // MySQL database config
        'mysqlDb'      => array(
            'class'              => 'CDbConnection',
            'connectionString'   => 'mysql:host=localhost;dbname=bud2',
            'emulatePrepare'     => true,
            'username'           => 'bud2',
            'password'           => 'taiwan8additions',
            'charset'            => 'utf8',
            'tablePrefix'        => 'ncp_',
            'enableProfiling'    => true,
            'enableParamLogging' => true
        ),

        // MSSQL config
        'db'           => array(
            'connectionString'   => 'dblib:host=crm.rappukraine.com:1433;dbname=SunInBevUkraine_MSCRM',
            'username'           => 'crm.rapp.web.agent',
            'password'           => 'Inerfnehrf$1',
            'enableProfiling'    => true,
            'enableParamLogging' => true,
            'charset'            => 'utf-8'
        ),

        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log'          => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                /*array(
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),*/
                // uncomment the following to show log messages on web pages

                // firebug logger
                array(
                    'class' => 'CWebLogRoute',
                    'levels'=>'trace',
                    'categories'=>'vardump',
                    'showInFireBug' => true
                ),
            ),
        ),

        //facebook
        'loid'         => array(
            'class' => 'application.extensions.lightopenid.loid',
        ),
        'eauth'        => array(
            'class'    => 'application.extensions.eauth.EAuth',
            'popup'    => true, // Use the popup window instead of redirecting.
            'services' => array( // You can change the providers and their classes.
                'facebook' => array(
                    'class'         => 'FacebookOAuthService',
                    'client_id'     => '616994881719173',
                    'client_secret' => '3c48f2231f6186b80e99a27edaa1ffc8',
                )
            ),
        ),

        /* 'cache'=>array(
                'class'=>'CDbCache',
            ),*/

        /*'cache' => array(
            'class' => 'system.caching.CFileCache'
        ),*/
    ),

    // дополнительные параметры
    'params'     => array(
        'adminEmail'   => '',
        'stopActivity' => false,
        'activity' => array(
            'name'=>'NCP2014-Passport2Rio',
            'guid'=>'97446F7D-A7A3-E311-8F3F-000C29E38BCE',
            'passport2rio' => array(
                'name' => 'NCP2014-Passport2Rio',
                'guid' => '97446F7D-A7A3-E311-8F3F-000C29E38BCE',
            ),
        ),

        'moderators' => array(
            '380672205280', //я
            '380674306337', //Леша
            '380502083055', //Таня
            '380677533644', //Вика
        )
    ),
);