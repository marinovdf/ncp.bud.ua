<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UserGUID')); ?>:</b>
	<?php echo CHtml::encode($data->UserGUID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Phone')); ?>:</b>
	<?php echo CHtml::encode($data->Phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('QuestionID')); ?>:</b>
	<?php echo CHtml::encode($data->QuestionID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('QuestionStatus')); ?>:</b>
	<?php echo CHtml::encode($data->QuestionStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Answer')); ?>:</b>
	<?php echo CHtml::encode($data->Answer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Points')); ?>:</b>
	<?php echo CHtml::encode($data->Points); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TransactionDateTime')); ?>:</b>
	<?php echo CHtml::encode($data->TransactionDateTime); ?>
	<br />

	*/ ?>

</div>