<?php
$this->breadcrumbs=array(
	'Пользователи',
);
/*
$this->menu=array(
	//array('label'=>'Create PassportAnketa','url'=>array('create')),
	//array('label'=>'Manage PassportAnketa','url'=>array('admin')),
);*/
?>
<style type="text/css">
    .row>.span9{
        width: 1170px;
    }
</style>
<h1>Пользователи</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'passport-anketa-grid',
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'columns'=>array(
        array('name'=>'UserGUID', 'header'=>'ID пользователя'),
        array('name'=>'FullName', 'header'=>'Имя'),
        array('name'=>'Phone', 'header'=>'Телефон'),
        array('name'=>'Email', 'header'=>'Email'),
        /*
        'Points',
        'TransactionDateTime',
        */
        array('name'=>'QuestionStatus', 'header'=>'Статус ответов'),
        array('name'=>'ModerationStatus', 'header'=>'Статус модерации'),
        array('name'=>'quizCount', 'header'=>'Quiz Пройден', 'value'=>'Moderation::getQuizCount($data->UserGUID)','htmlOptions'=>array('width'=>'50')),
        array('name'=>'points', 'header'=>'Сумма баллов', 'value'=>'Moderation::getPointsToList($data->UserGUID)'),
        array('name'=>'lastDate', 'header'=>'Дата', 'htmlOptions'=>array('width'=>'150')),
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}',
            'buttons'=>array
            (
                'view' => array
                (
                    'label'=>'Переглянути',
                    'url'=>'Yii::app()->createUrl("cpanel/admin/update", array("id"=>$data->id))',
                    'options'=>array(
                        //'class'=>'btn btn-small',
                    ),
                ),
            ),
        ),
    ),
)); ?>
