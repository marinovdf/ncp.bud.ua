<?php
$this->breadcrumbs=array(
	'Анкета'=>array('index'),
	'Управление',
);

$this->menu=array(
	array('label'=>'Список','url'=>array('index')),
	array('label'=>'Создать','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('moderation-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Модерация</h1>

<?php echo CHtml::link('Расширеный поиск','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php /*$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'passport-anketa-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'UserGUID',
		'Phone',
		'QuestionID',
		'QuestionStatus',
		'Answer',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
));*/ ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'passport-anketa-grid',
    'dataProvider'=>$model->getUsersList(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'UserGUID',
        'Phone',
        'QuestionID',
        'QuestionStatus',
        'Answer',
        /*
        'Points',
        'TransactionDateTime',
        */
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>

