<?php
$this->breadcrumbs=array(
	'Список'=>array('users'),
	//$model->id=>array('view','id'=>$model->id),
	'Модерация',
);

$this->menu=array(
	array('label'=>'Список пользователей','url'=>array('users')),
	//array('label'=>'Create PassportAnketa','url'=>array('create')),
	//array('label'=>'View PassportAnketa','url'=>array('view','id'=>$model->id)),
	//array('label'=>'Manage PassportAnketa','url'=>array('admin')),
);
?>
<?php
$user = new Users();
$userData = $user->getUser(array('ConsumerGUID'=>$model->UserGUID));
?>
<h1>Модерация анкеты</h1>
<h2><?php echo $model->UserGUID; ?></h2>
<ul style="list-style-type: none; margin: 10px 0;">
    <li><?php echo !empty($userData['FullName']) ? $userData['FullName'] : ''; ?></li>
    <li><?php echo !empty($userData['BirthDate']) ? $userData['BirthDate'] : ''; ?></li>
    <li><?php echo !empty($userData['MainMobilePhone']) ? $userData['MainMobilePhone'] : ''; ?></li>
    <li><?php echo !empty($userData['EmailAddress']) ? $userData['EmailAddress'] : ''; ?></li>
</ul>

<?php
echo $this->renderPartial('_form',array('model'=>$model));
?>