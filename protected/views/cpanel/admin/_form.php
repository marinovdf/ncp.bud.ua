<?php
/**
 * @var $model Moderation
 * @var $form CActiveForm
 */
?>

<style type="text/css">
    label{
        display: inline-block;
    }
    .moderation-wr{
        margin-bottom: 20px;
    }

</style>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'                   => 'moderation-form',
    'enableAjaxValidation' => false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php $model->answers = $model->getAnswers();?>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Номер вопроса</th>
        <th>Ответ</th>
        <th>Баллы</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($model->answers as $answer) : ?>
    <tr>
        <td><?php echo $answer['QuestionID'] ?></td>
        <td>
            <?php
            switch ($answer['QuestionID']) {
                case 1 :
                case 7 :
                    echo $answer['Answer'];
                    break;

                case 2 :
                case 6 :
                    echo $answer['InputText'];
                    break;
                case 5 :
                    echo CHtml::image('/media/images/'.$model->UserGUID.'.png');
                    break;

                case 3 :
                    $ans = unserialize($answer['Answer']);
                    foreach($ans as $k){
                        echo $k['id'].'<br>';
                    }
                    //print_r($ans);
                    break;
                case 4 :
                    $ans = unserialize($answer['Answer']);
                    foreach($ans as $k){
                        if(!empty($k['inputText'])){
                            echo $k['id'].' - '.$k['inputText'].'<br>';
                        } else {
                            echo $k['id'].'<br>';
                        }

                    }
                    //print_r($ans);
                    break;

                case 8 :
                    $ans = unserialize($answer['Answer']);
                    foreach($ans as $k){
                        echo $k['id'].' - '.$k['inputText'].'<br>';
                    }
                    //print_r($ans);
                    break;
                default :
                    echo $answer['Answer'];
                    break;

            }
            ?>
        </td>
        <td>
            <?php
            switch ($answer['QuestionID']) {
                case 1:
                case 3:
                case 4:
                case 7:
                    echo $answer['Points'];
                    break;

                case 2 :
                    echo $form->dropDownList($model,'pointsFor2', array(0,1,2,3,4,5,6,7,8,9,10), array('class'=>'small'));
                    break;
                case 6 :
                    echo $form->dropDownList($model,'pointsFor6', array(0,1,2,3,4,5,6,7,8,9,10), array('class'=>'small'));
                    break;
                case 5 :
                    echo $form->dropDownList($model,'pointsFor5', array(0,1,2,3,4,5), array('class'=>'small'));
                    break;
                case 8 :
                    echo $form->dropDownList($model,'pointsFor8', array(0,1,2,3,4,5,6,7,8,9,10), array('class'=>'small'));
                    break;
                default :
                    echo '';
                    break;

            }
            ?>
            <?php //echo $answer['Points'] ?>
        </td>
    </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="2">Баллы за квиз</td>
        <td><?php echo $model->getQuizPoints() ?></td>
    </tr>
    </tbody>

</table>

<div class="form-actions">
    <div class="moderation-wr">
        <?php echo $form->checkBox($model,'ModerationStatus', array('uncheckValue'=>NULL)) ?>
        <?php echo $form->label($model,'Статус модерации') ?>
    </div>

    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'label'=>'Сохранить',
        'loadingText'=>'loading...',
        'htmlOptions'=>array('id'=>'buttonStateful'),
    )); ?>
</div>

<?php $this->endWidget(); ?>
<?php Yii::trace(CVarDumper::dumpAsString($model->attributes),'vardump'); ?>