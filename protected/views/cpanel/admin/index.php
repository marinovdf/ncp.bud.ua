<?php
$this->breadcrumbs=array(
	'Ответы на вопросы анкеты',
);

$this->menu=array(
	array('label'=>'Create PassportAnketa','url'=>array('create')),
	array('label'=>'Manage PassportAnketa','url'=>array('admin')),
);
?>

<h1>Ответы на вопросы анкеты</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
