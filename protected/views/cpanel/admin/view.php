<?php
$this->breadcrumbs=array(
	'Passport Anketas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PassportAnketa','url'=>array('index')),
	array('label'=>'Create PassportAnketa','url'=>array('create')),
	array('label'=>'Update PassportAnketa','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete PassportAnketa','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PassportAnketa','url'=>array('admin')),
);
?>

<h1>View PassportAnketa #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'UserGUID',
		//'Phone',
		'QuestionID',
		'QuestionStatus',
		'Answer',
		'Points',
		'TransactionDateTime',
	),
)); ?>
