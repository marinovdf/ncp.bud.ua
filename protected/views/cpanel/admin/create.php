<?php
$this->breadcrumbs=array(
	'Passport Anketas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PassportAnketa','url'=>array('index')),
	array('label'=>'Manage PassportAnketa','url'=>array('admin')),
);
?>

<h1>Create PassportAnketa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>