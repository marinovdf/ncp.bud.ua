<?php
/**
 * Шаблона страницы "Подарки"
 *
 * @var $this SiteController
 */
$this->pageTitle = Yii::app()->name . ' | Подарунки';
?>
<div id="gifts-page" class="accordion-page">
    <!-- Accordion -->
    <ul class="accordion gifts-accordion" id="main-accordion">
        <li class="gift-item-1" data-slide="1">
            <div class="accordion-content">
                <p class="white-color">Наважся на більше</p>
            </div>
            <div class="arrow"></div>
        </li>
        <li class="gift-item-2" data-slide="2">
            <div class="accordion-content">
                <p class="white-color">На пульсі розваг</p>
            </div>
            <div class="arrow"></div>
        </li>
        <li class="gift-item-3" data-slide="3">
            <div class="accordion-content">
                <p class="white-color">Заряд емоцій</p>

            </div>
            <div class="arrow"></div>
        </li>
        <li class="last gift-item-4" data-slide="4">
            <div class="accordion-content">
                <p class="white-color">У ритмі серця</p>

            </div>
            <div class="arrow"></div>
        </li>
    </ul>
    <!-- End of Accordion -->

    <!-- Accordion descriptions -->
    <div class="accordion-description">
        <p class="descr">
            Що ти хочеш у подарунок: захоплюючий екстрім-досвід, запрошення на шалену вечірку, грандіозну вилазку з
            друзями чи омріяний майстер-клас? Пройди BUD-тест, дізнайся про свій темперамент та виграй неповторний
            подарунок, котрий ідеально пасуватиме саме до твого характеру.
        </p>

        <p class="descr-1 blue-color" style="display: none">
            Підкорити стихії, приборкати страх та зловити хвилю позитиву — це під силу винятково відважним сміливцям.
            Стрибок з парашутом, вейкборд на двох чи шалений рафтинг — для шукачів пригод Король Пива приготував
            характерні подарунки.
        </p>

        <p class="descr-2 blue-color">
            Вирушити на грандіозну вилазку з друзями, влаштувати незабутню вечірку, розважитись на повну та приголомшити
            всіх — такий запальний сценарій очікує на тебе та твою веселу компанію. Яхтинг, картинг, аквапарк, караоке
            чи боулінг — що тобі та твоїм друзям більше до вподоби?
        </p>

        <p class="descr-3 blue-color">
            Зробити Upgrade, спробувати себе у новій ролі, вийти на Level Up, зарядитись позитивними емоціями — за такою
            схемою працюють амбітні й цілеспрямовані. Курс професійної фотографії, екскурсія на кіностудію, майстер-клас
            з великого тенісу — для рішучих та наполегливих Король Пива приготував особливі подарунки.
        </p>

        <p class="descr-4 blue-color">
            Тримати руку на пульсі розваг, жити у ритмі музики, віддатись спокусі й вирушити назустріч пригодам — коли
            місто засинає, прокидається шалена тусовка й запалює аж до самого ранку. Запрошення на шалене party,
            омріяний майстер-клас від улюбленого DJ — отримай свій приз від Короля Пива.
        </p>
    </div>
    <!-- End of Accordion descriptions -->
</div>

<!-- Winners Popup -->
<?php // $this->renderPartial('popup-winners') ?>
<!-- End of Winners Popup -->