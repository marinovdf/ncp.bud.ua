<?php
/**
 * Шаблон блока авторизации + форма ввода логина/пароля
 * @var $this SiteController
 */
?>
<style type="text/css">
    .reminder-lnk{
        position: absolute;
        left: 130px;
        top: 0;
        color: #bd0d2a;
        font-size: 14px;
        margin-right: 15px;
        text-decoration: none;
        display: inline-block;
        vertical-align: middle;
        border-bottom: 2px solid transparent;
        margin-top: 12px;
        transition: ease-out .5s;
    }

    .reminder-lnk:hover{
        border-bottom: 2px solid #bd0d2a;
    }
</style>
<div class="login-block">
    <div class="login-form left">
        <?php
        $loginModel = new LoginForm();

        if (Yii::app()->request->isPostRequest) {
            if ($post = Yii::app()->request->getPost('LoginForm')) {
                $loginModel->attributes = $post;

                // validate user input and redirect to the previous page if valid
                if ($loginModel->validate() && $loginModel->login()) {
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            }
        }
        $this->renderPartial('//site/login', array('model' => $loginModel));
        ?>
    </div>


        <!--<div class="login-btns">
            <a href="#" class="btn"
               onclick="$('#login-form').submit()">Вхід</a><?php /*echo CHtml::link('Реєстрація', Yii::app()->createUrl('site/registration'), array('class' => 'btn fb-btn', 'id'=>'top-reg-btn')) */?>
        </div>-->
</div>