<?php
/**
 * Шаблон верхнего меню
 *
 * @var $this SiteController
 */
?>
<nav class="left">
    <?php $this->widget('zii.widgets.CMenu', array(
        'activeCssClass' => 'mitem-active',
        'items'          => array(
            array('label' => 'Головна', 'url' => array('/site/index')),
            array('label' => 'Правила', 'url' => array('/site/rules'),'template'=>'| {menu}'),
            array('label' => 'Тест', 'url' => array('/test'), 'template'=>'| {menu}'),
            array('label' => 'Подарунки', 'url' => array('/site/gifts'), 'template'=>'| {menu}'),
            array('label' => 'Запросити друга', 'url' => array('/invite'), 'template'=>'| {menu}'),
            array('label' => "Зворотній зв’язок", 'url' => array('/site/feedback'), 'template'=>'| {menu}'),
        ),
    )); ?>
</nav>
<div class="login-btns right">
    <a href="http://bud.ua" target="_blank" style="width: 180px; margin-top: 5px;">Перейти на bud.ua</a>
    <?php if (Yii::app()->user->isGuest) : ?>
        <a href="#" id="login-btn">Вхід</a>
    <?php else: ?>
        <?php if(!empty(Yii::app()->user->data->ConsumerGUID)) : ?>
            <?php echo '<span>Вітаємо, </span>' . CHtml::link(Yii::app()->user->name, Yii::app()->createUrl('/profile'), array('class' => 'profile-lnk')); ?>
        <?php endif ?>

        <?php echo CHtml::link('Вихід', array('site/logout'), array('id' => 'logout-btn')) ?>
    <?php endif ?>
</div>