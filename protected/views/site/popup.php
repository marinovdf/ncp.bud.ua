<?
/**
 * @var $title string sPopup title
 * @var $content string Popup content
 */
?>


<div class="overlay">
    <div class="popup">
        <span class="close-popup-btn"></span>
        <div class="title"><?php echo !empty($title) ? $title : '' ?></div>
        <div class="content"><?php echo !empty($content) ? $content : ''; ?></div>
        <input class="ok-btn btn red-bg-color" type="button" value="Ok"/>
    </div>
</div>
<script type="text/javascript">
    $('.ok-btn, .close-popup-btn').live('click', function(){$(this).parents('.overlay').hide()})
</script>
