<?php
/* @var $this SiteController */
/* @var $model ValidatePhone */
/* @var $form CActiveForm */
?>
<div class="overlay">
    <div class="popup">
        <div class="title">Перевірка номеру!</div>
        <div class="content">
            На ваш мобільний телефон був відправлений код перевірки. Введіть його у поле, що знаходиться знизу.
        </div>
        <div class="validation-form">
            <?php echo CHtml::textField('ValidatePhone[userCode]', $model->userCode, array('id'=>'validate-phone-inp')) ?>
            <?php echo CHtml::hiddenField('ValidatePhone[ValidateCode]', $model->ValidateCode) ?>
            <div class="error-summary">
                <?php echo $model->getError('userCode') ?>
            </div>
                <?php echo CHtml::submitButton('Перевірити', array('class' => 'check-code-submit', 'id'=>'check-code-btn')); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.ok-btn').on('click', function(){$(this).parents('.overlay').hide()})
</script>