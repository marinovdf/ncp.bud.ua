<?
/**
 * @var $title string sPopup title
 * @var $content string Popup content
 */
?>
<style type="text/css">
    .btns-wr{
        margin-top: 20px;
    }

    .ok-btn-wr{
        display: none;
    }
</style>
<div class="overlay">
    <div class="popup">
        <div class="title"><?php echo $title?></div>
        <div class="content">
            <div class="text">Тобі виповнилося 18 років?</div>
            <div class="btns-wr align-center">
                <div class="age-btn-wr">
                    <input class="age-btn" type="button" value="ТАК" style="margin-right: 2px"/> <input class="age-btn" type="button" value="НІ"/>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.age-btn').on('click', function(){
        if($(this).val()=='ТАК'){
            window.location.href = '/video/record';
            var date = new Date( new Date().getTime() + 15*24*60*60*1000 );
            document.cookie="age_valid=1; path=/; expires="+date.toUTCString();
        } else {
            $(this).parents('.overlay').find('.text').text('Нажаль ваш вік менший за 18 років, тому ви не зможете відвідати сайт.');
            $('.age-btn-wr').hide();
        }
    });
</script>
