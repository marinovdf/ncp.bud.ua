<?php
/**
 * @var $this SiteController
 * @var $form CActiveForm
 * @var $model FeedbackForm
 */
?>

<style type="text/css">
    #s2id_ReminderForm_operatorCode{
        width: 230px;
    }



    .table-row{
        display: table-row;
    }
    .table-cell{
        display: table-cell;
    }

    .table{
        display: table;
    }

    #remind-form .table-cell{
        vertical-align: top;
        padding-bottom: 20px;
        padding-right: 20px;
    }
</style>

<div id="reminder-page" class="column2">
    <div class="table-row">
        <div class="left-side red-bg-color border-box">
            <div class="vertical-arrow"></div>
            <img src="/images/reminder-icon.png" alt="" class="left" style="margin-right: 10px;"/>

            <h1 style="overflow: hidden; padding-top: 30px">Нагадати пароль</h1>

            <p class="subtitle" style="padding-left: 70px">
                Отакої, забув пароль?<br>
                Зараз ми тобі його нагадаємо.
            </p>
        </div>
        <div class="right-side border-box">

            <div class="form">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'remind-form',
                    'enableClientValidation' => false,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                )); ?>


                <?php
                if ($form->errorSummary($model))
                    Yii::app()->user->setFlash('ShowPopup', array('title' => 'Помилка', 'content' => $form->errorSummary($model, '')));

                // $this->renderPartial('//site/popup', array('title' => 'Помилка', 'content' => $form->errorSummary($model, '')));
                ?>

                <?php if (CCaptcha::checkRequirements()): ?>
                    <div class="table captcha" style="width: 470px;">

                        <div class="table-row">
                            <div class="table-cell">
                                <?php echo $form->dropDownList($model, 'operatorCode', $model->getOperatorCodeList(), array('empty' => 'Код*', 'class' => 'mob-code-sel')); ?>

                            </div>
                            <div class="table-cell">
                                <?php $this->widget('CMaskedTextField', array(
                                    'model'       => $model,
                                    'attribute'   => 'Phone',
                                    'mask'        => '9999999',
                                    'placeholder' => '_',
                                    'htmlOptions' => array(
                                        'size'         => 17,
                                        'maxlength'    => 17,
                                        'autocomplete' => 'off',
                                        'placeholder'  => 'Телефон*',
                                        'class'        => 'mob-phone-inp'
                                    )
                                ));
                                ?>
                            </div>
                        </div>


                        <div class="table-row">
                            <div class="table-cell">
                                <?php

                                $this->widget('CCaptcha', array(
                                        'imageOptions' => array('height' => '135', 'width' => '230', 'id'=>'captcha-img', 'title'=>'Оновити код'),
                                        'clickableImage'=>true,
                                        'showRefreshButton'=>false,
                                        //'buttonType' => 'link',
                                        //'buttonLabel' => 'Новий код',
                                        //'buttonOptions' => array('class' => 'refresh-lnk')
                                    )
                                );

                                ?>
                            </div>
                            <div class="table-cell" style="vertical-align: middle">
                                <div class="hint">
                                    Будь ласка, введи тут символи,<br>
                                    які ти бачиш на зображенні:
                                </div>
                                <div class="verify-code-wr">
                                    <?php echo $form->textField($model, 'verifyCode', array('class' => 'verify-inp')); ?>
                                </div>
                                <div>
                                    <?php echo CHtml::submitButton('Нагадати', array('class' => 'submit-btn red-btn')); ?>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php endif; ?>
                <div class="align-center">

                </div>
                <?php $this->endWidget(); ?>
            </div>


        </div>
    </div>

    <!-- Accordion descriptions -->
    <div class="accordion-description footer-text">
        <p class="blue-color">
            Друзі вважають тебе непересічним авантюристом, колеги пліткують: «він ще той дивак», сусіди скаржаться на гучні
            вечірки, а кохана людина глибоко переконана, що ти – завзятий ентузіаст з великим майбутнім, лише матуся наївно
            вірить, що опівночі ти солодко спиш під ковдрою. Настав час дізнатись, який ти насправді.
        </p>
    </div>
    <!-- End of Accordion descriptions -->
</div>