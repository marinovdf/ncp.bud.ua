<?php
/* @var $this SiteController */
/* @var $model RegistrationForm */
/* @var $form CActiveForm */
$this->pageTitle = Yii::app()->name . ' | Реєстрація';
?>
<div class="registration-wr">
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'                   => 'register-form',
    'enableAjaxValidation' => false,
)); ?>
<div class="form-wr">
    <!-- Title -->
    <div class="row-fluid title-wr">
        <div class="span2">
            <img src="/images/icons/registration-icon.png" alt="" class="title-icon"/>
        </div>
        <div class="span3 title">
            Реєстрація
        </div>
    </div>
    <!-- End of Title -->

    <!-- Row #1 -->
    <div class="row-fluid">
        <div class="span3 offset2">
            <?php echo $form->textField($model, 'LastName', array('placeholder' => 'Прізвище*', 'class' => 'span12')) ?>
        </div>
        <div class="span3">
            <?php echo $form->textField($model, 'FirstName', array('placeholder' => "Ім’я*", 'class' => 'span12')); ?>
        </div>
        <div class="span3">
            <?php echo $form->textField($model, 'Patronymic', array('placeholder' => "По-батькові", 'class' => 'span12')); ?>
        </div>
    </div>
    <!-- End of Row #1 -->

    <!-- Row #2 -->
    <div class="row-fluid">
        <div class="span3 offset2">
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'       => $model,
                'attribute'   => 'Birthday',
                'language'    => 'ua',
                'themeUrl'    => Yii::app()->baseUrl . '/css/jui',
                'theme'       => 'custom-theme',
                'cssFile'     => 'jquery-ui-1.9.2.custom.min.css',
                'options'     => array(
                    'dateFormat'        => 'yy.mm.dd', // format
                    'showOtherMonths'   => true, // show dates in other months
                    'selectOtherMonths' => true, // can select dates in other months
                    'changeYear'        => true, // can change year
                    'changeMonth'       => true, // can change month
                    'yearRange'         => '1930:1995', // range of year
                    'minDate'           => '1930.01.01', // minimum date
                    'maxDate'           => date((date('Y') - 18) . '.m.d'), // maximum date
                ),
                'htmlOptions' => array(
                    'maxlength'   => '10', // textField maxlength
                    'class'       => 'input-calendar span12',
                    'placeholder' => 'Дата народження*'
                ),

            ));
            ?>
        </div>

        <div class="span3">
            <?php echo $form->dropDownList($model, 'Gender', $model->getGenderList(),
                array(
                    'class'=>'span12',
                    'empty'   => 'Cтать*',
                ));
            ?>
        </div>

        <div class="span3">
            <?php echo $form->textField($model, 'Email', array('placeholder' => 'E-mail*', 'class'=>'span12')) ?>
        </div>
    </div>
    <!-- End of Row #2 -->

    <!-- Row #3 -->
    <div class="row-fluid">
        <div class="span3 offset2">
            <?php
            echo $form->dropDownList($model, 'Oblast', CHtml::listData($model->getOblastList(), 'OblastName', 'OblastName'),
                array(
                    'options' => array(
                        $model->Oblast => array('selected' => true)
                    ),
                    'empty'   => 'Область*',
                    'class'   => 'span12',
                    'ajax'    => array(
                        'type'   => 'POST', //request type
                        'url'    => Yii::app()->createUrl('site/getRayonList'), //url to call.
                        'update' => '.rayon-name', //selector to update
                        'data'   => 'js:{OblastName:this.value, YII_CSRF_TOKEN:"'.Yii::app()->request->csrfToken.'"}'
                    )));
            ?>
        </div>
        <div class="span3">
            <?php
            echo $form->dropDownList($model, 'Rayon', CHtml::listData(
                    $model->getRayonList(), 'RayonName', 'RayonName'),
                array(
                    'empty' => 'Район*',
                    'class' => 'rayon-name span12',
                    'ajax'  => array(
                        'type'   => 'POST', //request type
                        'url'    => Yii::app()->createUrl('site/getCityList'), //url to call.
                        'update' => '.city-name', //selector to update
                        'data'   => 'js:{RayonName:this.value, YII_CSRF_TOKEN:"'.Yii::app()->request->csrfToken.'"}'
                    )
                ));
            ?>
        </div>

        <div class="span3">
            <?php echo $form->dropDownList($model, 'CityName', CHtml::listData($model->getCityList(), 'CityName', 'CityName'), array('empty' => 'Населений пункт*', 'class' => 'city-name span12')); ?>
        </div>
    </div>
    <!-- End of Row #3 -->

    <!-- Row #4 -->
    <div class="row-fluid">
        <div class="span3 offset2">
            <?php echo $form->dropDownList($model, 'operatorCode', $model->getOperatorCodeList(),
                array(
                    'empty' => 'Код*',
                    'class' => 'mob-code-sel span12'
                ));
            ?>
        </div>
        <div class="span3">
            <?php $this->widget('CMaskedTextField', array(
                'model'       => $model,
                'attribute'   => 'MobilePhone',
                'mask'        => '9999999',
                'placeholder' => '_',
                'htmlOptions' => array(
                    'size'         => 17,
                    'maxlength'    => 17,
                    'autocomplete' => 'off',
                    'placeholder'  => 'Телефон*',
                    'class'        => 'mob-phone-inp span12'
                )
            ));
            ?>
        </div>

        <div class="span3">
            <?php //echo $form->textField($model, 'Password', array('placeholder' => 'Пароль*', 'class'=>'span12')) ?>
        </div>
    </div>
    <!-- End of Row #4 -->

    <!-- Disclaimer -->
    <div class="row-fluid">
        <div class="span9 offset2">
            <table id="disclaimer">
                <tr valign="top">
                    <td><?php echo $form->checkBox($model, 'DisclaimerCheckBox'); ?></td>
                    <td>Цим я підтверджую, що мені виповнилось 18 років, i що всі
                        наведені мною персональні дані є
                        достовірними. У відповідності до Закону України «Про захист персональних даних» я
                        добровільно та безоплатно надаю ТОВ«РАПП Україна» мої персональні дані та надаю мою згоду на
                        будь-яку безстрокову обробку наданих
                        мною моїх персональних даних у базі персональних даних «Споживачі», з метою надання
                        повнолітнім споживачам інформації рекламного характеру про будь-яку особу чи будь-який
                        товар, а також згоду на передачу/надання моїх персональних даних будь-яким третім
                        особам,
                        включаючи іноземних осіб. Я підтверджую, що погоджуюся отримувати вищезазначену інформацію
                        будь-якими
                        засобами її передачі (пошта, СМС/ММС-повідомлення, електронна пошта, тощо) протягом
                        необмеженого строку.
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- End of Disclaimer -->

    <!-- Submit -->
    <div class="row-fluid">
        <input type="submit" value="Зберегти" class="span3"/>
    </div>
    <!-- End of Submit -->
</div>
<?php
####################    Валидация телефона   ####################
if ($form->error($model, 'phoneVerified')) {

    $validationModel              = new ValidatePhone();
    $validationModel->PhoneNumber = $model->operatorCode . $model->MobilePhone;

    if ($validationModel->checkCountCode() > 3) {
        Yii::app()->user->setFlash('ShowPopup', array('title' => 'Помилка', 'content' => 'Використана максимальна кількість спроб реєстрацій на добу'));
    } else {
        if (isset($_POST["ValidatePhone"])) {
            $validationModel->ValidateCode = $_POST["ValidatePhone"]['ValidateCode'];
            $validationModel->userCode     = $_POST["ValidatePhone"]['userCode'];

            if ($validationModel->validate()) {
                $validationModel->saveSuccess();
                $model->phoneVerified = true;

                // флага валидации телефона
                $model->PhoneValidStatusCode = 'VALID';

                //сохраняем основную форму
                if ($model->save()) {


                    $sms = new Sms();
                    $text = "BUD vitaye! Tviy parol do saitu: ".$model->Password." Info: 0800508886";
                    $phone = $model->operatorCode.$model->MobilePhone;

                    $sms->sendSms($phone, $text);
                    //заносим во флеш-переменную статус регистрации
                    Yii::app()->user->setFlash('RegisterStatus', 'success');
                    Yii::app()->user->setFlash('RegistrationSuccessful', '1');
                    //Показыаваем попап с инфой об успешной регистрации
                    Yii::app()->user->setFlash('ShowPopup', array('title' => 'Повідомлення', 'content' => 'Вітаємо з успішною реєстрацією!'));

                    //автовход
                    $login             = new LoginForm();
                    $login->attributes = array(
                        'phone'    => $validationModel->PhoneNumber,
                        'password' => $model->Password
                    );

                    if ($login->login())
                        Yii::app()->user->setFlash('AutoAuth', '1');

                    $this->redirect(Yii::app()->createUrl('passport/app'));
                    Yii::app()->end();
                } else {
                    $this->renderPartial('//site/popup', array('title' => 'Помилка', 'content' => 'Виникла помилка! Спробуйте зареєструватись через Кілька годин.'));

                    // ЕСЛИ УБРАТЬ, ТО БУДЕТ ОТОБРАЖАТЬСЯ ПОПАП С ВАЛИДАЦИЕЙ ТЕЛЕФОНА !!!
                    $notRenderVaidatePopup = false;
                }
            }

            if (empty($notRenderVaidatePopup)) {
                $this->renderPartial('//site/validatePhone', array('model' => $validationModel));
            }
        } else {
            //отправляем смс
            $validationModel->ValidateCode = $validationModel->genCode();
            $validationModel->sendSms($validationModel->PhoneNumber, $validationModel->ValidateCode);

            //рендерим форму валидации телефона
            $this->renderPartial('//site/validatePhone', array('model' => $validationModel));
        }

    }
} else {
    if ($form->errorSummary($model))
        $this->renderPartial('//site/popup', array('title' => 'Помилка', 'content' => $form->errorSummary($model, '')));
}

?>
<?php $this->endWidget(); ?>
</div>