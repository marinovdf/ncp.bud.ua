<?php
/**
 * @var $this SiteController
 * @var $form CActiveForm
 * @var $model FeedbackForm
 */
$this->pageTitle = Yii::app()->name . ' | Зворотній зв’язок';
?>
<div id="feedback-page" class="column2">
    <div class="table-row">
        <div class="left-side red-bg-color border-box">
            <div class="vertical-arrow"></div>
            <img src="/images/feedback-icon.png" alt="" class="left" style="margin-right: 10px;"/>

            <h1 style="overflow: hidden; padding-top: 7px">Зворотній зв’язок</h1>

            <p class="subtitle" style="padding-left: 70px">
                Є, що сказати? Поділись з нами своїми враженнями!
            </p>
        </div>
        <div class="right-side border-box">
            <?php
            if ($feedbackStatus = Yii::app()->user->getFlash('FeedbackStatus')) {
                if ($feedbackStatus == 'error')
                    $this->renderPartial('//site/popup', array('title' => 'Повідомлення', 'content' => 'Виникла помилка. Спробуйте пізніше.'));
                else
                    $this->renderPartial('//site/popup', array('title' => 'Повідомлення', 'content' => 'Повідомлення відправлене.'));
            }

            ?>

            <?php if (Yii::app()->user->hasFlash('contact')): ?>
                <div class="flash-success">
                    <div style="font-size: 14pt;"><?php echo Yii::app()->user->getFlash('contact'); ?></div>
                </div>
            <?php else: ?>
                <div class="form feedback">
                    <?php $form = $this->beginWidget('CActiveForm', array(
                        'id'                     => 'contact-form',
                        'enableClientValidation' => false,
                        'clientOptions'          => array(
                            'validateOnSubmit' => true,
                        ),
                    )); ?>
                    <?php
                    if ($form->errorSummary($model))
                        Yii::app()->user->setFlash('ShowPopup', array('title' => 'Помилка', 'content' => $form->errorSummary($model, '')));
                    ?>
                    <?php if (Yii::app()->user->isGuest) {
                        echo $form->textField($model, 'email', array('class' => 'email', 'placeholder' => 'E-mail*'));
                    }
                    ?>

                    <div class="row">
                        <?php echo $form->textArea($model, 'body', array('placeholder' => 'Повідомлення*', 'rows' => 6, 'cols' => 50)); ?>
                    </div>

                    <?php if (CCaptcha::checkRequirements()): ?>
                            <div class="row captcha">
                            <div style="position: relative;">
                                <?php
                                $this->widget('CCaptcha', array(
                                        'imageOptions'      => array('height' => '135', 'width' => '225', 'title'=>'Оновити код'),
                                        'clickableImage'    => true,
                                        'showRefreshButton' => true,
                                        'buttonType'        => 'link',
                                        'buttonLabel'       => 'Новий код',
                                        'buttonOptions'     => array('class' => 'refresh-lnk')
                                    )
                                );

                                ?>
                                <table class="right">
                                    <tr>
                                        <td>
                                            <div class="hint">
                                                введи символи, які ти бачиш <br>
                                                на зображенні ліворуч
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $form->textField($model, 'verifyCode', array('class' => 'verify-inp')); ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php echo CHtml::submitButton('Надіслати', array('class' => 'submit btn red-bg-color')); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php $this->endWidget(); ?>
                </div><!-- form -->
            <?php endif; ?>
        </div>
    </div>

    <!-- Accordion descriptions -->
    <div class="accordion-description footer-text">
        <p class="blue-color">
            Друзі вважають тебе непересічним авантюристом, колеги пліткують: «він ще той дивак», сусіди скаржаться на гучні
            вечірки, а кохана людина глибоко переконана, що ти – завзятий ентузіаст з великим майбутнім, лише матуся наївно
            вірить, що опівночі ти солодко спиш під ковдрою. Настав час дізнатись, який ти насправді.
        </p>
    </div>
    <!-- End of Accordion descriptions -->
</div>