<?
/**
 * Попап с победителями
 * @var $this
 */
?>
<style type="text/css">
    #winners-popup{
        width: 600px;
        padding-bottom: 30px;
    }
</style>

<div class="overlay">
    <div class="popup" id="winners-popup">
        <span class="close-popup-btn">&nbsp;</span>
        <h3>Вітаємо переможців розіграшу!</h3>
        <div class="content">
            <h3>Cеред учасників, які створили відео-привітання, ящик прохолодного пива BUD отримує:</h3>
            <ul>
                <li>Анжеліка Пержинська</li>
                <li>Дмитро Федоренко</li>
                <li>Міла Калюжна</li>
                <li>Олександр Микитюк</li>
            </ul>

            <h3>Серед учасників,які створили фото-привітання, ящик прохолодного пива BUD отримує:</h3>
            <table width="100%">
                <thead>
                    <tr>
                        <td width="30%"><strong>Київ</strong></td>
                        <td width="30%"><strong>Харків</strong></td>
                        <td width="30%"><strong>Донецьк</strong></td>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Олександр Лушніков</td>
                    <td>Артем Остапишен</td>
                    <td>Микола Хорошкевич</td>
                </tr>
                <tr>
                    <td>Євгеній Мельников</td>
                    <td>Ярослав Ковальов</td>
                    <td>Антон Беліченко</td>
                </tr>
                <tr>
                    <td>Сергій Приходько</td>
                    <td>Аліна Дяченко</td>
                    <td>Яна Сошникова</td>
                </tr>
                <tr>
                    <td>Ірина Тимошенко</td>
                    <td>Павло Шевчишин</td>
                    <td>Денис Фронтах</td>
                </tr>
                <tr>
                    <td>Олександр Сердюк </td>
                    <td>Ігор Новіков</td>
                    <td>Сергій Коньков</td>
                </tr>
                <tr>
                    <td>Дмитро Баб’як</td>
                    <td>Юрій Миронов</td>
                    <td>Станіслав Мазалов</td>
                </tr>
                <tr>
                    <td>Тамара Гаврисевич</td>
                    <td>Наталя Волчанська</td>
                    <td>Сергій Орлов </td>
                </tr>
                <tr>
                    <td>Дмитро Каналів</td>
                    <td>Павло Світський </td>
                    <td>Микита Куделін</td>
                </tr>
                <tr>
                    <td>Анна Кириленко</td>
                    <td>Олексій Островерхов</td>
                    <td>Андрій Рязанцев </td>
                </tr>
                <tr>
                    <td>Сергій Голуб’єв</td>
                    <td>Владислав Амінов</td>
                    <td>Віктор Паркен</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.close-popup-btn').on('click', function(){$(this).parents('.overlay').fadeOut(200)})
</script>
