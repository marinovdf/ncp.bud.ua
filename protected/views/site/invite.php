<?php
/**
 * This is template of invite page
 *
 * @var $this SiteController
 *
 */
$this->pageTitle = Yii::app()->name . ' | Запросити друга';
?>
<div id="invite-page" class="accordion-page">
    <!-- Accordion -->
    <ul class="accordion" id="invite-accordion">
        <li class="gold-bg-color invite-item-1" data-slide="1">
            <div class="accordion-content">
                <p class="blue-color white-color">Збільшуй <br>свої шанси</p>
                <p class="descr" style="max-width: 250px">
                    Зроби приємне собі
                    та друзям: виграйте разом
                    ящик освіжаючого BUD.
                </p>

                <img src="/images/box.png" alt="box" id="box"/>
            </div>

            <div class="arrow"></div>
        </li>

        <li class="grey-bg-color invite-item-2" data-slide="2">
            <div class="accordion-content">
                <p class="red-color">Не гай часу</p>

                <p class="blue-color descr">
                    Більше френдів - <br>
                    більше шансів
                </p>
            </div>
            <div class="arrow"></div>
        </li>

        <li class="red-bg-color invite-item-3" data-slide="3">
            <div class="accordion-content">
                <p class="blue-color">Поклич друзів</p>

                <a href="<?php echo Yii::app()->createUrl('#')?>" class="btn invite-fb-btn">Facebook</a>

            </div>
            <div class="arrow"></div>
        </li>

        <li class="blue-bg-color last invite-item-4" data-slide="4">
            <div class="accordion-content">
                <!-- Send mail form -->
                <?php $model = new EmailInviteForm(); ?>

                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'invite-mail-form',
                    'enableClientValidation' => false,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                )); ?>

                <fieldset>
                    <?php echo $form->textField($model,'email', array('placeholder'=>'E-MAIL ДРУГА')) ?>
                </fieldset>

                <fieldset>
                    <?php echo $form->textField($model,'friendName', array('placeholder'=>'ІМ’Я ДРУГА')) ?>
                </fieldset>

                <fieldset>
                    <?php echo $form->textArea($model,'body', array('placeholder'=>'ТЕКСТОВЕ ПОВІДОМЛЕННЯ', 'id'=>'message', 'cols'=>30, 'rows'=>10)) ?>
                </fieldset>

                <?php $this->endWidget(); ?>
                <!-- End of Send mail form -->
                <p class="white-color title">Повідом усіх</p>

                <a href="#" class="btn invite-mail-btn">Надіслати</a>
            </div>
            <div class="arrow"></div>
        </li>
    </ul>
    <!-- End of Accordion -->

    <!-- Accordion descriptions -->
    <div class="accordion-description">
        <table>
            <tr>
                <td>Все просто:</td>
                <td>якщо друг, котрого ти запросив, пройде BUD-тест, ти отримаєш освіжаючу можливість
                    виграти один із 20-ти ящиків BUD для дружньої компанії!</td>
            </tr>
        </table>

    </div>
    <!-- End of Accordion descriptions -->
</div>

<script type='text/javascript'>
    if (top.location!= self.location)
    {
        top.location = self.location
    }
</script>

<script>


   FB.init({
       appId  : '218825218323385',
       frictionlessRequests: true
   });
   <?php if(Yii::app()->request->cookies['dev_mode']) : ?>
   function postToFriend()
   {
       FB.ui({
           method : 'send',
           name : 'БУДЬ СОБОЮ',
           max_recipients:1,
           link : '<?php echo Yii::app()->getBaseUrl(true)?>',
           show_error : 'true',
           description : 'Нарешті я відкрив справжнього себе. Пройди Bud-тест, дізнайся свій темперамент та отримай характерний подарунок.'
       }, function (response) {
           //console.log(response);
           //alert(response);
       });
   }
   <?php else : ?>
   function postToFriend()
   {
       FB.ui({
           method : 'send',
           name : 'БУДЬ СОБОЮ',
           max_recipients:1,
           link : '<?php echo Yii::app()->getBaseUrl(true)?>',
           show_error : 'true',
           description : 'Нарешті я відкрив справжнього себе. Пройди Bud-тест, дізнайся свій темперамент та отримай характерний подарунок.'
       }, function (response) {
           //console.log(response);
           //alert(response);
       });
   }
   <?php endif; ?>

    $(document).ready(function(){


        $('.invite-fb-btn').on('click', function(e){
            e.preventDefault();

           // FacebookInviteFriends();
           // postToFriend();
        });

        $('.invite-mail-btn').on('click', function(e){
            e.preventDefault();

            return false;
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl('invite/emailinvite') ?>",
                data: $("#invite-mail-form").serialize(),
                dataType: "json"
            }).done(function (data) {
                console.log(data.status);

                if (data.status == 'success') {
                    popup('Повідомлення', 'Повідомлення успішно відправлене');
                    document.getElementById('invite-mail-form').reset();
                } else {

                    message = '';
                    $.each(data.message, function( index, value ) {
                        message = message + '<br>' + value;
                    });

                    popup('Помилка', message);
                }
            });
        });
    })
</script>