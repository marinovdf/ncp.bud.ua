<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id'                     => 'login-form',
    'enableClientValidation' => true,
    'clientOptions'          => array(
        'validateOnSubmit' => true,
    ),
)); ?>
<?php
// Отображение ошибок входа
if (Yii::app()->request->getPost('LoginForm')) {
    if ($form->errorSummary($model))
        Yii::app()->user->setFlash('ShowPopup', array('title' => 'Помилка', 'content' => $form->errorSummary($model, '')));
}
?>

<div class="auth-fields left">
    <div class="phone-wr">
        <?php echo $form->labelEx($model, 'phone'); ?>
        <?php $this->widget('CMaskedTextField', array(
            'model'       => $model,
            'attribute'   => 'phone',
            'mask'        => '+380(99)999-99-99',
            'placeholder' => '_',
            'htmlOptions' => array(
                'size'         => 17,
                'maxlength'    => 17,
                'autocomplete' => 'off'
            )
        ));
        ?>
    </div>

    <div class="pwd-wr">
        <?php echo $form->labelEx($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password', array('class' => 'pwd-field', 'autocomplete' => 'off')); ?>
        <!--<div class="show-pwd" data-show-pwd='show'></div>-->
    </div>
</div>
<?php echo CHtml::link('Вхід', '', array('class'=>'large-btn left', 'id'=>'auth-btn')) ?>
<?php Yii::app()->eauth->renderWidget(); ?>
<div class="reg-fields left">
    <?php echo CHtml::link('Нагадати пароль', Yii::app()->createUrl('site/remind'), array('class'=>'large-btn left', 'id'=>'remind-btn')) ?>
    <?php echo CHtml::link('Реєстрація', Yii::app()->createUrl('site/registration'), array('class' => 'large-btn', 'id'=>'registration-btn')) ?>
</div>

<?php $this->endWidget(); ?>