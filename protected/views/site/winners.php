<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name . ' | Подарунки';
?>
<style type="text/css">
    .mCSB_draggerContainer{display: none;}
    .scroll_to_top,
    .scroll_to_bottom{
        position: absolute;
        left: 640px;
        cursor: pointer;
        transition: .2s;
    }

    .scroll_to_bottom{
        bottom: 50px;
    }


    .scroll_to_bottom:hover,
    .scroll_to_top:hover{
        opacity: 0.5;
    }
    .gifts-text{
        width: 460px;
    }
    .gifts{
        top: 210px;
    }
</style>
<div id="gifts" class="white-border content vert-middle " style="height: 518px">
    <div class="white-bg page">
        <h1 class="align-center page-title fgd-sh">Вітаємо! <br> <span
                class="blue">Гайз, так тримати! Отримуйте свої замовлення:</span></h1>

        <div class="gifts"></div>
        <a class="scroll_to_top" oncontextmenu="return false;"><img src="/images/scroll-top.png" alt=""/></a>
        <div class="gifts-text fgd-sh right align-left" style="height: 250px; margin-top: 40px;">

            <h3 style="margin: 0;">Головне заохочення Акції – iPad 2</h3><div class="blue" style="font-size: 24px">Усенко Денис Петрович</div><h3 style="margin: 10px 0 10px 0;">Келих з логотипом BUD</h3><div class="blue" style="font-size: 24px; margin: 0; line-height: 24px">
                Відерський Даніїл Валентинович <br>
                Войтюк Олександр Віталійович<br>
                Гуцало Тетяна Сергіївна<br>
                Драган Сергій Миколайович<br>
                Єлишев Сергій<br>
                Єльченко Олександр Аркадійович<br>
                Захожий Віталій Петрович<br>
                Канивцова Марія Миколаївна<br>
                Карастойка Юлія Володимирівна<br>
                Кокшаров Максим Олександрович<br>
                Литвинчук Андрій Юрійович<br>
                Малюха Михайло Володимирович<br>
                Мухін Денис Євгенійович<br>
                Новоселецький Вадим Францович<br>
                Овсяннікова Жанна Сергіївна<br>
                Пономаренко Марина Миколаївна<br>
                Попов Ярослав Сергійович<br>
                Столярчук Андрій Юрійович<br>
                Федоров Дмитро Володимирович<br>
                Якшибаєв Віктор Петрович
            </div><h3 style="margin: 10px 0 10px 0;">Ящик пива BUD 0,33</h3><div class="blue" style="font-size: 24px; margin: 0; line-height: 24px">
                Антюшин Євгеній Олександрович <br>
                Бабій Ігор Іванович <br>
                Глива Юрій<br>
                Григор'єв Віктор Володимирович<br>
                Дулевич Петро<br>
                Колісник Олександр Вікторович<br>
                Кочегура Олег Петрович<br>
                Макарчук Владислав<br>
                Маковей Максим Олександрович<br>
                Мудра Аліна Артурівна<br>
                Новіков Євгеній Влодимирович<br>
                Панчошний Роман Ігорович<br>
                Плаксіна Юлія Олександрівна<br>
                Романчук Олена Олегівна<br>
                Стець Андрій Володимирович<br>
                Ткаченко Олександр Віталійович<br>
                Цехмейструк Андрій Миколайович<br>
                Чорненький Петро Андрійович<br>
                Шагинський Валерій Миколайович<br>
                Шевчук Олександр Олександрович

            </div>


        </div>

        <a class="scroll_to_bottom" oncontextmenu="return false;"><img src="/images/scroll-bottom.png" alt=""/></a>
    </div>

</div>
<script type="text/javascript">

    $(document).ready(function(){
        var scroll_height = 100;
        var totalScrollOffsetH = $(".totalScrollOffset").height();
        $(".gifts-text").mCustomScrollbar({
            scrollButtons: {
                enable: true
            },
            callbacks: {
                onTotalScroll: function () {
                    appendTextOnTotalScroll();
                },
                onTotalScrollOffset: totalScrollOffsetH
            }
        });
        $(".scroll_to_bottom").click(function (e) {
            e.preventDefault();

            if(scroll_height == 1100) return;

            if(scroll_height == -100)
                scroll_height +=200;
            else
                scroll_height +=100;
            $(".gifts-text").mCustomScrollbar("scrollTo", scroll_height);
        });
        $(".scroll_to_top").click(function (e) {
            if(scroll_height == 0) return;

            if(scroll_height < 0) return;

            scroll_height -=100;
            e.preventDefault();
            $(".gifts-text").mCustomScrollbar("scrollTo", scroll_height-100);
            scroll_height -=100;

        });
    })

</script>