<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' | Старт';
?>
<div id="start" class="white-border content fgd vert-middle">
    <div class="white-bg page">
        <h1 class="align-center pink fgd-sh">BUD-мене, працюємо за наступною схемою:</h1>

        <div class="steps">
            <div class="step1">
                Знайомишся з гостями в барі <br>
                та запам'ятовуєш їх імена.
            </div>

            <div class="step2">
                Приймаєш замовлення та <br>
                пригощаєш гостей пивом BUD.
            </div>

            <div class="step3">
                Граєш активно та уважно,<br>
                аби набрати максимум балів.
            </div>

            <div class="step4">
                Береш участь у розіграші iPad, <br>
                20-ти фірмових бокалів та <br>
                50-ти ящиків BUD від Короля Пива.
            </div>
        </div>

        <div class="go pink align-center">
            Все просто, поїхали!
            <div>
                <?php echo CHtml::link('Розпочни гру', Yii::app()->createUrl('game/index'), array('class' => 'btn start-game-btn')) ?>
            </div>
        </div>
    </div>
</div>