<?
/**
 * @var $title string sPopup title
 * @var $content string Popup content
 * @var $form CActiveForm
 * @var $model ValidateBirthdateForm
 */
?>
<style type="text/css">
    #validate-birthdate-form input[type=text]{
        border:solid 1px #c3002f;
        width: 110px;
        text-align: center;
        color:#c3002f;
    }

    #validate-birthdate-form label{
        font: normal 16px/16px ubuntu_condensedregular;
        color: #9a9a9a;

    }

    #submit-birthdate{
        border: none;
    }

    #birth-wr {
        width: 100%;
        text-align: center;
    }

    #birth-wr .table-cell{
        width: auto;
    }

    #birth-wr .table-cell:first-child,
    #birth-wr .table-cell.last{
        width: 110px;
    }

    #submit-birthdate{
        width: 50%;
        float: left;
    }

    .table{
        display: table;
    }

    .table-row{
        display: table-row;
    }

    .table-cell{
        display: table-cell;
    }

</style>
<div class="overlay">
    <div class="popup" id="access-denied">
        <div class="title"></div>
        <div class="content">
            <p>Зазначте, будь ласка, дату вашого народження:</p>
            <div>
                <?php

                $form = $this->beginWidget('CActiveForm', array(
                    'id'                     => 'validate-birthdate-form',
                    'enableClientValidation' => false,
                    'clientOptions'          => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                ?>

                <?php
                    echo $form->errorSummary($model);
                ?>

                <div class="table" id="birth-wr">
                    <div class="table-row">
                        <div class="table-cell">
                            <?php echo $form->label($model, 'day') ?>
                            <?php
                            $this->widget('CMaskedTextField', array(
                                'mask' => '99',
                                'placeholder' => '_',
                                'model' => $model,
                                'name' => 'day',
                                'attribute' => 'day'
                            ));
                            ?>
                        </div>
                        <div class="table-cell">
                            <?php echo $form->label($model, 'month') ?>
                            <?php
                            $this->widget('CMaskedTextField', array(
                                'mask' => '99',
                                'placeholder' => '_',
                                'model' => $model,
                                'name' => 'month',
                                'attribute' => 'month'
                            ));
                            ?>
                        </div>
                        <div class="table-cell last">
                            <?php echo $form->label($model, 'year') ?>
                            <?php
                            $this->widget('CMaskedTextField', array(
                                'mask' => '9999',
                                'placeholder' => '_',
                                'model' => $model,
                                'name' => 'year',
                                'attribute' => 'year'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>
        <input id="submit-birthdate" class="ok-btn btn red-bg-color" type="button" value="Ok"/>
    </div>
</div>
<script type="text/javascript">
    $('#submit-birthdate').live('click', function(){$('#validate-birthdate-form').submit()})
</script>
