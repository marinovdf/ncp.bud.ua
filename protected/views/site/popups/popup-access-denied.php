<?
/**
 * @var $title string sPopup title
 * @var $content string Popup content
 */
?>

<div class="overlay">
    <div class="popup" id="access-denied">
        <span class="close-popup-btn"></span>

        <div class="title"></div>
        <div class="content" style="padding-top: 20px;">
            Отакої, доступ заборонений.<br>
            Але це легко виправити.<br>
            Тобі необхідно увійти на сайт<br>
            або пройти реєстрацію.
        </div>
        <div class="footer">
            <?php echo CHtml::link('Вхід', Yii::app()->createUrl('#'), array('class' => 'btn red-bg-color', 'id' => 'login-popup-btn')) ?>
            <?php echo CHtml::link('Реєстрація', Yii::app()->createUrl('site/registration'), array('class' => 'btn red-bg-color')) ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.ok-btn, .close-popup-btn').on('click', function () {
        $(this).parents('.overlay').hide()
    });
    $(document).ready(function () {
        $('#login-popup-btn').on('click', function (e) {
            e.preventDefault();

            $(this).parents('.overlay').hide();
            $('#login-btn').trigger('click');

        })
    });
</script>
