<?php
/**
 * @var $this  SiteController
 * @var $form  CActiveForm
 * @var $model FeedbackForm
 */
?>
<!--<script type="text/javascript">
    ga('send', 'event', 'BUD', 'GameAuth', 'Прерывание игры для авторизации');
</script>-->
<div id="auth" class="white-border vert-middle">
    <div class="white-bg">
        <h1 class="align-center blue fgd">Чудово! Вітаємо! </h1>
        <div class="pink fgd" style="font-size: 20px; line-height: 22px">
            Щоб перейти на наступний рівень гри потрібно ввести свій логін та пароль на сайт або зареєструватися.
        </div>
        <div class="align-center fgd">
            <?php echo CHtml::link('Вхід', Yii::app()->createUrl('site/register'), array('class' => 'btn auth-login-btn start-game-btn')) ?>
            &nbsp;
            &nbsp;
            &nbsp;
            <?php echo CHtml::link('Реєстрація', Yii::app()->createUrl('site/registration'), array('class' => 'btn auth-reg-btn start-game-btn')) ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.auth-login-btn').on('click', function(e){
            e.preventDefault();

            $('.login-block').slideToggle(200)
        })
    })
</script>