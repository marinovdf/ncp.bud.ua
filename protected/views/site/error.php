<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' | Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<div id="profile" class="white-border">
    <div class="white-bg page">
        <h1 class="align-center page-title">Помилка</h1>
        <h2>Error <?php echo $code; ?></h2>
        <div class="error">
            <?php echo CHtml::encode($message); ?>
        </div>
    </div>
</div>
