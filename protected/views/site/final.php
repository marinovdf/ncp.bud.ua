<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name . ' | Фінал';
?>
<script type="text/javascript">
    ga('send', 'event', 'BUD', 'Final', 'Финальный экран после прохождения 3 уровня игры');
</script>
<div id="final" class="white-border content fgd-sh">
    <div class="white-bg page">
        <h1 class="align-center page-title">Вітаємо, BUD-мене! <br>
            <span
                class="blue">Твій результат –- <?php echo Yii::t('app', '{n} бал|{n} бали|{n} балів|{n} бали', $points); ?>
                .</span>
        </h1>

        <div class="bottles"></div>
        <div class="final-text right">
            Король Пива приготував <br>
            для тебе <br>
            ящик BUD, пивні келихи та <br>
            суперприз – iPad. <br><br>

            <span class="blue">Чекай на розіграш 30 вересня</span>

            <?php echo CHtml::link('Грати ще', Yii::app()->createUrl('game/index'), array('class' => 'btn start-game-btn')) ?>
        </div>
    </div>
</div>