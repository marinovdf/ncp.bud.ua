<?php
?>

<style type="text/css">
    #flash-wr{
        padding-top: 128px;
        width: 900px;
        height: 500px;
        margin: 0 auto;
    }
</style>
<script type="text/javascript">
    var flashvars = {
        dataUrl: "<?php echo Yii::app()->createUrl('/passport/api/') ?>"
    };
    swfobject.embedSWF("<?php echo Yii::app()->baseUrl . '/flash/Passport2Rio.swf?v=14033101' ?>", "flash", '900', '500', "9.0.0", false , flashvars);
</script>
<div id="passport-page" class="border-box">
    <div id="flash-wr">
        <div id="flash"></div>
    </div>
</div>

<script type='text/javascript'>
    if (top.location!= self.location)
    {
        top.location = self.location
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '<?php echo Yii::app()->eauth->services['facebook']['client_id'] ?>',
            status     : true,
            cookie     : true,
            xfbml      : true,
            frictionlessRequests: true
        });

        $(document).trigger('fbload');  //  <---- THIS RIGHT HERE TRIGGERS A CUSTOM EVENT CALLED 'fbload'
    };

    function sharePassport(){
        FB.ui(
            {
                method: 'feed',
                name: 'BUD',
                link: '<?php echo Yii::app()->request->hostInfo ?>',
                picture: '<?php echo Yii::app()->request->hostInfo ?>/images/FBS_PASSP.jpg',
                caption: 'BUD Passport',
                description: 'Я став беззаперечним кандидатом на шалену подорож до Ріо-де-Жанейро і маю грандіозні шанси вирушити на Чемпіонат Світу з футболу!'
            },
            function(response) {
                if (response && response.post_id) {
                    console.log(response);

                    $.ajax({
                        url:'<?php echo Yii::app()->createUrl('passport/shareLog') ?>',
                        data:{post_id:response.post_id},
                        type:'post'
                    }).done(function(data){
                        console.log(data);
                    });
                } else {
                    console.log('Post was not published');

                    $.ajax({
                        url:'<?php echo Yii::app()->createUrl('passport/shareLog') ?>',
                        type:'post'
                    }).done(function(data){
                        console.log(data);
                    });
                }
            }
        );
    }
</script>