<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title></title>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/bootstrap.min.css') ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/layers.css') ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/controls.css') ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/content-pages.css') ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/normalize.min.css') ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/js/select2-3.3.1/select2.css') ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jNice.css') ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/fonts/fonts.css') ?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/main.css') ?>

    <!--[if lt IE 9]>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/html5.min.js'); ?>
    <![endif]-->
    <?php Yii::app()->getClientScript()->registerCoreScript('jquery'); ?>
    <?php Yii::app()->getClientScript()->registerCoreScript('jquery.ui'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.placeholder.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/uaDatepicker.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/select2-3.3.1/select2.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/select2-3.3.1/select2_locale_uk.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.jNice.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/main.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/swfobject.js'); ?>

    <!-- Google analytics code -->
    <?php $this->renderPartial('//site/html/ga'); ?>
    <!-- End of Google analytics code -->
    <script type="text/javascript">
        function popup(title, content){
            $( "#ajax-popup" ).load( "<?php echo Yii::app()->createUrl('/site/popup')?>",function() {
                $(this).find('.title').html(title);
                $(this).find('.content').html(content);
            });
        }
    </script>
</head>
<body>
<div id="background-holder" class="layer "></div>
<div id="content-layer" class="layer ">
    <div class="main-header">
        <div class="h-block">
            <div>

            </div>
        </div>
    </div>
    <div class="main-menu">
        <div class="h-block">
            <div class="content">
                <div>
                    <span class="to-jump" data-page="prizes">ЗАОХОЧЕННЯ</span>
                    <span class="to-jump" data-page="mech">МЕХАНІКА</span>
                    <span class="to-jump" data-page="chemp">КРАЇНИ-ЧЕМПІОНИ</span>
                    <span class="to-jump" data-page="stadium">Check in НА СТАДІОНІ</span>
                    <span class="to-jump" data-page="tornament">ТУРНІРНА ТАБЛИЦЯ</span>
                    <span class="to-jump" data-page="winners">ПЕРЕМОЖЦІ</span>
                    <form id="to-jump-form" action="http://football.bud.ua" method="post">
                        <input type="hidden" name="current_page"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="main-content"><div id="page-mech" class="h-block" style="opacity: 1;">
            <div class="bg-left-part"></div>
            <div class="bg-right-part"></div>
            <div class="page-holder content">
                <div>
                    <?php echo $content; ?>
                </div>
            </div>
        </div></div>
    <div class="main-footer">
        <div class="h-block">
            <div class="content">
                <span style="float:left;margin-left:40px;">© BUD, SUN InBev Ukraine, 2014</span>
                <span style="margin-left:70px;">СПОЖИВАЙТЕ ВІДПОВІДАЛЬНО</span>
                <span style="float:right;margin-right:40px;">Телефон гарячої лінії: 0 800 50 8886</span>
            </div>
        </div>
    </div>
    <script>
    </script>
</div>
<div id="front-layer" class="layer ">
    <div class="main-header">
        <div class="h-block">
            <div class="content">
                <div class="logo dev to-jump" data-page="landing"></div>
                <div id="cabinet" class="panel logined closed" style="display: none">
                    <div class="content">
                        <div class="title">
                            ГАРАНТОВАНІ ЗАОХОЧЕННЯ
                        </div>
                        <div class="h-line"></div>
                        <div class="logined-form">
                            <div class="h-block order-btn panel2">
                                Замовити заохочення
                            </div>
                            <button class="gold-panel">НАБРАНО 1000 БАЛІВ</button>
                            <div class="title">Країни</div>
                            <div class="countries">
                                <div class="cap  ">
                                    <div>br</div>
                                </div>
                                <div class="cap shadowed">
                                    <div>it</div>
                                </div>
                                <div class="cap shadowed">
                                    <div>de</div>
                                </div>
                                <br>
                                <div class="cap shadowed">
                                    <div>ar</div>
                                </div>
                                <div class="cap shadowed">
                                    <div>uy</div>
                                </div>
                                <br>
                                <div class="cap shadowed">
                                    <div>fr</div>
                                </div>
                                <div class="cap shadowed">
                                    <div>es</div>
                                </div>
                                <div class="cap shadowed">
                                    <div>en</div>
                                </div>
                            </div>
                            <button class="passport-btn inactive">ЗАПОВНИТИ ПАСПОРТ</button>
                        </div>
                    </div>
                    <div class="h-line"></div>
                    <div class="grip" onclick="">
                        <div>ОСОБИСТИЙ КАБІНЕТ</div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="dialog panel">
        <div class="message">
        </div>
        <button>ok</button>
    </div>
</div>
<?php
if ($popup = Yii::app()->user->getFlash('ShowPopup'))
    $this->renderPartial('//site/popup', $popup);

if (Yii::app()->user->getFlash('AccessDeniedPopup'))
    $this->renderPartial('//site/popups/popup-access-denied');
?>
<div id="fb-root"></div>
<script type="text/javascript">
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript">

    $(document).ready(function () {
        $("select").select2();

        /*if (!$.placeholder.input || !$.placeholder.textarea) {
            $('#hint').hide();
        }*/
    });
</script>
<div id="ajax-popup"></div>
</body></html>