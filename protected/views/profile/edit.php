<?php
/* @var $this SiteController */
/* @var $model RegistrationForm */
/* @var $form CActiveForm */
$this->pageTitle = Yii::app()->name . ' | Реєстрація';
?>

<style type="text/css">

</style>

<div id="registration-page" class="column2">
<div class="table-row" style="height: 100%;">
    <div class="left-side red-bg-color border-box" style="padding-right: 25px;">
        <div class="vertical-arrow"></div>
        <img src="/images/registration-icon.png" alt="" class="left" style="margin-right: 10px;"/>

        <h1 style="overflow: hidden; padding-top: 25px">Редагування власних данних</h1>

        <p class="subtitle">
        </p>
    </div>
    <div class="right-side border-box">
        <div class="registration-wr">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'                   => 'register-form',
                'enableAjaxValidation' => false,
            )); ?>
            <div class="form-wr">
                <!-- Row #1 -->
                <div class="row">
                    <div class="cell">
                        <?php echo $form->textField($model, 'LastName', array('placeholder' => 'Прізвище*')) ?>
                    </div>
                    <div class="cell">
                        <?php echo $form->textField($model, 'FirstName', array('placeholder' => "Ім’я*")); ?>
                    </div>
                    <div class="cell">
                        <?php echo $form->textField($model, 'Patronymic', array('placeholder' => "По-батькові")); ?>
                    </div>

                    <div class="cell">
                        <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'       => $model,
                            'attribute'   => 'Birthday',
                            'language'    => 'ua',
                            'themeUrl'    => Yii::app()->baseUrl . '/css/jui',
                            'theme'       => 'custom-theme',
                            'cssFile'     => 'jquery-ui-1.9.2.custom.min.css',
                            'options'     => array(
                                'dateFormat'        => 'yy.mm.dd', // format
                                'showOtherMonths'   => true, // show dates in other months
                                'selectOtherMonths' => true, // can select dates in other months
                                'changeYear'        => true, // can change year
                                'changeMonth'       => true, // can change month
                                'yearRange'         => '1930:1995', // range of year
                                'minDate'           => '1930.01.01', // minimum date
                                'maxDate'           => date((date('Y') - 18) . '.m.d'), // maximum date
                            ),
                            'htmlOptions' => array(
                                'maxlength'   => '10', // textField maxlength
                                'class'       => 'input-calendar',
                                'placeholder' => 'Дата народження*'
                            ),

                        ));
                        ?>
                    </div>

                    <div class="cell">
                        <?php
                        echo $form->dropDownList($model, 'Oblast', CHtml::listData($model->getOblastList(), 'OblastName', 'OblastName'),
                            array(
                                'options' => array(
                                    $model->Oblast => array('selected' => true)
                                ),
                                'empty'   => 'Область*',
                                'ajax'    => array(
                                    'type'   => 'POST', //request type
                                    'url'    => Yii::app()->createUrl('site/getRayonList'), //url to call.
                                    'update' => '.rayon-name', //selector to update
                                    'data'   => 'js:{OblastName:this.value}'
                                )));
                        ?>
                    </div>
                    <div class="cell">
                        <?php
                        echo $form->dropDownList($model, 'Rayon', CHtml::listData(
                                $model->getRayonList(), 'RayonName', 'RayonName'),
                            array(
                                'empty' => 'Район*',
                                'class' => 'rayon-name',
                                'ajax'  => array(
                                    'type'   => 'POST', //request type
                                    'url'    => Yii::app()->createUrl('site/getCityList'), //url to call.
                                    'update' => '.city-name', //selector to update
                                    'data'   => 'js:{RayonName:this.value}'
                                )
                            ));
                        ?>
                    </div>
                    <div class="cell">
                        <?php echo $form->dropDownList($model, 'CityName', CHtml::listData($model->getCityList(), 'CityName', 'CityName'), array('empty' => 'Місто / Село*', 'class' => 'city-name')); ?>

                    </div>
                    <div class="cell gender-cell">
                        <label for="gender">Стать*:</label>
                        <?php
                        echo $form->radioButtonList($model, 'Gender', $model->getGenderList(), array(
                            'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;',
                            'template'  => '{input}{label}.',
                            'class'     => 'gender'
                        ));
                        ?>
                    </div>

                    <div class="cell">
                        <?php echo $form->dropDownList($model, 'operatorCode', $model->getOperatorCodeList(), array('empty' => 'Код*', 'class' => 'mob-code-sel')); ?>
                        <?php $this->widget('CMaskedTextField', array(
                            'model'       => $model,
                            'attribute'   => 'MobilePhone',
                            'mask'        => '9999999',
                            'placeholder' => '_',
                            'htmlOptions' => array(
                                'size'         => 17,
                                'maxlength'    => 17,
                                'autocomplete' => 'off',
                                'placeholder'  => 'Телефон*',
                                'class'        => 'mob-phone-inp'
                            )
                        ));
                        ?>
                    </div>
                    <div class="cell" style="width: auto">
                        <?php echo $form->textField($model, 'Email', array('placeholder' => 'E-mail*', 'style' => 'width:460px;')) ?>
                    </div>

                    <div class="cell">
                        <input type="submit" value="Зберегти зміни"/>
                    </div>

                </div>
                <!-- End of Row #1 -->
            </div>
            <?php
            ####################    Валидация телефона   ####################
            if ($form->error($model, 'phoneVerified')) {
                $validationModel              = new ValidatePhone();
                $validationModel->PhoneNumber = $model->operatorCode . $model->MobilePhone;

                if (isset($_POST["ValidatePhone"])) {
                    $validationModel->ValidateCode = $_POST["ValidatePhone"]['ValidateCode'];
                    $validationModel->userCode     = $_POST["ValidatePhone"]['userCode'];

                    if ($validationModel->validate()) {
                        $validationModel->saveSuccess();
                        $model->phoneVerified = true;

                        // флага валидации телефона
                        $model->PhoneValidStatusCode = 'VALID';

                        //сохраняем основную форму
                        if ($model->validate()) {
                            if ($model->save()) {
                                Yii::app()->user->setFlash('ShowPopup', array('title' => 'Повідомлення', 'content' => 'Профайл збережений'));

                                // Переписываем данные пользователя в сессии
                                if ($user = (object)$model->getConsumer(array('EmailAddress' => Yii::app()->user->email)))
                                    Yii::app()->user->data = $user;

                                $this->redirect(Yii::app()->createUrl('profile'));
                                Yii::app()->end();
                            } else
                                Yii::app()->user->setFlash('ShowPopup', array('title' => 'Помилка', 'content' => 'Увага! Виникла помилка. Профайл не збережений'));
                        }

                    }
                    $this->renderPartial('//site/validatePhone', array('model' => $validationModel));
                } else {
                    //отправляем смс
                    $validationModel->ValidateCode = $validationModel->genCode();
                    $validationModel->sendSms($validationModel->PhoneNumber, $validationModel->ValidateCode);

                    //рендерим форму валидации телефона
                    $this->renderPartial('//site/validatePhone', array('model' => $validationModel));
                }

            } else {
                if ($form->errorSummary($model))
                    $this->renderPartial('//site/popup', array('title' => 'Помилка', 'content' => $form->errorSummary($model, '')));
            }

            ?>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<!-- Accordion descriptions -->
<div class="accordion-description footer-text">
    <p class="blue-color">
        Друзі вважають тебе непересічним авантюристом, колеги пліткують: «він ще той дивак», сусіди скаржаться на гучні
        вечірки, а кохана людина глибоко переконана, що ти – завзятий ентузіаст з великим майбутнім, лише матуся наївно
        вірить, що опівночі ти солодко спиш під ковдрою. Настав час дізнатись, який ти насправді.
    </p>
</div>
<!-- End of Accordion descriptions -->
</div>