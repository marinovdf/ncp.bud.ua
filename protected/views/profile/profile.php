<?php
/* @var $this SiteController
 * @var $model Profile;
 * */
$this->pageTitle = Yii::app()->name . '  | Профайл';
?>

<div id="profile-page" class="column2">
    <div class="table-row">
        <div class="left-side red-bg-color border-box">
            <div class="vertical-arrow"></div>
            <img src="/images/profile-icon.png" alt="" class="left" style="margin-right: 10px;"/>

            <h1 style="overflow: hidden; padding-top: 25px">Профайл</h1>

            <p class="subtitle" style="padding-left: 73px">
                Знайшов помилку
                у профайлі?
                Не зволікай - <br>
                відредагуй.
            </p>
        </div>
        <div class="right-side border-box">
            <p>
                <?php echo $model->fullName ?> <br>
                <?php echo Yii::t('app', '{n} рік|{n} року|{n} років', $model->age); ?> (<?php echo $model->birthDate ?>)
            </p>

            <p>
                <?php echo $model->phone ?><br>
                <?php echo $model->email ?>
            </p>

            <p>
                <?php echo $model->cityName ?><br>
                <?php echo $model->rayonName ?> р-н, <?php echo $model->oblastName ?> обл.
            </p>

            <div><?php echo CHtml::link('Редагувати', Yii::app()->createUrl('/profile/edit'), array('class' => 'red-btn edit-profile-btn')) ?></div>

        </div>
    </div>

    <!-- Accordion descriptions -->
    <div class="accordion-description footer-text">
        <p class="blue-color">
            Друзі вважають тебе непересічним авантюристом, колеги пліткують: «він ще той дивак», сусіди скаржаться на гучні вечірки, а кохана людина глибоко переконана, що ти – завзятий ентузіаст з великим майбутнім, лише матуся наївно вірить, що опівночі ти солодко спиш під ковдрою. Настав час дізнатись, який ти насправді.
        </p>
    </div>
    <!-- End of Accordion descriptions -->
</div>