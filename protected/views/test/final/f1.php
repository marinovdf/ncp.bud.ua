<?php
/**
 * This is template of final test
 *
 * @var $this SiteController
 *
 */
$this->pageTitle = Yii::app()->name . ' | Результат тесту';
?>
<style type="text/css">
    @media (max-height: 900px) {
        .container{
            overflow-y: visible;
        }
        footer{
            position: relative;
            bottom: -120px;
        }
    }
</style>
<div id="final-page">
    <div style="position: relative; height: 100%;">
        <!-- Left side -->
        <div class="final-descr">
            <div class="vertical-arrow"></div>
            <p class="title">Вітаємо</p>
            <p class="subtitle">Ти успішно пройшов BUD-тест і береш участь у розіграші характерного подарунку.</p>
            <h1>Ловець емоцій</h1>

            <p class="final-gift-type">
                Твій тип подарунку: <br>
                <a href="<?php echo Yii::app()->createUrl('site/gifts')?>">Наважся на більше</a>
            </p>

            <p class="final-description">
                Темп-і-ритм твоєму життю задає шалений темперамент та адреналіновий драйв. Ти справжній експериментатор,
                здатний до фантастичних пригод та неймовірних перетворень. Одного твого потужного заряду вистачить, аби
                прокачати цілий стадіон. Ловити шалену хвилю креативу – це твоя щоденна справа. Ти знаєш вже сьогодні, що
                буде популярним завтра. Ризикуй, драйвуй, експериментуй – твоє запальне серце вже не спинити.
            </p>

            <div class="share-btns">
                <a href="#" class="btn invite-fb-btn">поділитись</a>
                <!--<a href="http://vkontakte.ru/share.php?url=<?php /*echo Yii::app()->getBaseUrl(true)*/?>" target="_blank" class="btn invite-vk-btn">поділитись</a>-->
                <a href="#" target="_blank" class="btn invite-vk-btn">поділитись</a>
            </div>
        </div>
        <!-- End of Left side -->

        <div class="final-img">
            <img src="/images/final/final_12.jpg" alt=""/>
        </div>
    </div>
    <div style="clear: both; height: 0">&nbsp;</div>
    <!-- Accordion descriptions -->
    <div class="accordion-description">
        <table id="final-footer">
            <tr>
                <td class="blue-color">
                    <div class="subtitle cinema">
                        Гранд-готель Будапешт (2014)<br>
                        Вона (2014)<br>
                        Всередині Льюїса Девіса (2013)<br>
                    </div>
                </td>
                <td class="dark-grey-color">
                    <div class="subtitle music">
                        Kaiser Chiefs “Education, Education, Education&War”<br>
                        Tricky “False Idols”<br>
                        Two door cinema club “Changing of the Seasons”<br>
                    </div>
                </td>
                <td class="red-color">
                    <div class="subtitle must-have">
                        Спробувати себе у флайбордингу<br/>
                        Вирушити з друзями у вело-тріп<br/>
                        Відвідати Фінал Чемпіонату Світу з футболу<br/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!-- End of Accordion descriptions -->
</div>

<script type="text/javascript">
    FB.init({
        appId  : '218825218323385',
        frictionlessRequests: true
    });

    function postToWall() {
        FB.ui({
            method: 'feed',
            picture: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>',
            link: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>',
            name: 'БУДЬ СОБОЮ',
            description: 'Нарешті я відкрив справжнього себе. Пройди Bud-тест, дізнайся свій темперамент та отримай характерний подарунок.',
            redirect_uri: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>'
        }, function (response) {

        });
    }

    $(document).ready(function(){
        $('.invite-fb-btn').on('click', function(e){
            e.preventDefault();

            //postToWall();
        });
    });

</script>