<?php
/**
 * This is template of final test
 *
 * @var $this SiteController
 *
 */
$this->pageTitle = Yii::app()->name . ' | Результат тесту';
?>
<style type="text/css">
    @media (max-height: 900px) {
        .container{
            overflow-y: visible;
        }
        footer{
            position: relative;
            bottom: -120px;
        }
    }
</style><div id="final-page">
    <div style="position: relative; height: 100%;">
    <!-- Left side -->
    <div class="final-descr">
        <div class="vertical-arrow"></div>
        <p class="title">Вітаємо</p>

        <p class="subtitle">Ти успішно пройшов BUD-тест і береш участь у розіграші характерного подарунку.</p>

        <h1>Завзятий ентузіаст</h1>

        <p class="final-gift-type">
            Твій тип подарунку: <br>
            <a href="<?php echo Yii::app()->createUrl('site/gifts')?>">Заряд емоцій</a>
        </p>

        <p class="final-description">
            Не можна так просто взяти і стати найкращим представником нашого часу - епохи соціальних мереж, електронної музики, невибагливого арту й невимушених серіалів, але – мої вітання! – тобі це вдалося. Всупереч нездоланній спокусі перейняти чужий образ, ти залишайся собою – сміливим, прогресивним,  безкомпромісним.Ти – завзятий ентузіаст з великим майбутнім. Ніколи не зраджуй собі – нехай твоя неповторність підкорить світ.
        </p>

        <div class="share-btns">
            <a href="#" class="btn invite-fb-btn">поділитись</a>
            <!--<a href="http://vkontakte.ru/share.php?url=<?php /*echo Yii::app()->getBaseUrl(true) */?>" target="_blank"
               class="btn invite-vk-btn">поділитись</a>-->
        </div>
    </div>
    <!-- End of Left side -->

    <div class="final-img">
        <img src="/images/final/final_42.jpg" alt=""/>
    </div>
        </div>
    <div style="clear: both; height: 0">&nbsp;</div>
    <!-- Accordion descriptions -->
    <div class="accordion-description">
        <table id="final-footer">
            <tr>
                <td class="blue-color">
                    <div class="subtitle cinema">
                        Мад (2013)<br>
                        Потяг на Дарджилінг (2007)<br>
                        Великий Лебовські (1998)<br>
                    </div>
                </td>
                <td class="dark-grey-color">
                    <div class="subtitle music">
                        Red Hot Chili Peppers “I’m with You”<br>
                        David Bowie “The Next Day”<br>
                        Lorde “Pure Heroine”<br>
                    </div>
                </td>
                <td class="red-color">
                    <div class="subtitle must-have">
                        Влаштувати дружній турнір з гри у монополію<br>
                        Реалізувати ідею стартапу<br>
                        Додати буккросинг у список своїх зацікавлень<br>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!-- End of Accordion descriptions -->
</div>

<script type="text/javascript">
    FB.init({
        appId: '218825218323385',
        frictionlessRequests: true
    });

    function postToWall() {
        FB.ui({
            method: 'feed',
            picture: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>',
            link: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>',
            name: 'БУДЬ СОБОЮ',
            description: 'Нарешті я відкрив справжнього себе. Пройди Bud-тест, дізнайся свій темперамент та отримай характерний подарунок.',
            redirect_uri: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>'
        }, function (response) {

        });
    }

    $(document).ready(function () {
        $('.invite-fb-btn').on('click', function (e) {
            e.preventDefault();

            //postToWall();
        });
    });

</script>