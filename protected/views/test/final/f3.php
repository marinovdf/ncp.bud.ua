<?php
/**
 * This is template of final test
 *
 * @var $this SiteController
 *
 */
$this->pageTitle = Yii::app()->name . ' | Результат тесту';
?>
<style type="text/css">
    @media (max-height: 900px) {
        .container{
            overflow-y: visible;
        }
        footer{
            position: relative;
            bottom: -120px;
        }
    }
</style>
<div id="final-page">

    <div style="position: relative; height: 100%;">
    <!-- Left side -->
    <div class="final-descr">
        <div class="vertical-arrow"></div>
        <p class="title">Вітаємо</p>

        <p class="subtitle">Ти успішно пройшов BUD-тест і береш участь у розіграші характерного подарунку.</p>

        <h1>Душа компанії</h1>

        <p class="final-gift-type">
            Твій тип подарунку: <br>
            <a href="<?php echo Yii::app()->createUrl('site/gifts')?>">На пульсі розваг</a>
        </p>

        <p class="final-description">
            Рейтинг популярності зашкалює. Ти бажаний гість на будь-якій вечірці, й без твоєї присутності свято приречене на невдачу. Тобі не звикати бути у центрі уваги – твоя харизма не може залишитись непомітною. Дотепні жарти, гострі метафори та запальний характер – це те, що приваблює друзів, френдів та випадкових незнайомців. Ти здатен знайти спільну мову будь з ким: від натхненного хіпі до наполегливого хіпстера. І друзям це однозначно до душі.</p>

        <div class="share-btns">
            <a href="#" class="btn invite-fb-btn">поділитись</a>
            <!--<a href="http://vkontakte.ru/share.php?url=<?php /*echo Yii::app()->getBaseUrl(true) */?>" target="_blank"
               class="btn invite-vk-btn">поділитись</a>-->
        </div>
    </div>
    <!-- End of Left side -->

    <div class="final-img">
        <img src="/images/final/final_32.jpg" alt=""/>
    </div>
    </div>
    <div style="clear: both; height: 0">&nbsp;</div>
    <!-- Accordion descriptions -->
    <div class="accordion-description">
        <table id="final-footer">
            <tr>
                <td class="blue-color">
                    <div class="subtitle cinema">
                        Недоторкані (2011)<br>
                        Літо. Однокласники. Любов (2012)<br>
                        Це кінець (2013)<br>
                    </div>
                </td>
                <td class="dark-grey-color">
                    <div class="subtitle music">
                        Onerepublic<br>
                        Rihanna<br>
                        Bruno Mars
                    </div>
                </td>
                <td class="red-color">
                    <div class="subtitle must-have">
                        Влаштувати зустріч однокласників<br/>
                        Вирушити з друзями у Євротур<br/>
                        Організувати Karaoke Party<br/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!-- End of Accordion descriptions -->
</div>

<script type="text/javascript">
    FB.init({
        appId: '218825218323385',
        frictionlessRequests: true
    });

    function postToWall() {
        FB.ui({
            method: 'feed',
            picture: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>',
            link: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>',
            name: 'БУДЬ СОБОЮ',
            description: 'Нарешті я відкрив справжнього себе. Пройди Bud-тест, дізнайся свій темперамент та отримай характерний подарунок.',
            redirect_uri: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>'
        }, function (response) {

        });
    }

    $(document).ready(function () {
        $('.invite-fb-btn').on('click', function (e) {
            e.preventDefault();

           // postToWall();
        });
    });

</script>