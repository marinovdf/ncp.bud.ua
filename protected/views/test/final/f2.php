<?php
/**
 * This is template of final test
 *
 * @var $this SiteController
 *
 */
$this->pageTitle = Yii::app()->name . ' | Результат тесту';
?>
<style type="text/css">
    @media (max-height: 900px) {
        .container{
            overflow-y: visible;
        }
        footer{
            position: relative;
            bottom: -120px;
        }
    }
</style>
<div id="final-page">

    <div style="position: relative; height: 100%;">
        <!-- Left side -->
        <div class="final-descr">
            <div class="vertical-arrow"></div>
            <p class="title">Вітаємо</p>

            <p class="subtitle">Ти успішно пройшов BUD-тест і береш участь у розіграші характерного подарунку.</p>

            <h1>Невиправний <br>
                тусовщик</h1>

            <p class="final-gift-type">
                Твій тип подарунку: <br>
                <a href="<?php echo Yii::app()->createUrl('site/gifts')?>">У ритмі серця</a>
            </p>

            <p class="final-description">
                Тримати руку на пульсі розваг – це твоє покликання. Ти завжди в курсі, коли відбудеться найкрутіша вечірка
                року, де грають європейські хедлайнери і як потрапити на концерт легендарного рок-бенду. Нічне життя – це
                твоя стихія. Після заходу сонця завжди тягне на пригоди – і ти не пропустиш жодної нагоди весело
                розважитись. Коли місто засинає, прокидається шалена тусовка й запалює аж до самого ранку.
            </p>

            <div class="share-btns">
                <a href="#" class="btn invite-fb-btn">поділитись</a>
                <!--<a href="http://vkontakte.ru/share.php?url=<?php /*echo Yii::app()->getBaseUrl(true) */?>" target="_blank"
                   class="btn invite-vk-btn">поділитись</a>-->

                <a href="#" target="_blank" class="btn invite-vk-btn">поділитись</a>
            </div>
        </div>
        <!-- End of Left side -->

        <div class="final-img">
            <img src="/images/final/final_22.jpg" alt=""/>
        </div>
    </div>
    <div style="clear: both; height: 0">&nbsp;</div>
    <!-- Accordion descriptions -->
    <div class="accordion-description">
        <table id="final-footer">
            <tr>
                <td class="blue-color">
                    <div class="subtitle cinema">
                        <!--<div class="title">
                            КІНО<br/>
                            DIGEST
                        </div>-->

                        Похмілля у Вегасі (2009)<br>
                        Великий Гетсбі (2013)<br>
                        Студія 54 (1998)<br>
                    </div>
                </td>
                <td class="dark-grey-color">
                    <div class="subtitle music">
                        Armin van Buuren “Intense: The More Intense Edition”<br>
                        Avici “True”<br>
                        Steve Aoki “Wonderland”<br>
                    </div>
                </td>
                <td class="red-color">
                    <div class="subtitle must-have">
                        Вирушити фестивалити на Kazantip<br/>
                        Зафрендити музичного хедлайнера<br/>
                        Влаштувати вечірку на даху мегаполіса<br/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <!-- End of Accordion descriptions -->
</div>



<script type="text/javascript">
    FB.init({
        appId: '218825218323385',
        frictionlessRequests: true
    });

    function postToWall() {
        FB.ui({
            method: 'feed',
            picture: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>',
            link: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>',
            name: 'БУДЬ СОБОЮ',
            description: 'Нарешті я відкрив справжнього себе. Пройди Bud-тест, дізнайся свій темперамент та отримай характерний подарунок.',
            redirect_uri: '<?php echo Yii::app()->createAbsoluteUrl('site/index') ?>'
        }, function (response) {

        });
    }

    $(document).ready(function () {
        $('.invite-fb-btn').on('click', function (e) {
            e.preventDefault();

            //postToWall();
        });
    });

</script>