<?php
/**
 * This is template of question #1
 *
 * @var $this SiteController
 *
 */
$this->pageTitle = Yii::app()->name . ' | Головна';
?>

<div class="test">
    <!-- Accordion -->
    <ul class="accordion" id="test-accordion" data-question="<?php echo $questionId ?>">
        <li class="gold-bg-color question-<?echo $questionId ?>-item-1" data-slide="1">
            <div class="arrow"></div>
        </li>
        <li class="red-bg-color question-<?echo $questionId ?>-item-2" data-slide="2">
            <div class="arrow"></div>
        </li>
        <li class="grey-bg-color question-<?echo $questionId ?>-item-3" data-slide="3">
            <div class="arrow"></div>
        </li>
        <li class="blue-bg-color last question-<?echo $questionId ?>-item-4" data-slide="4">
            <div class="arrow"></div>
        </li>
    </ul>
    <!-- End of Accordion -->

    <!-- Accordion descriptions -->
    <div class="accordion-description" id="test-description">
        <ul id="test-questions">
            <?php for($i=1; $i<=8; $i++) : ?>
                <li><?php echo CHtml::link($i, Yii::app()->createUrl('test'), array('class'=>$questionId == $i ? 'active' : '')) ?></li>
            <?php endfor ?>
        </ul>
        <p class="blue-color">
            <?php
            switch($questionId) {
                case 1: echo "Мій ідеальний відпочинок – це…";
                    break;
                case 2: echo "Найбільше я люблю слухати…";
                    break;
                case 3: echo "З друзями я залюбки питиму…";
                    break;
                case 4: echo "Люблю дивитися на ТВ…";
                    break;
                case 5: echo "Мрію потрапити до…";
                    break;
                case 6: echo "Маю хист до…";
                    break;
                case 7: echo "Моя найкраща компанія для пляшки Бад – це …";
                    break;
                case 8: echo "З пивом мені смакує…";
                    break;
                default ;
                    break;
            }
            ?>
        </p>
    </div>
    <!-- End of Accordion descriptions -->
</div>
<script type='text/javascript'>
    if (top.location!= self.location)
    {
        top.location = self.location
    }
</script>

<script type="text/javascript">
    $('#test-accordion>li').on('click', function(e){
        var q_id = $('#test-accordion').data('question');
        var a_id = $(this).data('slide');

        $.ajax({
            type:"POST",
            url:"<?php echo Yii::app()->createUrl('test/answer')?>",
            data:{q_id:q_id, a_id:a_id},
            dataType:"json"
        }).done(function(data) {
            if(data.status == 'success'){
                location.reload();
            } else {
                alert('Возникла ошибка!');
            }
        });
    })
</script>