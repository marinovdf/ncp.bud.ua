<?php
/**
 * Class City
 */
class City extends CBehavior
{
    /**
     * @var CDbConnection
     */
    private $_connection;
    private $_charset;

    /**
     *
     */
    function __construct()
    {
        $this->_connection = Yii::app()->db;
        $this->_charset    = new Charset();
    }


    /**
     * Get oblast list
     *
     * @param array $params
     *
     * @return array
     */
    public function getOblast($params = array())
    {

        $sql = 'EXECUTE dbo.p_rapp_GetOblast
                    @OblastGUID =:OblastGUID,
                    @OblastName =:OblastName,
                    @DirectBy =:DirectBy';

        $command = $this->_connection->createCommand($sql);
        $command->bindValues(array(
            ':OblastGUID' => !empty($params['OblastGUID']) ? $params['OblastGUID'] : null,
            ':OblastName' => !empty($params['OblastName']) ? $params['OblastName'] : null,
            ':DirectBy'   => isset($params['DirectBy']) ? $params['DirectBy'] : 0,
        ));

        $result = $command->queryAll();

        foreach ($result as $key => $value)
            foreach ($value as $k => $v)
                $result[$key][$k] = $this->_charset->toUTF8($v);

        return $result;
    }

    /**
     * Get rayon list
     *
     * @param array $params
     *
     * @return array
     */
    public function getRayon($params = array())
    {
        $sql = 'EXECUTE dbo.p_rapp_GetRayon
                    @OblastGUID =:OblastGUID,
                    @OblastName =:OblastName,
                    @RayonGUID =:RayonGUID,
                    @RayonName =:RayonName,
                    @DirectBy =:DirectBy';

        $command = $this->_connection->createCommand($sql);
        $command->bindValues(array(
            ':OblastGUID' => !empty($params['OblastGUID']) ? $params['OblastGUID'] : null,
            ':OblastName' => !empty($params['OblastName']) ? $this->_charset->toCP1251($params['OblastName']) : null,
            ':RayonGUID'  => !empty($params['RayonGUID']) ? $params['RayonGUID'] : null,
            ':RayonName'  => !empty($params['RayonName']) ? $this->_charset->toCP1251($params['RayonName']) : null,
            ':DirectBy'   => isset($params['DirectBy']) ? $params['DirectBy'] : 0,
        ));

        $result = $command->queryAll();

        foreach ($result as $key => $value)
            foreach ($value as $k => $v)
                $result[$key][$k] = $this->_charset->toUTF8($v);

        return $result;
    }


    /**
     * Get city list
     *
     * @param array $params
     *
     * @return array
     */
    public function getCity($params = array())
    {
        $sql = 'EXECUTE dbo.p_rapp_GetCity
                    @OblastGUID =:OblastGUID,
                    @OblastName =:OblastName,
                    @RayonGUID =:RayonGUID,
                    @RayonName =:RayonName,
                    @CityGUID =:CityGUID,
                    @CityName =:CityName,
                    @DirectBy =:DirectBy';

        $command = $this->_connection->createCommand($sql);
        $command->bindValues(array(
            ':OblastGUID' => !empty($params['OblastGUID']) ? $params['OblastGUID'] : null,
            ':OblastName' => !empty($params['OblastName']) ? $this->_charset->toCP1251($params['OblastName']) : null,
            ':RayonGUID'  => !empty($params['RayonGUID']) ? $params['RayonGUID'] : null,
            ':RayonName'  => !empty($params['RayonName']) ? $this->_charset->toCP1251($params['RayonName']) : null,
            ':CityGUID'   => !empty($params['CityGUID']) ? $params['CityGUID'] : null,
            ':CityName'   => !empty($params['CityName']) ? $this->_charset->toCP1251($params['CityName']) : null,
            ':DirectBy'   => isset($params['DirectBy']) ? $params['DirectBy'] : 0,
        ));

        $result = $command->queryAll();

        foreach ($result as $key => $value)
            foreach ($value as $k => $v)
                $result[$key][$k] = $this->_charset->toUTF8($v);

        return $result;
    }
}