<?php

/**
 * Class Charset
 *
 * Convert charset
 */
class Charset extends CBehavior
{
    /**
     * Convert string to ut8 charset
     *
     * @param $str
     *
     * @return string
     */
    public function toUTF8($str)
    {
        return iconv('cp1251', 'utf8', $str);
    }

    /**
     * Convert string to cp1251 charset
     *
     * @param $str
     *
     * @return string
     */
    public function toCP1251($str)
    {
        return iconv('utf8', 'cp1251', $str);
    }
}