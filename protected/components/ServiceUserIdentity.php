<?php
class ServiceUserIdentity extends UserIdentity {
    const ERROR_NOT_AUTHENTICATED = 3;

    /**
     * @var EAuthServiceBase the authorization service instance.
     */
    private $_id;
    protected $service;

    /**
     * Constructor.
     * @param EAuthServiceBase $service the authorization service instance.
     */
    public function __construct($service) {
        $this->service = $service;
    }

    /**
     * Authenticates a user based on {@link username}.
     * This method is required by {@link IUserIdentity}.
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        if ($this->service->isAuthenticated) {
            $this->username = $this->service->getAttribute('name');

            $this->_id = $this->service->id;
            $this->setState('id', $this->service->id);
            $this->setState('facebookID', $this->service->id);
            $this->setState('name', $this->username);
            $this->setState('email', $this->service->getAttribute('email'));
            $this->setState('birthday', $this->service->getAttribute('birthday'));
            $this->setState('service', $this->service->serviceName);
            $this->setState('data', array());
            $this->errorCode = self::ERROR_NONE;

            // Трекинг авторизации
            Yii::app()->user->setFlash('FacebookAuth', '1');
        }
        else {
            $this->errorCode = self::ERROR_NOT_AUTHENTICATED;
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}