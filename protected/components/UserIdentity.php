<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    public $MobilePhone;
    public $Password;

    private $_id;

    function __construct($MobilePhone, $Password)
    {
        $this->MobilePhone = $MobilePhone;
        $this->Password    = $Password;
    }

    /**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $users = new Users();

        // Получаем юзера по мобильному телефону
        $data = $users->getUsers(array('MobilePhone'=>$this->MobilePhone));

        // Если юзера нет - возвращаем ошибку
        if (empty($data))
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            $this->errorMessage = 'Користувач не знайдений';
        } else {
            // Проходимся циклом по паролям найденных пользователей
            foreach ($data as $key)
            {
                // Если пароль находим - прерываем цикл и заводим переменную данными пользователя
                if($key['Password'] === $this->Password && $key['MValidCode'] == 1)
                {
                    $userData = (object)$key;
                    break;
                } else {
                    $this->errorCode = self::ERROR_PASSWORD_INVALID;
                    $this->errorMessage = 'Невірний пароль';
                }
            }
        }

        // Если идентифицировали пользователя - заносим данные в сессию
        if(!empty($userData))
        {
            $this->_id = $userData->MainMobilePhone;

            $this->setState('name', $userData->FirstName);
            $this->setState('email', $userData->EmailAddress);
            $this->setState('phone', $userData->MainMobilePhone);
            $this->setState('data', $userData);
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
	}

    public function getId()
    {
        return $this->_id;
    }
}