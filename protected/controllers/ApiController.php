<?php

/**
 * Class ApiController
 * Контроллер для взаимодействия с сервером JUMP
 */
class ApiController extends Controller
{
    /**
     * Модель пользователя
     *
     * @var Api
     */
    private $_api;

    /**
     * Ответ от сервера
     *
     * @var array
     */
    private $_output;

    /**
     * Иниациализация класса
     */
    public function init()
    {
        $this->_api   = new Api();
        $this->_output = array('status' => 'error', 'message' => '');

        $this->layout = false;
        parent::init();
    }

    /**
     *
     */
    public function actionIndex()
    {
        echo $this->action->id;
    }

    /**
     * Проверяет наличие пользователя в CRM по его номеру телефона и паролю
     *
     * @param null $phone    Телефон пользователя
     * @param null $password Пароль пользователя
     * @param null $activity Вид активности
     */
    public function actionAuth($phone = null, $password = null, $activity = null)
    {
        //$this->disableLog();

        $this->_api->phone = Yii::app()->request->getParam('phone',null);
        $this->_api->password = Yii::app()->request->getParam('password',null);
        $this->_api->activity = Yii::app()->request->getParam('activity',null);

        $this->_api->activity = preg_replace('|stadiums|','stadium',$this->_api->activity);

        if($this->_api->validate(array('activity'))){
            if ($this->_api->validate(array('password','phone'))) {
                // Если пользователь не найден - отправляем ответ
                if ($user = $this->_api->isUserExists()) {

                    if ($user['Password'] === $this->_api->password) { // если пользователь найден
                        $loginForm             = new LoginForm;
                        $loginForm->attributes = array(
                            'phone'    => $this->_api->phone,
                            'password' => $this->_api->password
                        );

                        try {
                            if($this->_api->getUserCountries() >= 8){
                                if ($loginForm->login()) { // пытаемся залогинить пользователя
                                    $this->redirect('/');
                                } else {
                                    $this->redirect('http://football.bud.ua');
                                }
                            } else {
                                $this->redirect('http://football.bud.ua');
                            }
                        } catch (Exception $e) {
                            $this->redirect('http://football.bud.ua');
                        }
                    } else { // если пароль не совпадает
                        Yii::app()->user->setFlash('RegistrationAllowed', true);
                        $this->redirect('/site/registration');
                    }
                } else { // если пользователь не найден
                    Yii::app()->user->setFlash('RegistrationAllowed', true);
                    $this->redirect('/site/registration');
                    Yii::app()->end();

                    //echo $this->_user->getPassword($this->_user->phone);
                }
            } else {
                $this->_output['message'] = $this->_api->getErrors();
            }
        } else {
            Yii::app()->user->setFlash('RegistrationAllowed', true);
            $this->redirect('/site/registration');
            Yii::app()->end();
        }
        //echo json_encode($this->_output);
        Yii::app()->user->setFlash('RegistrationAllowed', true);
        $this->redirect('/site/registration');
        Yii::app()->end();
    }

    /**
     * Тестовый метод для получения сгенерированного пароля
     *
     * @param null $phone
     *
     * @return bool
     */
    public function actionGetPassword($phone = null)
    {
        $this->disableLog();

        $this->_api->phone = $phone;

        try{
            $password = $this->_api->getPassword();
        } catch (Exception $e) {
            echo $e->getMessage();

            return false;
        }

        echo $password;

        Yii::app()->end();
    }

    public function actionGetCountries($phone = null)
    {
        $this->disableLog();

        $this->_api->phone = $phone;

        try{
            $countries = $this->_api->getUserCountries();
        } catch (Exception $e) {
            echo $e->getMessage();

            return false;
        }

        echo $countries;

        Yii::app()->end();
    }

    /**
     * Проверка доступности Passport 2 Rio
     *
     * @param null $phone    Номер телефона
     * @param null $password Пароль
     * @param null $token    Токен от JUMP
     */
    public function actionPassport2Rio($phone = null, $password = null, $token = null)
    {
        $this->disableLog();

        $this->_api->attributes = array(
            'phone'    => $phone,
            'password' => $password,
            'token'    => $token
        );

        if($this->_api->validate()){

            //тестовая проверка токена
            //echo $this->_api->checkValidToken();

            Yii::app()->end();
        } else {
            $this->_output['message'] = $this->_api->getErrors();
        }

        echo json_encode($this->_output);

        Yii::app()->end();

        return;
    }

    /**
     * Тестовый метод для валидации токена
     */
    public function actionCheckValidToken($phone = null, $token = null)
    {
        $output = array(
            'status'           => 'ok',
            'phone'            => $phone,
            'token'            => $token,
            'validTokenStatus' => $token == 'n9gbt25bv8gm4re4qd93rt7q04' ? 1 : 0
        );

        echo json_encode($output);

        Yii::app()->end();

        return;
    }
}