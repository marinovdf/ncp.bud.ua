<?php

/**
 * Class SiteController
 */
class SiteController extends Controller
{
    public function init()
    {
        Yii::app()->theme = null;

        parent::init(); // TODO: Change the autogenerated stub
    }


    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class'     => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'    => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex($r_id = null)
    {

        if ($r_id) {
            $emailInviteModel = new EmailInvite();

            if ($emailInviteModel->isExistsRequestID($r_id)) {
                $cookie                                = new CHttpCookie('e_r_id', $r_id);
                $cookie->expire                        = time() + 60 * 60 * 24 * 30;
                Yii::app()->request->cookies['e_r_id'] = $cookie;
            }
        }

        if (!empty(Yii::app()->user->data)) {

            Yii::trace(CVarDumper::dumpAsString(Yii::app()->user->data), 'vardump');

            if (!empty(Yii::app()->user->data->ConsumerGUID)) {

                $emailRequestID = (isset(Yii::app()->request->cookies['e_r_id']->value)) ?
                    Yii::app()->request->cookies['e_r_id']->value : '';

                if ($emailRequestID) {
                    $emailInviteModel = new EmailInvite();

                    if ($emailInviteModel->isExistsRequestID($emailRequestID)) {
                        $emailInviteModel->attributes = array(
                            'EmailRequestID' => $emailRequestID,
                            'FriendUserGUID' => Yii::app()->user->data->ConsumerGUID,
                            'InviteStatus'   => 'invited',
                        );

                        if ($emailInviteModel->validate()) {
                            if ($emailInviteModel->updateInviteStatus()) {
                                unset(Yii::app()->request->cookies['e_r_id']);
                            };

                        } else {

                        }
                    } else {
                        unset(Yii::app()->request->cookies['e_r_id']);
                    }
                } else {
                    unset(Yii::app()->request->cookies['e_r_id']);
                }
            }
        }

        $this->render('index');
    }

    /**
     * Рендерит страницу "Правила"
     */
    public function actionRules()
    {
        $this->render('rules');
    }

    /**
     * Рендерит страницу "Подарки"
     */
    public function actionGifts()
    {
        $this->render('gifts');
    }


    /**
     * Рендерит страницу регистрации
     */
    public function actionRegistration()
    {
        $model = new PassportRegistrationForm();

        if ($post = Yii::app()->request->getPost('PassportRegistrationForm')) {
            $model->attributes = $post;

            if ($model->validate()) {
                $model->save();
            }
        } else {
            if(!Yii::app()->user->hasFlash('RegistrationAllowed')){
                $this->redirect('http://football.bud.ua');
                Yii::app()->end();
            }
        }

        $this->render('registration/index', array('model' => $model));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $service = Yii::app()->request->getQuery('service');

        if (!empty($service)) {
            $authIdentity              = Yii::app()->eauth->getIdentity($service);
            $authIdentity->redirectUrl = Yii::app()->user->returnUrl;
            $authIdentity->cancelUrl   = $this->createAbsoluteUrl('site/index');

            if ($authIdentity->authenticate()) {
                $identity = new ServiceUserIdentity($authIdentity);
                // Успешный вход
                if ($identity->authenticate()) {

                    Yii::app()->user->login($identity);
                    $user = new Users();

                    // если пользователь не найден по его id
                    // редиректим на регистрацию
                    if ($userData = $user->getUser(array('FacebookUserID' => Yii::app()->user->id))) {
                        if ($userData['MValidCode'] == 1) {
                            Yii::app()->user->data = (object)$userData;

                            // Фиксируем пользователя как участника активности
                            $consumerGUID = !empty(Yii::app()->user->data->ConsumerGUID) ? Yii::app()->user->data->ConsumerGUID : null;

                            if ($consumerGUID) {
                                $params = array('ConsumerGUID' => $consumerGUID);

                                Users::setCampaignActivity($params);
                            }

                            $authIdentity->redirect();
                        } else {
                            // Редирект на регистрацию
                            $authIdentity->redirect(Yii::app()->createUrl('site/registration'));

                        }
                    } else {
                        /*
                         * если пользователя нашли по email,
                         * апдейдтим его facebookID
                         * */
                        if ($userData = $user->getUser(array('EmailAddress' => Yii::app()->user->email))) {
                            if ($userData['MValidCode'] == 1) {
                                Yii::app()->user->data = (object)$userData;

                                /*
                                 * проверяем, совпадает ли facebookID найденного пользователя с facebookID залогиненного.
                                 * Если не совпадает - апдейтим учетную запись
                                 * */
                                if (Yii::app()->user->id != $userData['FaceBookUserID']) {
                                    $regModel = new RegistrationForm();

                                    $regModel->attributes = array(
                                        'LastName'       => $userData['LastName'],
                                        'FirstName'      => $userData['FirstName'],
                                        'Patronymic'     => $userData['Patronymic'],
                                        'Birthday'       => $userData['BirthDate'],
                                        'Gender'         => $userData['GenderValue'],
                                        'CityName'       => $userData['CityName'],
                                        'Rayon'          => $userData['RayonName'],
                                        'Oblast'         => $userData['OblastName'],
                                        'operatorCode'   => substr($userData['MainMobilePhone'], 0, 5),
                                        'MobilePhone'    => substr($userData['MainMobilePhone'], 5, 7),
                                        'Email'          => $userData['EmailAddress'],
                                        'Password'       => $userData['Password'],
                                        'FaceBookUserID' => Yii::app()->user->id
                                    );

                                    $regModel->save();
                                }

                                $authIdentity->redirect();
                            } else {
                                // Редирект на регистрацию
                                $authIdentity->redirect(Yii::app()->createUrl('site/registration'));
                            }

                        } else {
                            // Редирект на регистрацию
                            $authIdentity->redirect(Yii::app()->createUrl('site/registration'));
                        }
                    }
                } else {
                    // Закрываем popup окно и перенаправляем на cancelUrl
                    $authIdentity->cancel();
                }
            }

            // Что-то пошло не так, перенаправляем на страницу входа
            $this->redirect(array('site/index'));
        }

        $model = new LoginForm;

        // collect user input data
        if ($post = Yii::app()->request->getPost('LoginForm')) {
            $model->attributes = $post;
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {


                // $this->redirect('site/index');

                // Yii::app()->end();
            } else {
                Yii::app()->user->setFlash('loginError', 'error');
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }


    /**
     * This is the method to rendering auth page
     */
    public function actionAuth()
    {
        $this->render('auth');
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);

        Yii::app()->end();
    }

    /**
     * This method is return list of rayon
     */
    public function actionGetRayonList()
    {
        $obl = Yii::app()->request->getPost('OblastName');

        if (!$obl)
            $list = array();
        else {
            $city = new City();

            $list = $city->getRayon(array('OblastName' => $obl));
        }

        foreach ($list as $key)
            echo CHtml::tag('option',
                array('value' => $key['RayonName']), CHtml::encode($key['RayonName']), true);

        Yii::app()->end();
    }

    /**
     * This method is return list of cities
     */
    public function actionGetCityList()
    {
        $rayon = Yii::app()->request->getPost('RayonName');

        if (!$rayon)
            $list = array();
        else {
            $cities = new City();

            $list = $cities->getCity(array('RayonName' => $rayon));
        }

        foreach ($list as $key)
            echo CHtml::tag('option',
                array('value' => $key['CityName']), CHtml::encode($key['CityName']), true);

        Yii::app()->end();
    }

    /**
     * This is the method to rendering feedback form and sending feedback message
     */
    public function actionFeedback()
    {
        $model = new FeedbackForm();

        if (Yii::app()->request->isPostRequest) {
            $model->attributes = Yii::app()->request->getPost('FeedbackForm');

            if ($model->validate()) {
                $mail = new Mail();

                if ($mail->sendFeedbackMail('support@rappukraine.com', $model->email, 'Bud Profiling', 'Feedback', $model->body)) {
                    Yii::app()->user->setFlash('FeedbackStatus', 'success');

                    $model->resetForm();
                } else
                    Yii::app()->user->setFlash('FeedbackStatus', 'error');
            }
        }

        $this->render('contact', array('model' => $model));
    }


    /**
     * This is the method to remind password
     */
    public function actionRemind()
    {
        $model = new ReminderForm();

        if ($post = Yii::app()->request->getPost('ReminderForm')) {
            $model->attributes = array(
                'operatorCode' => $post['operatorCode'],
                'Phone'        => $post['Phone'],
                'verifyCode'   => $post['verifyCode']
            );

            if ($model->validate()) {
                $reminderPasswordModel = new ReminderPassword();
            }
        }


        $this->render('remind', array('model' => $model));

    }

    public function actionPopup()
    {
        $this->renderPartial('popup');
    }

    public function actionFinal()
    {
        $this->render('test/final/f1');
    }

    /**
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        if (Yii::app()->params['stopActivity']) {
            return array(
                array('allow',
                    'users'   => array('*'),
                    'actions' => array('index', 'rules', 'feedback', 'test', 'gifts', 'captcha'),
                ),
                array('deny',
                    'users' => array('*'),
                ),
            );
        } else {
            return array();
        }
    }
}