<?php
/**
 * Class GameController
 */
class ProfileController extends Controller
{
    public function init()
    {
        
        /*if(!Yii::app()->user->isGuest)
        {
            if(empty(Yii::app()->user->data->ConsumerGUID))
                throw new CHttpException(403,'Доступ заборонений!');
        }*/

        /*if ((isset($_COOKIE['age']) && $_COOKIE['age'] != 1) || !isset($_COOKIE['age']))
            throw new CHttpException(403,'Доступ заборонений!');*/

        parent::init();
    }


    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $model = new Profile();

        $this->render('profile', array('model'=>$model));
    }

    /**
     * Render profile page
     */
    public function actionEdit()
    {
        $model = new ProfileForm();

        if($userData = Yii::app()->request->getPost('ProfileForm'))
        {
            $model->attributes = $userData;

            if($model->validate())
            {
                if($model->save())
                {
                    Yii::app()->user->setFlash('ShowPopup', array('title'=>'Повідомлення', 'content'=>'Профайл збережений'));

                    // Переписываем данные пользователя в сессии
                    if($user = (object)$model->getConsumer(array('EmailAddress'=>Yii::app()->user->email)))
                        Yii::app()->user->data = $user;

                    $this->redirect(Yii::app()->createUrl('profile'));
                    Yii::app()->end();
                }
                else
                    Yii::app()->user->setFlash('ShowPopup', array('title'=>'Помилка', 'content'=>'Увага! Виникла помилка. Профайл не збережений'));
            }
        } else {
            $userData = (object)Yii::app()->user->data;

            $model->attributes = array(
                'LastName'       => $userData->LastName,
                'FirstName'      => $userData->FirstName,
                'Patronymic'     => $userData->Patronymic,
                'Birthday'       => date('Y.m.d', strtotime($userData->BirthDate)),
                'Gender'         => $userData->GenderValue,
                'CityName'       => $userData->CityName,
                'Rayon'          => $userData->RayonName,
                'Oblast'         => $userData->OblastName,
                'operatorCode'   => substr($userData->MainMobilePhone, 0, 5),
                'MobilePhone'    => substr($userData->MainMobilePhone, 5, 7),
                'Email'          => $userData->EmailAddress,
                'Password'       => $userData->Password,
                'FaceBookUserID' => Yii::app()->user->id
            );
        }

        $this->render('edit', array('model'=>$model));
    }

    /**
     * Метод для проверки полной авторизации (не фейсбучная)
     *
     * @return bool
     */
    public function checkAccessRule()
    {
        if(!empty(Yii::app()->user->data)){
            if(!empty(Yii::app()->user->data->ConsumerGUID))
                return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'   => array('@'),
                'actions' => array('index', 'edit'),
                'expression'=> array($this, 'checkAccessRule')
                /*'deniedCallback' => function() {
                        throw new CHttpException(403, 'Доступ заборонений!');
                    },*/
            ),

            array('deny',
                'users' => array('*'),
                'actions'=> array('index', 'edit'),
                'deniedCallback' => function() {
                        Yii::app()->user->setFlash('AccessDeniedPopup', true);
                        Yii::app()->request->redirect('/');
                    },
            ),
        );
    }
}