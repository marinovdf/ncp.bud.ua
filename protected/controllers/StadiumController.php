<?php

class StadiumController extends Controller
{

    /**
     * Already selected caps params
     * @var
     */
    public $topCapsListParams = null;

    /**
     * CRM user data array
     * @var null
     */
    private $_stateUser = null;

//	// Uncomment the following methods and override them if needed
//	public function filters()
//	{
//		// return the filter configuration for this controller, e.g.:
//		return array(
//			'inlineFilterName',
//			array(
//				'class'=>'path.to.FilterClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}

//	public function actions()
//	{
//		// return external action classes, e.g.:
//		return array(
//			'action1'=>'path.to.ActionClass',
//			'action2'=>array(
//				'class'=>'path.to.AnotherActionClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}

    /**
     * Инициализация класса
     */
    public function init()
    {
        Yii::app()->theme = null;

        // TODO: change user state param!!!!
        $this->_stateUser = Stadium::$userState;
//        $this->_stateUser = Yii::app()->user->getState('data');

        // checking auth status of user
        if(isset($this->_stateUser) && !empty($this->_stateUser)){

            $flag = Yii::app()->user->getState('stadiumUserExcising');
            $userData = Stadium::getUserByGUID($this->_stateUser['ConsumerGUID']);

            // set flag, if user have new promo-code
            if(isset($userData) && !empty($userData))
                if(isset($userData['last_code']) && !empty($userData['last_code']))
                    Yii::app()->user->setState('newPromoCodeAdded', true);

//            Stadium::clearSession();
//            die;

            if(empty($flag)){

                // create new record if it's need
                if(!Stadium::isUser($this->_stateUser['ConsumerGUID'])){
                    if(Stadium::createNewUser($this->_stateUser['ConsumerGUID']))
                        Yii::app()->user->setState('stadiumUserExcising', true);
                } else
                    Yii::app()->user->setState('stadiumUserExcising', true);

                $this->topCapsListParams = $this->buildCapsList($userData);

            } else
                $this->topCapsListParams = $this->buildCapsList($userData);

        }
        parent::init();
    }

    private function buildCapsList($user)
    {
        if(isset($user) && !empty($user)){
            $selectedCaps = array();
            foreach(Stadium::$country_code_array as $k => $v)
                if(isset($user['cap_'.$v]) && !empty($user['cap_'.$v]))
                    $selectedCaps[$v] = $user['cap_'.$v];

            $res = json_encode($selectedCaps);
        } else
            $res = json_encode(array());

        return $res;
    }

    /**
     * Рендерит страницу регистрации
     */
    public function actionRegistration()
    {
        $model = new StadiumRegistrationForm();

        if ($post = Yii::app()->request->getPost('StadiumsRegistrationForm')) {
            $model->attributes = $post;
            if ($model->validate()) {
                $model->save();
            }
        }

        $this->render('//site/registration/index', array('model' => $model));
    }

    /**
     * Deactivation method
     * @return string
     * @throws CHttpException
     */
    public function actionDeactivateNewPromoCodePopupViewing()
    {
        if (!Yii::app()->request->isAjaxRequest) throw new CHttpException('Url should be requested via ajax only');

        $output = array();
        $form = Yii::app()->request->getPost("Stadium");
        if($form){
            if(isset($this->_stateUser) && !empty($this->_stateUser)){

                $model = Stadium::model()->findByAttributes(array('user_guid'=>$this->_stateUser['ConsumerGUID']));
                $model->last_code = null;

                if($model->save()){
                    $output['status']= "ok";
                    Yii::app()->user->setState('newPromoCodeAdded', null);
                } else {
                    $output['status']= "error";
                    $output['message']= "Data doesn't saved!";
                }

            } else {
                $output['status']= "error";
                $output['message']= "State data is empty!";
            }

            echo json_encode($output);
            Yii::app()->end();

        } else{
            echo json_encode(array('status' => "error", "message" => "Ha-ha!"));
            Yii::app()->end();
        }

    }

    /**
     * Index and all =)
     */
    public function actionIndex()
    {
//        for($i=0;$i<1000;$i++){
//            Stadium::createNewUser(Stadium::createGUID());
//        }

        $this->layout = "stadiums";

        // check auth status
        if(isset($this->_stateUser) && !empty($this->_stateUser)){
            // get current user from MySQL
            $user = Stadium::getUserByGUID($this->_stateUser['ConsumerGUID']);
            $currentCheckIn = "";
            if(isset($user) && !empty($user)){
                $currentCheckIn = $user['current_st'];
                $lastCode = $user['last_code'];
            }

            $wonCaps = $this->buildCapsList($user);

        } else {
            $currentCheckIn = "";
            $wonCaps = "";
        }

        $this->render("index", array(
            'currentCheckIn' => $currentCheckIn,
            'wonCaps' => $wonCaps,
        ));

        $flag = Yii::app()->user->getState('newPromoCodeAdded');
        if(isset($flag) && !empty($flag)){
            $this->renderPartial("new_code_popup", array("code"=>isset($lastCode) ? $lastCode : ""));
        }
    }

    /**
     * @param $id
     * @throws Exception
     */
    public function actionChecking($id)
    {
        $this->layout = "stadiums";
        $model = new Stadium();

        $stadiumCode = Stadium::$country_code_array[$id];
        if(isset($stadiumCode) && !empty($stadiumCode)){

            // get count of users by cap_*
            $stadiumPeopleCounter = $model->countAllUsersByCap($stadiumCode);

            // Calculate percent of people on stadium
            if($stadiumPeopleCounter >= 0 && $stadiumPeopleCounter <= 100)
                $percent = $stadiumPeopleCounter;
            else
                $percent = 100;

            // check auth status
            if(isset($this->_stateUser) && !empty($this->_stateUser)){
                // get current user from MySQL
                $user = Stadium::getUserByGUID($this->_stateUser['ConsumerGUID']);

                // check existence "check-in" for selected stadium
                if($stadiumCode == $user['current_st'])
                    $countryAlreadySelected = true;

                $isAuth = true;
            } else
                $isAuth = false;


            $this->render("checking", array(
                "stadiumPeopleCounter" => $stadiumPeopleCounter,
                "percent" => $percent,
                "countryAlreadySelected" => isset($countryAlreadySelected) ? $countryAlreadySelected : "",
                "selected_st" => $stadiumCode,
                'isAuth' => $isAuth,
            ));
        } else
            throw new Exception("ERROR: This stadium doesn't exist!");
    }


    public function actionChoiceOfStadium()
    {
        if (!Yii::app()->request->isAjaxRequest) throw new CHttpException('Url should be requested via ajax only');

        $postData = Yii::app()->request->getPost("Stadium");
        if(isset($postData) && !empty($postData)){

            $output = array();

            if(isset($this->_stateUser) && !empty($this->_stateUser)){
                // get current user from MySQL
                $user = Stadium::getUserByGUID($this->_stateUser['ConsumerGUID']);

                // checking for slyly asses dudes!
                if($user['current_st'] != $postData['selected_st']){
                    if(isset($postData['selected_st']) && !empty($postData['selected_st']))
                        $output = Stadium::changeStadium($postData['selected_st'] , $this->_stateUser['ConsumerGUID']);
                    else {
                        $output['status'] = "error";
                        $output['message'] = "Stadium is not selected!";
                    }
                } else {
                    $output['status'] = "error";
                    $output['message'] = "ERROR: Ha-ha-ha!";
                }
            } else {
                $output['status'] = "error";
                $output['message'] = "ERROR: You are not authenticated!";
            }

            echo json_encode($output);
            Yii::app()->end();

        } else
            throw new Exception("ERROR: Sorry, but you not right!");
    }


    /**
     * Action for CRON
     * Raffle of codes
     */
    public function actionRaffle()
    {
        $stadiumsCounts = array();

        $stadiumsCounts['BR'] = Stadium::getStadiumCountByCode("BR");
        $stadiumsCounts['IT'] = Stadium::getStadiumCountByCode("IT");
        $stadiumsCounts['DE'] = Stadium::getStadiumCountByCode("DE");
        $stadiumsCounts['AR'] = Stadium::getStadiumCountByCode("AR");
        $stadiumsCounts['UY'] = Stadium::getStadiumCountByCode("UY");
        $stadiumsCounts['FR'] = Stadium::getStadiumCountByCode("FR");
        $stadiumsCounts['ES'] = Stadium::getStadiumCountByCode("ES");
        $stadiumsCounts['EN'] = Stadium::getStadiumCountByCode("EN");


        $alreadySelected = array();
        foreach($stadiumsCounts as $st_code => $v){
            if($v >= 100){
                if(!in_array($v , $alreadySelected))
                    $alreadySelected[] = $v;
                else{
                    $logResponse = Stadium::saveRaffleLog(array(
                        'stadium_code' => $st_code,
                        'desc' => json_encode($stadiumsCounts),
                        'error' => 'Repeated values of stadium count.'
                    ));
                    if($logResponse){
                        /** ==== Avada Kedavra ==== */
                        echo "Repeated values of stadium count.";
                        die;
                    }
                }
            } else {
                $logResponse = Stadium::saveRaffleLog(array(
                    'stadium_code' => $st_code,
                    'desc' => json_encode($stadiumsCounts),
                    'error' => 'Stadium count value lower than 100.'
                ));
                if($logResponse){
                    /** ==== Avada Kedavra ==== */
                    echo "Stadium count value lower than 100.";
                    die;
                }
            }
        }

        // sorting array from higher to lower values
        arsort($stadiumsCounts);


        // get first array key and value -> it's winner
        $winnerStCode = null;
        foreach($stadiumsCounts as $key => $val){
            $winnerStCode = $key;
            break;
        }


        // get needed users by st code
        $winners = Stadium::getStadiumUsersByCode($winnerStCode);

        // checking for empty winners array
        if(empty($winners)){
            $errorLog = Stadium::saveRaffleLog(array(
                'stadium_code' => $winnerStCode,
                'desc' => json_encode($stadiumsCounts),
                'error' => 'All users have previously won.'
            ));
            if($errorLog){
                echo "All users have previously won.";
                die;
            }
        }

        // get needed codes
        $selectedCapCodes = Stadium::getNeededCodesForRaffle($winnerStCode , count($winners));

        // checking for empty promo-codes array
        if(empty($selectedCapCodes)){
            $errorLog = Stadium::saveRaffleLog(array(
                'stadium_code' => $winnerStCode,
                'desc' => json_encode($stadiumsCounts),
                'error' => 'Promo-codes is ended.'
            ));
            if($errorLog){
                echo "Promo-codes is ended.";
                die;
            }
        }

        // set code to user
        $i = 0;
        foreach($winners as $winnerUser){
            Stadium::setCodeToUser($selectedCapCodes[$i],$winnerStCode,$winnerUser['user_guid']);
            $i++;
        }

        // save winner log
        $resLog = Stadium::saveRaffleLog(array(
            'stadium_code' => $winnerStCode,
            'desc' => "Winner stadium: $winnerStCode; => " . json_encode($stadiumsCounts),
            'users_count' => count($winners)
        ));

        if($resLog){
            /** ==== Avada Kedavra ==== */
            echo "Success!";
            die;
        }

    }


}