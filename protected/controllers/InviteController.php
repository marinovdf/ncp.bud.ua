<?php

/**
 * Class InviteController
 */
class InviteController extends Controller
{
    /**
     * This is the method to rendering invite page
     */
    public function actionIndex()
    {

        $this->render('//site/invite');
    }

    /**
     * This is the method to save facebook invite
     *
     * @throws CHttpException
     */
    public function actionFacebookInvite()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model = new FacebookInvite();

            if ($friendId = Yii::app()->request->getPost('friend_id', null)) {
                $model->attributes = array(
                    'FriendFacebookID'  => $friendId[0],
                    'InviteStatus'      => 'send',
                    'FacebookRequestID' => Yii::app()->request->getPost('request_id', null)
                );

                if ($model->validate()) {
                    if ($model->save()) {

                    } else {
                        $error = $model->getErrors();
                    }
                } else {
                    $error = $model->getErrors();
                }

                // Формируем ответ сервера
                if (!empty($error)) {
                    echo json_encode(array('status' => 'error', 'message' => $error));
                } else {
                    echo json_encode(array('status' => 'success'));
                }
            }
        } else {
            throw new CHttpException(403, 'request should only be called AJAX');
        }
    }

    public function actionEmailInvite()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model = new EmailInviteForm();

            $requestId = uniqid('e_');

            $data = Yii::app()->request->getPost('EmailInviteForm', null);

            $model->attributes = $data;

            if($model->validate()){
                $inviteModel = new EmailInvite();

                $inviteModel->attributes = array(
                    'FriendEmail'    => $model->email,
                    'InviteStatus'   => 'send',
                    'EmailRequestID' => $requestId
                );

                if($inviteModel->validate()){

                    try{
                        $model->sendInviteMail($requestId);
                    } catch (Exception $e){
                        $error = $e->getMessage();
                    }

                    if(empty($error)){
                        if($inviteModel->save()){

                        } else {
                            $error = $inviteModel->getErrors();
                        }
                    }

                } else {
                    $error = $inviteModel->getErrors();
                }

            } else {
                $error = $model->getErrors();
            }

            // Формируем ответ сервера
            if (!empty($error)) {
                echo json_encode(array('status' => 'error', 'message' => $error));
            } else {
                echo json_encode(array('status' => 'success'));
            }
        } else {
            throw new CHttpException(403, 'request should only be called AJAX');
        }
    }

    /**
     * Метод для проверки полной авторизации (не фейсбучная)
     *
     * @return bool
     */
    public function checkAccessRule()
    {
        if(!empty(Yii::app()->user->data)){
            if(!empty(Yii::app()->user->data->ConsumerGUID))
                return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'   => array('@'),
                'actions' => array('index', 'emailInvite', 'facebookInvite'),
                'expression'=> array($this, 'checkAccessRule')
                /*'deniedCallback' => function() {
                        throw new CHttpException(403, 'Доступ заборонений!');
                    },*/
            ),

            array('deny',
                'users' => array('*'),
                'actions'=> array('index', 'emailInvite', 'facebookInvite'),
                'deniedCallback' => function() {
                        Yii::app()->user->setFlash('AccessDeniedPopup', true);
                        Yii::app()->request->redirect('/');
                    },
            ),
        );
    }
}