<?php

/**
 * Class PassportController
 * Контроллер активности Passport2Rio
 */
class PassportController extends Controller
{
    /**
     * Инициализация класса
     */
    public function init()
    {
        Yii::app()->theme = null;

        parent::init();
    }

    public function actionIndex()
    {
        $this->redirect('app');
    }

    /**
     * Рендерит стрницу с флеш-приложением
     */
    public function actionApp()
    {
        $this->render('app');
    }

    /**
     * Екшн для работы с API флеш-приложения
     *
     * @param null $method Название метода API
     */
    public function actionApi($method = null)
    {
        if(!Yii::app()->user->isGuest){

            if(!empty(Yii::app()->user->data)){
                if(!empty(Yii::app()->user->data->ConsumerGUID)){

                    $questionnaire           = new Anketa();
                    $questionnaire->UserGUID = Yii::app()->user->data->ConsumerGUID;

                    $quiz           = new Quiz();
                    $quiz->UserGUID = Yii::app()->user->data->ConsumerGUID;

                    if (!empty($_FILES['croppedImage'])) {
                        $questionnaire->image = $_FILES['croppedImage'];

                        if (!$questionnaire->saveImage()) {
                            die('image saved error');
                        }
                    }

                    //TODO принимать параметры постом
                    //TODO прикрутить CSRF-валидацию

                    //отключаем логирование во избежании попадания js логов в аутпут
                    $this->disableLog();

                    $output = array(
                        'status' => '1',
                    );

                    // Получаем параметры из массива $_POST
                    if(Yii::app()->request->isPostRequest){
                        $post = $_POST;
                    } else {
                        $post = null;
                    }

                    $method = Yii::app()->request->getParam('method', null);// fixme после тестинга включить только POST

                    switch ($method) {
                        case 'clearData' :
                            $output = $questionnaire->clearData();
                            break;

                        case 'getSessionData' :
                            $output = $questionnaire->getSessionData();
                            break;

                        case 'setQuestionnaireStep' :

                            $questionnaire->stepId = $post['stepId'];
                            $questionnaire->attributes = array(
                                'UserGUID' => $post['userId'],
                            );

                            $output = $questionnaire->setQuestionnaireStep();
                            break;

                        case 'setQuestionnaireStepResult' :
                            $questionnaire->stepId = $post['stepId'];
                            $questionnaire->result = json_decode($post['result']);
                            $questionnaire->attributes = array(
                                'UserGUID' => $post['userId'],
                            );

                            $output = $questionnaire->setQuestionnaireStepResult();
                            break;

                        case 'saveQuestionnaireData' :
                            $questionnaire->attributes = array(
                                'UserGUID' => $post['userId'],
                            );

                            $output = $questionnaire->saveQuestionnaireData();
                            break;

                        case 'sendQuestionnaireData' :
                            $questionnaire->attributes = array(
                                'UserGUID' => $post['userId'],
                            );

                            $output = $questionnaire->sendQuestionnaireData();
                            break;

                        case 'sendQuestionnaireImage' :
                            $questionnaire->attributes = array(
                                'UserGUID' => $post['userId'],
                            );

                            $output = $questionnaire->sendQuestionnaireImage();
                            break;

                        case 'setQuizStep' :
                            $questionnaire->attributes = array(
                                'UserGUID'  => $post['userId'],
                            );

                            $output = $questionnaire->setQuizStep();
                            break;

                        case 'setQuizStepResult' :
                            $questionnaire->stepIndex  = $post['stepIndex'];
                            $questionnaire->stepId     = $post['stepId'];
                            $questionnaire->result     = json_decode($post['result']);
                            $questionnaire->attributes = array(
                                'UserGUID' => $post['userId'],
                                'result'   => $post['result'] //json!
                            );

                            $output = $questionnaire->setQuizStepResult();
                            break;

                        default :
                            $output['message'] = 'invalid api method';
                    }
                } else {
                    $output = array(
                        'status' => 0,
                        'message' => 'empty user guid'
                    );
                }
            } else {
                $output = array(
                    'status' => 0,
                    'message' => 'empty user data'
                );
            }

        } else {
            $output = array(
                'status' => 0,
                'message' => 'forbidden'
            );
        }

        //выбрасываем ответ сервера
        echo json_encode($output);
        Yii::app()->end();
    }

    public function actionShareLog()
    {
        if(Yii::app()->request->isAjaxRequest){
            $model = new PassportShareLog;

            $model->attributes = array(
                'UserGUID' => Yii::app()->user->data->ConsumerGUID,
                'Phone'=> Yii::app()->user->data->MainMobilePhone,
                'PostID'=>Yii::app()->request->getPost('post_id',null)
            );

            if($model->validate()){
                try {
                    $model->save();
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else {
                die('error');
            }
        }
    }

    /**
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users'   => array('*'),
                'actions' => array('registration', 'api'),
            ),

            array('allow',
                'users' => array('@'),
                'actions' => array('app', 'index', 'shareLog')
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }
}