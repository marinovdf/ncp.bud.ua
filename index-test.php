<?php
if (!empty($_GET['val'])) {
    echo 'ajax result';
    exit;
}
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#get-data-lnk').on('click', function (e) {
                e.preventDefault();

                $.get('index-test.php',
                    {
                        val: 1, // параметры
                        val2: 2 // параметры
                    }, function (data) {            //data - то, что пришло аяксом
                        $('#result').html(data);
                    }
                );
            })
        })
    </script>
</head>
<body>
<a href="/" id="get-data-lnk">get data</a> <br/>

<div id="result">
    Result
</div>
</body>
</html>