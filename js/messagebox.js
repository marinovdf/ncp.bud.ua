/* 
 * Developed by
 * YOBA
 * 
 */

function MessageBox(){
  $('.dialog button').click(MessageBox.hide);
  $(window).resize(function(){Utils.centerfy('.dialog')});
}

MessageBox.show=function(message){
    $('.dialog .message').html(message);
    $('.dialog').show();
    Utils.centerfy('.dialog');
};
  
MessageBox.hide=function(){
    $('.dialog').hide();
};
