$(document).ready(function () {
	/***** Cabinet panel *****/
    $('#cabinet').find('.grip').on('click',function(e){
        var duration = 100;
        var panel = $('#cabinet');

        if(panel.hasClass('opened')){
            panel.removeClass('opened',duration).addClass('closed', duration);
        } else {
            panel.removeClass('closed', duration).addClass('opened',duration);
        }
    });
	/***** End of Cabinet panel *****/

    /***** Jump Redirect *****/
    $('.to-jump').on('click',function(e){
        e.preventDefault();
        var page = $(this).data('page');

        $('#to-jump-form').find("input[name='current_page']").val(page);
        $('#to-jump-form').submit();
    });
    /***** End of Jump Redirect *****/
});