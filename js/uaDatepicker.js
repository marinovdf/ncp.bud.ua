$(document).ready(function(){
	if($.datepicker){
		$.datepicker.regional['ua']={
			closeText:'Закрити',prevText:'Попередній',nextText:'Наступний',currentText:'',
			monthNames:['Січень','Лютий','Березень','Квітень','Травень','Червень',
				'Липень','Серпень','Вересень','Жовтень','Листопад','Грудень'],
			monthNamesShort:['Січ.','Лют.','Бер.','Квіт.','Трав.','Черв.',
				'Лип.','Серп.','Вер.','Жовт.','Лист.','Груд.'],
			dayNames:['Неділя','Понеділок','Вівторок','Середа','Четвер','П`ятниця','Субота'],
			dayNamesShort:['Нед.','Пон.','Вівт', 'Сер.','Чет.','П`ят.','Суб.'],
			dayNamesMin:['Нд','Пн','Вт','Ср','Чт','Пт','Сб'],
			weekHeader:'Sem.',dateFormat:'dd/mm/yy',firstDay:1,isRTL:false,
			showMonthAfterYear:false,yearSuffix:''};
		$.datepicker.setDefaults($.datepicker.regional['ua']);

		if(!$.placeholder.input || !$.placeholder.textarea) {
			$('#hint').hide();
		}
	}
});