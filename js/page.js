/* 
 * Developed by
 * YOBA
 * 
 */

function CabinetPanel(){
  this.isOpened=0;
  this.gripClick=function(event){
    if(this.isOpened==0){
      $('#cabinet').addClass('opened');
      this.isOpened=1;
    } else{
      $('#cabinet').removeClass('opened');
      this.isOpened=0;
    }
  }
  this.refresh=function(){
    $('#cabinet').load('main/cabinet');
  };
}

function Page(){
  this.cabinet=new CabinetPanel();
  this.currentPage="landing";
  this.inited=0;
  MessageBox();
  this.showError=function(msg){
    MessageBox.show(msg);
  };
  this.init=function(inputData){
    if((inputData['errorMessage']!=null) && (inputData['errorMessage'].length>0)){
      this.showError(inputData['errorMessage']);
    }
    this.cabinet.refresh();
  };
  this.switchPage=function(page){
    $('#page-'+this.currentPage).animate({opacity:'0'},200);
        $('#content-layer .main-content').load('main/'+page,function(){
          $('#page-'+page).delay(200).animate({opacity:'1'},200);
        });
    this.currentPage=page;
  };
  
  this.loginUser=function(login,password){
    $.post('main/login',{login:login,password:password},
    function(data){
      if(data=='login_ok'){
        page.cabinet.refresh();
      }else{
        MessageBox.show(data);
      }
      
    });
    
  };
  
}
