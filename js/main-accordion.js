/**
 * Created by d.marinov on 07.02.14.
 */
$(document).ready(function(){
    var dur = '400';
    $('.accordion li .accordion-link-wr').hide();

    $('.accordion>li').on('mouseenter', function(){

        var itemId = $(this).data('slide');

        $('.accordion>li').removeClass('active').addClass('not-active');
        $(this).removeClass('not-active').addClass('active');

        $('.accordion>.not-active').stop().animate({width: '20%'}, {duration: dur, queue: false });
        $(this).stop().animate({width: '40%'}, {duration: dur, queue: false });

        /***** Arrow *****/
        $('.arrow').stop().animate({bottom: '309px'}, {duration: dur, queue: false });
        $(this).find('.arrow').stop().animate({bottom: '350px'}, {duration: dur, queue: false });
        /***** End of Arrow *****/

        //Only invite-accordion
        if($(this).parents('ul').attr('id') == 'invite-accordion'){
            $('.accordion-content').stop().animate({marginBottom: '20px'}, {duration: dur, queue: false });
            $(this).find('.accordion-content').stop().animate({marginBottom: '50px'}, {duration: dur, queue: false });
        }

        //Only main accordion
        if($(this).parents('ul').attr('id') == 'main-accordion'){
            /***** Title width *****/
            $('.accordion li p:first-child').stop().animate({width: '210px'}, {duration: 0, queue: false });
            $(this).find('p').stop().animate({width: '100%'}, {duration: 0, queue: false });
            /***** End of Title width *****/

            /***** Link *****/
            $('.accordion li .accordion-link-wr').stop().fadeOut(dur);
            $(this).find('.accordion-link-wr').stop().fadeIn(dur);
            /***** End of Link *****/

            /***** Description *****/
            $('.accordion-description p').hide();
            $('.descr-'+itemId).stop().fadeIn();
            /***** End of Description *****/

            /***** Mask *****/
            $('.accordion li .mask').stop().fadeIn(dur);
            $(this).find('.mask').stop().fadeOut(dur);
            /***** End of Mask *****/
        }

        //Only gifts-accordion
        if($(this).parents('ul').hasClass('gifts-accordion')){

            $('.accordion-content').stop().animate({marginBottom: '0'}, {duration: dur, queue: false });
            $(this).find('.accordion-content').stop().animate({marginBottom: '30px'}, {duration: dur, queue: false });
        }

    });

    $('.accordion').on('mouseleave', function(){
        $('.accordion>li').removeClass('active not-active').stop().animate({width: '25%'}, dur, function() {});
        $('.arrow').stop().animate({bottom: '309px'}, {duration: dur, queue: false });

        //Only main accordion
        if($(this).attr('id') == 'main-accordion'){
            $('.accordion li p:first-child').stop().animate({width: '210px'}, {duration: dur, queue: false });
            $('.accordion li .accordion-link-wr').stop().fadeOut(dur);

            $('.accordion-description p').stop().fadeOut(dur);
            $('.accordion-description p:first-child').stop().fadeIn(dur);

            $('.accordion li .mask').stop().fadeOut(dur);
        }

        //Only gifts-accordion
        if($(this).hasClass('gifts-accordion')){
            $('.accordion-content').stop().animate({marginBottom: '0'}, {duration: dur, queue: false });
        }

        //Only invite-accordion
        if($(this).attr('id') == 'invite-accordion'){
            $('.accordion-content').stop().animate({marginBottom: '20px'}, {duration: dur, queue: false });
        }
    });
});